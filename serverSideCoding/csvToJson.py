import pandas as pd
import json
from itertools import groupby
from collections import OrderedDict
import csv
import random as rand
import plotly
plotly.offline.init_notebook_mode(connected=True)
import plotly.figure_factory as ff


import datetime
from dateutil.relativedelta import relativedelta

print('started csvToJson')

startDate=datetime.date.today()
print(startDate)

# Job,Operation,Machine,OpCode,StartTime,EndTime,Duration
df = pd.read_csv('sortedByOperation.csv',
                 dtype={
                    "Job" : str,
                    "Operation" : str,
                    "Machine" : str,
                    "OpCode" : str,
                    "StartTime" : int,
                    "EndTime" : int,
                    "Duration" : str
                 },header = 0, index_col=False)

# with open('sortedByOperation.csv','r') as inFile:
#     reader=csv

# print(df)

# results=df.to_json(orient='records')
# print(results)


data=df.to_dict(orient='records')

# with open('schedule.json','w') as file:
#     json.dump(data,file,indent=4)

machines=[]
for i in data:
    # print(i['Machine'])
    # print(type(int(i['StartTime'])))
    machines.append(dict(Task=str('Machine:'+i['Machine']),
                         Start=str(startDate+datetime.timedelta(hours=i['StartTime'])),#relativedelta(i['StartTime'])),
                         Finish=str(startDate+datetime.timedelta(hours=i['EndTime'])),#relativedelta(i['EndTime'])),
                         Resource=str('Job:'+i['Job'])
                    ))

colors={}
for i in machines:
    obj=i['Resource']
    r=rand.randint(0,255)
    g=rand.randint(0,255)
    b=rand.randint(0,255)
    colors[obj]='rgb('+str(r)+','+str(g)+','+str(b)+')'

    # print(colors[obj])


fig = ff.create_gantt(machines, colors=colors, index_col='Resource', show_colorbar=True, group_tasks=True)
plotly.offline.plot(fig)