var express=require('express');
var app=express();
var path = require('path');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;

app.get('/',(req,res)=>{

    console.log('gonna execure csv2json')

    const subprocess = spawn('python3',['./csvToJson.py']);
    subprocess.stdout.on('data', function(data) {

        console.log(data.toString());
        if(data.length>50){
            // res.sendFile(path.join(__dirname + '/temp-plot.html'));
            res.end();
        }
//        res.write(data);
//        res.end('end');
    });
});

app.get('/getGantt',(req,res)=>{
    res.sendFile(path.join(__dirname + '/temp-plot.html'));
//    res.end();
});

app.get('/runJss',(req,res)=>{

    console.log('executing JSS');    

    exec('../run', function(error, stdout, stderr) {
        if(stdout){
            // console.log(stdout.toString());
           res.send(stdout);
           console.log('ended JSS');
        }
        if(error){
            console.log(error);
            res.json(error);
            console.log('ended JSS');
        }
        if(stderr){
            console.log(stderr);
            res.json(stderr);
            console.log('ended JSS');
        }
    //    res.end();   
    });
    
});

app.listen(3000, function() {
    console.log('server running on port 3000');
});