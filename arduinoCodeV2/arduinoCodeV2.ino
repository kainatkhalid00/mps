#pragma once

#include <PubSubClient.h>

#include<TimerOne.h>

#include <Ethernet2.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp2.h>

#include <SPI.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "Functionality.h"


#include "DHT.h"

#define DHTPIN A3     // pin connected to the DHT sensor
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);
float t=30.0;
float h=50.0;
unsigned long  nowTime=0;

EnergyMonitor emon;

//volatile unsigned long startTime = 0;
//volatile unsigned long finishTime = 0;
//unsigned long maxTime=0;

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEE}; //Assign a mac address
IPAddress ip(192, 168, 137, 20); //node IP address
IPAddress server(192, 168, 137, 2); //server/broker IP address
unsigned int localPort = 9000; //nodes Port to talk over

EthernetClient ethClient;
PubSubClient mqttClient(ethClient);


volatile double irmsBuffer[100]={0};
volatile int irmsBufferPointer=0;
volatile double sumI=0;

void configureServer(){

  
  mqttClient.setServer(server, 1884);//1884 is the port of MQTT broker
  mqttClient.connect("node");
  
  Ethernet.begin(mac, ip); //Initialize Ethernet

  if (!mqttClient.connected()) {
    reconnectMQTT();
  }
  mqttClient.loop();
  
  Serial.println("Server Configured ....");
  delay(1000); //delay in ms
}

void reconnectMQTT() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect("node")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      mqttClient.publish("current","reconnected node");

    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void sendDataToPCViaMQTT(void){
  Serial.println("in sendDataToPCViaMQTT ");
  double I=(sqrt(sumI/irmsBufferPointer)* adc0.getMvPerCount()* 30)/1000;
//  String Irms="node,"+String(fabs(I-0.02));//0.02A is zero error && fabs is abs for floats or doubles
//  String Irms="node,"+String(I);
//  Serial.print("Irms = ");
//  Serial.println(String(millis())+","+I);
//  //////////////dht stuff///////////////
//  h = dht.readHumidity();
//  t = dht.readTemperature();
//  Serial.print("h=");
//  Serial.print(h);
//  Serial.print(",t=");
//  Serial.println(t);
  String data="nodeData,"+String(double(I))+","+String(t)+","+String(h);
  //////////////////////////////////////
//  mqttClient.publish("nodeData",Irms.c_str());
  mqttClient.publish("nodeData",data.c_str());
  
  irmsBufferPointer=0;
  sumI=0;

  if (!mqttClient.connected()) {
    reconnectMQTT();
  }
  mqttClient.loop();  
}

void readCurrentValue(void){

  Serial.println(irmsBufferPointer);
  double I=emon.calcIrms();
  sumI=sumI+(I*I);
  irmsBufferPointer++;
  
}

void setup() {
  // put your setup code here, to run once:  
  Wire.begin();
  Serial.begin(9600);
  dht.begin();
  configureServer();
  emon.init();
  
  delay(1000);
  
  Timer1.initialize(200000);// initialize timer1, and set a 200ms period
  Timer1.attachInterrupt(sendDataToPCViaMQTT);  // attaches callback() as a timer overflow interrupt

//  //initial fetching of T and H
//  h = dht.readHumidity();
//  t = dht.readTemperature();
//  Serial.print("h=");
//  Serial.print(float(h));
//  Serial.print(",t=");
//  Serial.println(float(t));

}

void loop() {

  delay(3);//read an ADC value after 3ms to make 5 Irms values per second
  readCurrentValue();
  
  if(millis()-nowTime>5000){

    //updating T & H after 5 sec
    h = dht.readHumidity();
    t = dht.readTemperature();
//    Serial.print("h=");
//    Serial.print(float(h));
//    Serial.print(",t=");
//    Serial.println(float(t));

    nowTime=millis();
  }
  


}
