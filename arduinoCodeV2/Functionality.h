#pragma once

#include "ADS1115_.h"


ADS1115 adc0(ADS1115_DEFAULT_ADDRESS);

class EnergyMonitor
{
  public:
  void init()
    {
      adc0.initialize();

      adc0.setMode(ADS1115_MODE_CONTINUOUS);
      adc0.setRate(ADS1115_RATE_860);
      adc0.setGain(ADS1115_PGA_1P024);//0.03125
//      adc0.setGain(ADS1115_PGA_2P048);//0.625
      Serial.println("EnergyMonitor initialized()...");
      delay(500);
    }
    double calcIrms(){
//      Serial.println("here");
//      return (adc0.getConversionP0N1()* adc0.getMvPerCount() / 1000) * 0.707 * 30;
      return adc0.getConversionP0N1();
    }
    
};
