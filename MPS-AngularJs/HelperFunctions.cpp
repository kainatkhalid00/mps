#include "HelperFunctions.h"



// template <typename T>
// int getIndex(const std::vector<T>& vec, const T& obj) {
// 	return (distance(vec.begin(), find(vec.begin(), vec.end(), obj)));
// }

// template <typename T>
// void printVector(vector<T>& vec) {
// 	for (auto i : vec) {
// 		cout << i << " ";
// 	}
// 	cout << endl;
// }

// template <typename T>
// void printChromosome(vector<T>& vec) {
// 	for (auto i : vec) {
// 		cout << i << ",";
// 	}
// 	cout << endl;
// }

void writeCSVsortedByOperation(vector<Job>& jobs) {
	ofstream myfile;
	myfile.open("sortedByOperation.csv");
	myfile << "Job,Operation,Machine,OpCode,StartTime,EndTime,Duration" << endl;

	for (auto job : jobs) {
		for (auto operation : job.operations) {
			myfile << job.jobIndex << "," << operation.operationIndex << "," << operation.allotedMachine << "," << 
				operation.operationCode << "," << operation.operationStartTime << "," << operation.operationEndTime<<","<<
				operation.operationDuration << endl;
		}

	}
	myfile.close();
}

void writeCSVsortedByMachine(vector<Machine>& machines) {
	ofstream myfile;
	myfile.open("sortedByMachine.csv");
	myfile << "Job,Operation,Machine,OpCode,ST,ET" << endl;


	for (auto& machine : machines) {
		for (auto scheduledOp : machine.scheduledOperations) {
			myfile << scheduledOp.jobRef.jobIndex << "," << scheduledOp.operationRef.operationIndex << "," << scheduledOp.operationRef.allotedMachine << "," <<
				scheduledOp.operationRef.operationCode << "," << scheduledOp.operationRef.operationStartTime << "," << scheduledOp.operationRef.operationEndTime
				<< endl;
		}

	}

	myfile.close();


}

void writeCSVFile(Machine* ma, Job* ja, int machineSize, int* machineScheduledOperationIndex) {

	ofstream myfile;
	myfile.open("output.csv");
	myfile << "Job,Operation,FOQtyIndex,OpCode,Machine,StartTime,EndTime" << endl;

	for (int i = 0; i < machineSize; i++) {

		for (int j = 0; j < machineScheduledOperationIndex[i]; j++) {

			int jobIndex = ma[i].scheduledOperations[j].jobRef.jobIndex;
			string opCode = ma[i].scheduledOperations[j].operationRef.operationCode;
			int FOQtyIndex = ma[i].scheduledOperations[j].operationRef.FOQtyIndex;

			int opIndex = ma[i].scheduledOperations[j].operationRef.operationIndex;
			int startTime = ma[i].scheduledOperations[j].operationRef.operationStartTime;
			int endTime = ma[i].scheduledOperations[j].operationRef.operationEndTime;

			// cout << jobIndex << "\t" << opIndex << "\t" << i << "\t" << startTime << "\t" << endTime << endl;
			myfile << jobIndex << "," << opIndex << "," << FOQtyIndex << "," << opCode << "," << i << "," << startTime
				<< "," << endTime << "\n";

		}

	}

	myfile.close();

}
