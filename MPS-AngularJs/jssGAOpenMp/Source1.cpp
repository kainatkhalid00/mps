#include "classes.h"
// #include <sys/time.h>
#include "HelperFunctions.h"
#include "Genetic.h"
#include "ParseJSON.h"
#include <thread>
#include <omp.h>


const int popSize = 8;
const int genSize = 100;

int bestParentId = -1;
int bestChildId = -1;

int bestFitness = -1;

//jobs and machines for each chromosome
vector<vector<Job>>newJobs(popSize);
vector<vector<Machine>>newMachines(popSize);
vector<vector<Job>>newJobsC(popSize);
vector<vector<Machine>>newMachinesC(popSize);

using namespace std;
using namespace std::chrono;

char* getCmdOption(char** begin, char** end, const std::string& option) {

	char** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end) {
		return *itr;
	}
	return 0;
}


bool cmdOptionExists(char** begin, char** end, const std::string& option) {
	return std::find(begin, end, option) != end;
}

void jobsAndMachinesAllocator(vector<vector<Job>>&newJobs, vector<vector<Machine>>&newMachines,int id) {
	newJobs[id] = jobs;
	newMachines[id] = machines;
}

void executeGA(int popSize,int maxGenerations) {

	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	vector<vector<int>> parentPop(popSize);
	vector<int> makespanP(popSize);

	vector<vector<int>> childPop(popSize);
	vector<int>makespanC(popSize);


	for (int gen = 0; gen < maxGenerations; gen++) {

		cout << "gen = " << gen<<" , "<<endl;
		
		#pragma omp parallel num_threads(popSize)
		{
				jobsAndMachinesAllocator( newJobs, newMachines, omp_get_thread_num());
		}
		//cout << "parents allocated....." << endl;
		//#pragma omp barrier
		#pragma omp parallel  num_threads(popSize)
		{
			jobsAndMachinesAllocator(newJobsC, newMachinesC, omp_get_thread_num());
		}
		//cout << "childs allocated....." << endl;

		//#pragma omp barrier
		#pragma omp parallel num_threads(popSize)
		{
			generateChromosome(newJobs, parentPop, omp_get_thread_num());
		}
		//cout << "chromosomes generated....." << endl;

		//#pragma omp barrier
		#pragma omp parallel  num_threads(popSize)
		{
			int i = omp_get_thread_num();
			getMakespan( parentPop[i], newJobs[i],newMachines[i], makespanP, i);
		}
		//cout << "getMakespan parents....." << endl;

		int bestParentFitness = *min_element(makespanP.begin(), makespanP.end());
		int bestParentIndex = getIndex(makespanP, bestParentFitness);
		//cout << "best parent = " << bestParentFitness <<" , ";


		//#pragma omp barrier
		#pragma omp parallel num_threads(popSize)
		{
			int i = omp_get_thread_num();
			if (i % 2 == 0) {
				onePointCrossover(parentPop[i], parentPop[i + 1], childPop[i]);
				onePointCrossover(parentPop[i + 1], parentPop[i], childPop[i + 1]);
			}
		}
		//cout << "crossover....." << endl;

		//#pragma omp barrier
		#pragma omp parallel num_threads(popSize)
		{
			int i = omp_get_thread_num();
			mutate( childPop[i]);
		}
		//cout << "mutation....." << endl;

		//#pragma omp barrier
		#pragma omp parallel  num_threads(popSize)
		{
			int i = omp_get_thread_num();
			getMakespan(childPop[i], newJobsC[i], newMachinesC[i], makespanC, i);
		}
		//cout << "getMakespan childs....." << endl;


		int bestChildFitness = *min_element(makespanC.begin(), makespanC.end());
		int bestChildIndex = getIndex(makespanC, bestChildFitness);
		int worstChildFitness = *max_element(makespanC.begin(), makespanC.end());
		int worstChildIndex = getIndex(makespanC, worstChildFitness);

		//cout << "best child = " << bestChildFitness << " , ";
		//cout << "worst child = " << worstChildFitness << endl;

		childPop[worstChildIndex] = parentPop[bestParentIndex];

		//#pragma omp barrier
		#pragma omp parallel  num_threads(popSize)
		{
			int i = omp_get_thread_num();
			parentPop[i] = childPop[i];
		}

		//#pragma omp barrier


		if (bestParentFitness >= bestChildFitness) {
			//cout << "best is parent having = " << bestParentFitness << endl;
			bestParentId = bestParentIndex;
			bestChildId = -1;
			bestFitness = bestParentFitness;
		}
		else {
			//cout << "best is child having = " << bestChildFitness << endl;//bestFitness
			bestChildId = bestChildIndex;
			bestParentId = -1;
			bestFitness = bestChildFitness;
		}
		//#pragma omp barrier
	}

	//#pragma omp barrier

	//delete threads;

	auto finish = std::chrono::high_resolution_clock::now();
	std::cout << __func__ << " took "
		<< std::chrono::duration_cast<chrono::milliseconds>(finish - start).count()
		<< " milliseconds\n";

}

vector<int> getTokensofLine(string s) {
	vector<int> tokens;
	std::string delimiter = ",";

	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
		token = s.substr(0, pos);
		if (token.compare("")) {
			tokens.push_back(stoi(token));
		}
		s.erase(0, pos + delimiter.length());
	}

	return tokens;
}


int main(int argc, char* argv[]) {

	char* streamsChar = getCmdOption(argv, argv + argc, "-s");

	loadData();

	executeGA(popSize,genSize);

	cout << "best fitness = " << bestFitness << endl;

	if (bestChildId < bestParentId) {
		writeCSVsortedByOperation(newJobs[bestParentId]);
	}
	else {
		writeCSVsortedByOperation(newJobsC[bestChildId]);
	}

	
	return 0;
}
