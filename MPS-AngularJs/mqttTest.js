var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://localhost:1884');
 
var devicesNames=['Bulb2','Bulb1','EnergySaver','Kettle'];
var devicesCurrentValues=[0.45,0.052,0.035,5.8]


client.on('connect', function () {
  client.subscribe('nodeData', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt');
    }
  });
});
 
client.on('message', function (topic, message) {
  // message is Buffer
//   console.log(message.toString())
  var i=message.toString().indexOf(',');
  if(i===null || i===undefined || i===-1){
      return;
  };
  var j=message.toString().indexOf(',',i+1);
  if(j===null || j===undefined || j===-1){
      return;
  };
//   console.log(i,j);
  var current = Number.parseFloat(message.toString().slice(i+1,j));
  console.log('current = ',current);


  if       (.02<=current && current<0.038){
    console.log('Current Running Devices are : ',devicesNames[2])
  }else if (.038<=current && current<0.06){
    console.log('Current Running Devices are : ',devicesNames[0])
  }else if (.35<=current && current<0.4){
    console.log('Current Running Devices are : ',devicesNames[1])
  }else if (.42<=current && current<0.45){
    console.log('Current Running Devices are : ',devicesNames[0],devicesNames[2])
  }else if (.07<=current && current<0.09){
    console.log('Current Running Devices are : ',devicesNames[1],devicesNames[2])
  }else if (.45<=current && current<0.475){
    console.log('Current Running Devices are : ',devicesNames[0],devicesNames[1],devicesNames[2])
  }else if (.4<=current && current<0.42){
    console.log('Current Running Devices are : ',devicesNames[0])
  }else if (current>devicesCurrentValues[3]){
    var i=current-devicesCurrentValues[3];
    var devices=[devicesNames[3]];
    if (.02<=i && i<0.038){
        devices.push(devicesNames[2]);
    }else if (.038<=i && i<0.06){
        devices.push(devicesNames[0]);
        console.log('Current Running Devices are : ',devicesNames[0])
    }else if (.35<=i && i<0.4){
        devices.push(devicesNames[1]);
        console.log('Current Running Devices are : ',devicesNames[1])
    }else if (.42<=i && i<0.45){
        devices.push(devicesNames[0],devicesNames[2]);
    }else if (.07<=i && i<0.09){
        devices.push(devicesNames[1],devicesNames[2]);
    }else if (.45<=i && i<0.475){
        devices.push(devicesNames[0],devicesNames[1],devicesNames[2]);
    }else if (.4<=i && i<0.42){
        devices.push(devicesNames[0]);
    }
    console.log('Current Running Devices are : ',devices)
  } 
  else{
      console.log('No devices are running')
  }



//   client.end()
});