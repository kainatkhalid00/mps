var express=require('express');
var router=express.Router();

var JobStatus=require('../models/jobStatus.model');

router.get('/',(req,res)=>{
    console.log('got req for JobStatus');
    
    JobStatus.find().then(jobStatus=>{
        console.log(jobStatus[jobStatus.length-1]);
        res.send(jobStatus[-1]);            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/',(req,res)=>{//it is actually jobStatus
    // console.log(req.body.jobStatus);
    var temp=req.body.jobStatus;

    var jb=new JobStatus({        
       
        wait:temp.wait,
        wip:temp.wip,
        done:temp.done
    });

    // JobStatus.collection.drop();
    // .then(()=>{
        
        jb.save().then(data=>{
            console.log('new JobStatus added');
            res.send(data);
        }).catch(err=>{
            res.send('error => '+err);
        });
    // }).catch(err)=>{
    //     console.log(err);
    // });
    

});

module.exports=router;