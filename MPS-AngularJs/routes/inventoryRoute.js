var express=require('express');
var router=express.Router();

var Inventory=require('../models/inventory.model');

router.get('/',(req,res)=>{
    console.log('got req for inventory');
    
    Inventory.find().then(products=>{
        res.send(products);            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/',(req,res)=>{//it is actually inventoryRoute route
    // console.log(req.body.products);
    var temp=req.body.products;
    var product={
                
        id: temp.id,
        description: temp.description,
        inStockQuantity: temp.inStockQuantity,
        newOrderQuantity: temp.newOrderQuantity,
        actualQuantity:temp.actualQuantity,
        avaiableRaw:temp.avaiableRaw,
        requestedRaw:temp.requestedRaw,
        daysToRecieveRaw:temp.daysToRecieveRaw
    };
    var inv=new Inventory({        
       
        id: temp.id,
        description: temp.description,
        inStockQuantity: temp.inStockQuantity,
        newOrderQuantity: temp.newOrderQuantity,
        actualQuantity:temp.actualQuantity,
        avaiableRaw:temp.avaiableRaw,
        requestedRaw:temp.requestedRaw,
        daysToRecieveRaw:temp.daysToRecieveRaw
    });

    if(temp._id!=null){
        Inventory.findOneAndDelete({'_id':temp._id})
        .then(oldData=>{
            if(oldData){
                // console.log(oldData);
                console.log('old product deleted');
                inv.save().then(data=>{
                    console.log('product updated');
                    res.send(data);
                }).catch(err=>{
                    res.send('error => '+err);
                });
            }
                                 
        }).catch(err=>{    
            res.status(500).json({ message: 'Some Error!' });
            console.log(err);
        });
    }else{
        inv.save().then(data=>{
            console.log('new product added');
            res.send(data);
        }).catch(err=>{
            res.send('error => '+err);
        });

    }
});

module.exports=router;