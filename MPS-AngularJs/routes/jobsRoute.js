var express=require('express');
var router=express.Router();

var Job=require('../models/jobs.model');

router.post('/delete',(req,res)=>{
    
    var temp=req.body.jobs;
    // console.log(temp);
    // return;

    Job.findOneAndDelete({'_id':temp._id})
    .then(oldData=>{
        if(oldData){
            // console.log(oldData);
            console.log('emp deleted');
            res.send('emp deleted');
        }
                             
    }).catch(err=>{    
        res.status(500).json({ message: 'Some Error!' });
        console.log(err);
    });
});


router.get('/',(req,res)=>{
    console.log('got req for jobs');
    
    Job.find().then(jobs=>{
        res.send(jobs);            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/',(req,res)=>{//it is actually jobsRoute
    // console.log(req.body.job);
    var temp=req.body.jobs;

    var jb=new Job({        
       
        allotedWorker:temp.allotedWorker,
        jobId:temp.jobId,
        jobType:temp.jobType,
        jobDuration:temp.jobDuration,
        startTime:temp.startTime,
        endTime:temp.endTime,
        jobStatus:temp.jobStatus
    });

    if(temp._id!=null){
        Job.findOneAndDelete({'_id':temp._id})
        .then(oldData=>{
            if(oldData){
                // console.log(oldData);
                console.log('old product deleted');
                jb.save().then(data=>{
                    console.log('product updated');
                    res.send(data);
                }).catch(err=>{
                    res.send('error => '+err);
                });
            }
                                 
        }).catch(err=>{    
            res.status(500).json({ message: 'Some Error!' });
            console.log(err);
        });
    }else{
        jb.save().then(data=>{
            console.log('new product added');
            res.send(data);
        }).catch(err=>{
            res.send('error => '+err);
        });

    }
});

module.exports=router;