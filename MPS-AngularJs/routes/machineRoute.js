var express=require('express');
var router=express.Router();

var Machine=require('../models/machine.model');

router.post('/delete',(req,res)=>{
    
    var temp=req.body.machines;
    // console.log(temp);
    // return;

    Machine.findOneAndDelete({'_id':temp._id})
    .then(oldData=>{
        if(oldData){
            // console.log(oldData);
            console.log('mac deleted');
            res.send('mac deleted');
        }
                             
    }).catch(err=>{    
        res.status(500).json({ message: 'Some Error!' });
        console.log(err);
    });
});


router.get('/',(req,res)=>{
    console.log('got req for machines');
    
    Machine.find().then(machines=>{
        res.send(machines);            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/',(req,res)=>{//it is actually jobsRoute
    // console.log(req.body.machines);
    var temp=req.body.machines;

    var mac=new Machine({        
       
        
        machineId:temp.machineId,
        machineType:temp.machineType,
        machineCapacity:temp.machineCapacity,
        machineInstallationDate:temp.machineInstallationDate,
        machineStatus:temp.machineStatus
    });

    if(temp._id!=null){
        Machine.findOneAndDelete({'_id':temp._id})
        .then(oldData=>{
            if(oldData){
                // console.log(oldData);
                console.log('old machine deleted');
                mac.save().then(data=>{
                    console.log('machine updated');
                    res.send(data);
                }).catch(err=>{
                    res.send('error => '+err);
                });
            }
                                 
        }).catch(err=>{    
            res.status(500).json({ message: 'Some Error!' });
            console.log(err);
        });
    }else{
        mac.save().then(data=>{
            console.log('new machine added');
            res.send(data);
        }).catch(err=>{
            res.send('error => '+err);
        });

    }
});

module.exports=router;