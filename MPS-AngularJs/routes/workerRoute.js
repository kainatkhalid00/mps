var express=require('express');
var router=express.Router();

var Worker=require('../models/workers.model');
var Job=require('../models/jobs.model');

router.get('/',(req,res)=>{
    console.log('got req for workers');
    
    Worker.find().then(workers=>{
        res.send(workers);            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/name',(req,res)=>{
    console.log('got req for a worker');
    var id=req.body.id;
    Worker.findOne({'id':id}).then(worker=>{
        // res.send(worker.name);
        var wName=worker.name;
        var myJobs=[];
        Job.find().then(jobs=>{
            // myJobs=jobs;   
            jobs.forEach(function(element) {
                if(element.allotedWorker==wName){
                    myJobs.push(element);
                }                
            }, this); 
            res.send(myJobs);        
        }).catch(err=>{
            res.send("error : ",err);   
        });            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/',(req,res)=>{//it is actually inventoryRoute route
    // console.log(req.body.workers);
    var temp=req.body.workers;

    var worker=new Worker({        
       
        name:temp.name,
        id:temp.id
    });

    if(temp._id!=null){
        Worker.findOneAndDelete({'_id':temp._id})
        .then(oldData=>{
            if(oldData){
                // console.log(oldData);
                console.log('old worker deleted');
                worker.save().then(data=>{
                    console.log('worker updated');
                    res.send(data);
                }).catch(err=>{
                    res.send('error => '+err);
                });
            }
                                 
        }).catch(err=>{    
            res.status(500).json({ message: 'Some Error!' });
            console.log(err);
        });
    }else{
        worker.save().then(data=>{
            console.log('new worker added');
            res.send(data);
        }).catch(err=>{
            res.send('error => '+err);
        });

    }
});

module.exports=router;