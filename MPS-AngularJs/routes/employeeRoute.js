var express=require('express');
var router=express.Router();

var Employee=require('../models/employee.model');

router.post('/delete',(req,res)=>{
    
    var temp=req.body.employee;
    // console.log(temp);
    // return;

    Employee.findOneAndDelete({'_id':temp._id})
    .then(oldData=>{
        if(oldData){
            // console.log(oldData);
            console.log('emp deleted');
            res.send('emp deleted');
        }
                             
    }).catch(err=>{    
        res.status(500).json({ message: 'Some Error!' });
        console.log(err);
    });
});

router.get('/',(req,res)=>{
    console.log('got req for employees');
    
    Employee.find().then(employees=>{
        res.send(employees);            
    }).catch(err=>{
        res.send("error : ",err);   
    });
});

router.post('/',(req,res)=>{//it is actually updateEmployee route
    // console.log(req.body.employee);
    var temp=req.body.employee;
    var employee={
                
        employeeId: temp.employeeId,
        name: temp.name,
        designation:temp.designation,
        department:temp.department,
        gender:temp.gender,
        salary:temp.salary
    };
    var emp=new Employee({        
        employeeId: temp.employeeId,
        name: temp.name,
        designation:temp.designation,
        department:temp.department,
        gender:temp.gender,
        salary:temp.salary
    });

    if(temp._id!=null){
        Employee.findOneAndDelete({'_id':temp._id})
        .then(oldData=>{
            if(oldData){
                console.log(oldData);
                console.log('old emp deleted');
                emp.save().then(data=>{
                    console.log('emp updated');
                    res.send(data);
                }).catch(err=>{
                    res.send('error => '+err);
                });
            }
                                 
        }).catch(err=>{    
            res.status(500).json({ message: 'Some Error!' });
            console.log(err);
        });
    }else{
        emp.save().then(data=>{
            console.log('new emp added');
            res.send(data);
        }).catch(err=>{
            res.send('error => '+err);
        });

    }
   
});

module.exports=router;