// var regression=require('regression');

// var data = [];
// // var result = regression.polynomial(data, { order: 3 });

// // console.log(result)
// x=-200;
// for(let i=0;i<10;i++){
//     // y=Math.round(Math.random()*100000);
//     y=Math.random();
//     x=x+200;
//     data.push([x,y]);
//     console.log( regression.polynomial(data, { order: 3 }));

//     if(data.length>=10){
//         data.shift();
//     }
    
// }

//////////////////////////////////////////////////////
var PolynomialRegression =require('ml-regression-polynomial');

// var x = [50, 50, 50, 70, 70, 70, 80, 80, 80, 90, 90, 90, 100, 100, 100];
// var y = [3.3, 2.8, 2.9, 2.3, 2.6, 2.1, 2.5, 2.9, 2.4, 3.0, 3.1, 2.8, 3.3, 3.5, 3.0];
var degree = 5; // setup the maximum degree of the polynomial
var xArr=[],yArr=[];
x=-200;
for(let i=0;i<10;i++){
    // y=Math.random();
    y=0.65;
    x=x+200;
    xArr.push(x);
    yArr.push(y);    
}

console.log(xArr,'\n',yArr)

var regression = new PolynomialRegression(xArr, yArr, degree);
console.log(regression.predict(80)); // Apply the model to some x value. Prints 2.6.
// console.log(regression.coefficients); // Prints the coefficients in increasing order of power (from 0 to degree).
// console.log(regression.toString(3)); // Prints a human-readable version of the function.
// console.log(regression.toLaTeX());
// console.log(regression.score(x, y));