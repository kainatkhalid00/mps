#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>
#include <curand.h>
#include <curand_kernel.h>
#include "classes.h"
#include "lock.h"

//struct Lock {
//	int*mutex;
//	Lock() {
//		int state = 0;
//		cudaMalloc((void**) &mutex, sizeof(int));
//		cudaMemcpy(mutex, &state, sizeof(int), cudaMemcpyHostToDevice);
//	}
//
//	~Lock() {
//		cudaFree(mutex);
//	}
//
//	__device__ void lock() {
//		while (atomicCAS(mutex, 0, 1) != 0)
//			;
//	}
//
//	__device__ void unlock() {
//		atomicExch(mutex, 0);
//	}
//};
//#ifndef LOCK_H
//#define LOCK_H
//struct MyLock {
//	int*mutex;
//	MyLock();
//
//	~MyLock();
//
//	__device__ void lock();
//
//	__device__ void unlock();
//};
//
//#endif

void kernel_wrapper(Job** jobs, Machine** machines, int jobSize, int machineSize, int** machineScheduledOperationIndex,int* sem);

