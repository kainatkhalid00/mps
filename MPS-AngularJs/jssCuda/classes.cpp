#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include <random>
#include <chrono>
#include <string>
#include "kernel.cuh"

#include "classes.h"

using namespace std;

void sortScheduledOperations(vector<ScheduledOperation>& scheduledOperations);

template<typename T>
bool AreVectorsSame(const std::vector<T>& i_Vec1, const std::vector<T>& i_Vec2) {
	if (i_Vec1.size() != i_Vec2.size())
		return false;

	for (size_t i = 0; i < i_Vec1.size(); i++)
		if (i_Vec1[i] == i_Vec2[i]) {
		} else
			return false;

	return true;
}

Tuple::Tuple(int start, int end) {
	this->start = start;
	this->end = end;
}

void Machine::clear() {
	machineFreeTime = 0;
	//scheduledOperations.clear();
}

Machine::Machine(int machineID, string operationCode, string machineName) {
	this->machineID = machineID;
	this->operationCode = operationCode;
	this->machineName = machineName;
	machineFreeTime = 0;
	//timeSlots.assign(5000, 0);

}

//void Machine::scheduleOperationOnMachine(ScheduledOperation op) {
//	this->scheduledOperations.push_back(op);
//	sortScheduledOperations(this->scheduledOperations);
//	this->machineFreeTime = this->scheduledOperations.back().operationPtr->operationEndTime;
//
//	for (int i = op.operationPtr->operationStartTime; i < op.operationPtr->operationEndTime; i++) {
//		this->timeSlots[i] = 1;
//	}
//}

//void Machine::clearSchedule() {
//	scheduledOperations.clear();
//}

bool operator==(const Machine& m1, const Machine& m2) {
	return (m1.machineID == m2.machineID && m1.operationCode == m2.operationCode && m1.machineName == m2.machineName);

}

bool operator==(const Job& job1, const Job& job2) {
	return (job1.jobCode == job2.jobCode && job1.operations == job2.operations && job1.unscheduledOperationIndex == job2.unscheduledOperationIndex
			&& job1.operationsPtr == job2.operationsPtr

	);
}

//Operation Machine::getLastScheduledOperation() {
//	if (!this->scheduledOperations.empty())
//		return *this->scheduledOperations.back().operationPtr;
//	return Operation();
//}
//
//Operation Machine::getFirstScheduledOperation() {
//	return *scheduledOperations.front().operationPtr;
//}

vector<Tuple> Machine::getFreeTimeSlots() {

	vector<Tuple> slots;
	int start = 0;
	int end = 0;
	int i = 0;
	while (i != timeSlotsSize) {

		while (this->timeSlots[i] != 0) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		start = i;

		if (i == timeSlotsSize)
			break;

		while (this->timeSlots[i] != 1) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		end = i;

		slots.push_back(Tuple(start, end));

	}

	return slots;
}

__device__ __host__ Tuple* Machine::getFreeTimeSlotsPtr() {
	vector<Tuple> timeSlots = getFreeTimeSlots();
	return &timeSlots[0];

}

ostream& operator<<(ostream& os, const Machine& machine) {
	os << machine.machineName << "(" << machine.operationCode << ")";

	return os;
}

void Operation::clear() {
	operationStartTime = 0;
	operationEndTime = 0;
}

Operation::Operation(string operationCode, int operationDuration, int operationSequence, bool mappable) {
	this->operationCode = operationCode;
	this->operationDuration = operationDuration;
	this->operationSequence = operationSequence;
	this->mappable = mappable;
	operationStartTime = 0;
	operationEndTime = 0;

}

void Operation::initializeEligibleMachinesIndicesArray(int size) {
	eligibleMachinesIndices = new int[size];
	for (int i = 0; i < size; i++) {
		eligibleMachinesIndices[i] = 0;
	}

	/*for (int i = 0; i < size; i++) {
	 cout << eligibleMachinesIndices[i] << endl;
	 }*/

}

bool operator==(const Operation& operation1, const Operation& operation2) {
	return (operation1.operationCode == operation2.operationCode && operation1.operationDuration == operation2.operationDuration
			&& operation1.operationSequence == operation2.operationSequence && operation1.operationStartTime == operation2.operationStartTime
			&& operation1.operationEndTime == operation2.operationEndTime);
}

bool operator==(const ScheduledOperation& o1, const ScheduledOperation& o2) {
	return (o1.jobPtr == o2.jobPtr && o1.operationPtr == o2.operationPtr);
}

ostream& operator<<(ostream& os, const Operation& operation) {
	os << "\t" << operation.operationSequence << " " << " " << operation.operationCode << " " << operation.operationDuration;

	return os;
}

Job::Job(int jobCode) {
	this->jobCode = jobCode;
	this->unscheduledOperationIndex = 0;
}

void Job::clear() {
	for (Operation& operation : this->operations) {
		operation.clear();
	}
}

void Job::addOperation(Operation operation) {
	this->operations.push_back(operation);
}

ScheduledOperation::ScheduledOperation(Job* job, Operation* operation) {
	this->jobPtr = job;
	this->operationPtr = operation;
}

void sortMachines(vector<Machine>& machines) {
	sort(machines.begin(), machines.end(), [](const Machine& lhs, const Machine& rhs) {
		return lhs.machineFreeTime < rhs.machineFreeTime;
	});
}

void sortScheduledOperations(vector<ScheduledOperation>& scheduledOperations) {
	sort(scheduledOperations.begin(), scheduledOperations.end(), [](const ScheduledOperation& lhs, const ScheduledOperation& rhs) {
		return lhs.operationPtr->operationStartTime < rhs.operationPtr->operationStartTime;
	});
}
