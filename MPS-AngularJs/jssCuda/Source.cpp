#include "kernel.cuh"
#include "classes.h"

#include "json.hpp"

using namespace std;
using namespace std::chrono;
using json = nlohmann::json;

map<string, vector<Machine>> machinesMap;
vector<Job> jobs;
vector<Machine> machines;

const bool SHOW_MAKESPAN = true;

int NUMBER_OF_STREAMS;

const int NUMBER_OF_RUNS = 1;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort =
		true) {
	if (code != cudaSuccess) {
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
				line);
		if (abort)
			exit(code);
	}
}

template<typename T>
int getIndex(const vector<T>& vec, const T& obj) {
	return (distance(vec.begin(), find(vec.begin(), vec.end(), obj)));
}

void loadJobsData(vector<Job>& jobs, json& jsonData) {
	jobs.clear();
	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	for (json section : jsonData["fos"]) {
		for (json job : section["jobs"]) {
			int jobcode = job["jobcode"];

			Job newJob(jobcode);

			for (json operation : job["operations"]) {
				Operation newOperation(operation["type"],
						ceil(operation["duration"].get<int>() / (double) 60),
						operation["sequence"], operation["mappable"]);

				newOperation.operationIndex = newOperation.operationSequence;

				newJob.addOperation(newOperation);
				//cout << operation["type"] << ceil(operation["duration"].get<int>() / (double) 60) << operation["sequence"] << operation["mappable"] << endl;

			}
			jobs.push_back(newJob);
			jobs.back().jobIndex = getIndex(jobs, newJob);
		}
	}

	auto finish = std::chrono::high_resolution_clock::now();
	// std::cout << "loadJobsData() took "
	// 	<< std::chrono::duration_cast<milli>(finish - start).count()
	// 	<< " milliseconds\n";
}

void loadMachinesData(vector<Machine>& machines, json& jsonData) {
	machines.clear();
	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	for (json machineObj : jsonData["availableMachines"]) {
		for (json machine : machineObj["machines"]) {
			Machine newMachine(machine["machineId"], machine["code"],
					machine["name"]);

			machines.push_back(newMachine);
		}
	}

	auto finish = std::chrono::high_resolution_clock::now();
	// std::cout << "loadMachinesData() took "
	// 	<< std::chrono::duration_cast<milli>(finish - start).count()
	// 	<< " milliseconds\n";

//	for (Machine machine : machines) {
//		cout << machine.machineID << "\t" << machine.operationCode << "\t" << machine.machineName << endl;
//	}
}

map<string, vector<Machine>> mapMachinesToOperations(
		vector<Machine>& machines) {
	vector<string> uniqueOperations;
	map<string, vector<Machine>> machinesMap;

	for (auto machine : machines) {
		if (find(uniqueOperations.begin(), uniqueOperations.end(),
				machine.operationCode) == uniqueOperations.end()) {
			uniqueOperations.push_back(machine.operationCode);
		}
	}

	for (auto uniqueOperation : uniqueOperations) {
		vector<Machine> machines;
		machinesMap[uniqueOperation] = machines;
	}

	for (auto machine : machines) {
		string operation = machine.operationCode;
		machinesMap[operation].push_back(machine);
	}

	return machinesMap;
}

void writeCSVFile(Machine* ma, Job* ja, int machineSize,
		int* machineScheduledOperationIndex) {

	ofstream myfile;
	myfile.open("output.csv");
	myfile << "Job, Operation,OpCode, Machine, StartTime, EndTime" << endl;

	for (int i = 0; i < machineSize; i++) {

		for (int j = 0; j < machineScheduledOperationIndex[i]; j++) {

			int jobIndex = ma[i].scheduledOperations[j].jobPtr->jobIndex;
			string opCode =
					ma[i].scheduledOperations[j].operationPtr->operationCode;
			int opIndex =
					ma[i].scheduledOperations[j].operationPtr->operationIndex;
			int startTime =
					ma[i].scheduledOperations[j].operationPtr->operationStartTime;
			int endTime =
					ma[i].scheduledOperations[j].operationPtr->operationEndTime;

			// cout << jobIndex << "\t" << opIndex << "\t" << i << "\t" << startTime << "\t" << endTime << endl;
			myfile << jobIndex << "," << opIndex << "," << opCode << "," << i
					<< "," << startTime << "," << endTime << "\n";

		}

	}

	myfile.close();

}

char* getCmdOption(char** begin, char** end, const std::string& option) {

	char** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end) {
		return *itr;
	}
	return 0;
}

Job** allocateJobsOnGPU(vector<Job> jobs) {

	int jobSize = jobs.size();

	Job**jobsArray = new Job*[NUMBER_OF_STREAMS];

	for (int i = 0; i < NUMBER_OF_STREAMS; i++) {

		Job* ja;
		gpuErrchk(cudaMallocManaged(&ja, jobSize * sizeof(Job)));

		for (int i = 0; i < jobSize; i++) {
			ja[i] = *(new Job(jobs[i]));
			ja[i].operationsSize = jobs[i].operations.size();

			gpuErrchk(
					cudaMallocManaged(&(ja[i].operationsPtr),
							ja[i].operations.size() * sizeof(Operation)));

			for (int op = 0; op < (ja[i]).operations.size(); op++) {
				ja[i].operationsPtr[op] =
						*(new Operation(ja[i].operations[op]));

				gpuErrchk(
						cudaMallocManaged(
								&(ja[i].operationsPtr[op].eligibleMachinesIndices),
								ja[i].operationsPtr[op].eligibleMachinesSize
										* sizeof(int)));

				for (int em = 0;
						em < ja[i].operationsPtr[op].eligibleMachinesSize;
						em++) {
					ja[i].operationsPtr[op].eligibleMachinesIndices[em] =
							(ja[i].operationsPtr[op].eligibleMachinesVector[em]);
				}
			}

		}

		jobsArray[i] = ja;

	}

	return jobsArray;

}

Machine** allocateMachinesOnGPU(vector<Machine> machines) {

	int machineSize = machines.size();

	Machine**machinesArray = new Machine*[NUMBER_OF_STREAMS];

	for (int i = 0; i < NUMBER_OF_STREAMS; i++) {

		Machine* ma;
		gpuErrchk(cudaMallocManaged(&ma, machineSize * sizeof(Machine)));

		for (int i = 0; i < machineSize; i++) {
			ma[i] = *(new Machine(machines[i]));
		}

		int* machineScheduledOperationIndex;
		gpuErrchk(
				cudaMallocManaged(&machineScheduledOperationIndex,
						machineSize * sizeof(int)));
		for (int i = 0; i < machineSize; i++) {
			machineScheduledOperationIndex[i] = 0;
		}

		machinesArray[i] = ma;

	}

	return machinesArray;

}

int** allocateScheduledOperationIndexArrayOnGPU(int machineSize) {
	int** machineScheduledOperationIndexArray = new int*[NUMBER_OF_STREAMS];

	for (int i = 0; i < NUMBER_OF_STREAMS; i++) {
		int *machineScheduledOperationIndex;
		gpuErrchk(
				cudaMallocManaged(&machineScheduledOperationIndex,
						machineSize * sizeof(int)));
		for (int j = 0; j < machineSize; j++) {
			machineScheduledOperationIndex[j] = 0;
		}

		machineScheduledOperationIndexArray[i] = machineScheduledOperationIndex;

	}

	return machineScheduledOperationIndexArray;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option) {
	return std::find(begin, end, option) != end;
}

int main(int argc, char* argv[]) {
	char* filename = getCmdOption(argv, argv + argc, "-f");
	char *streamsChar = getCmdOption(argv, argv + argc, "-s");

	if (streamsChar == 0) {
		NUMBER_OF_STREAMS = 1;
		cout << "No streams provided, defaulting to 1 stream" << endl;
	}

	else {
//		int streamsNum = atoi(streamsChar);
		NUMBER_OF_STREAMS = atoi(streamsChar);
		cout << "Streams:" << NUMBER_OF_STREAMS << endl;
	}

	try {
		if (!filename) {
			throw std::invalid_argument("no input file provided");
		}

	} catch (const std::invalid_argument& ia) {
		std::cerr << "Invalid argument: " << ia.what() << '\n';
		exit(-1);
	}

//	using milli = std::chrono::milliseconds;
//	auto start = std::chrono::high_resolution_clock::now();

	json jsonData;
	ifstream is;
	is.open(string(filename), ifstream::in);
	is >> jsonData;

	loadJobsData(jobs, jsonData);
	loadMachinesData(machines, jsonData);
	machinesMap = mapMachinesToOperations(machines);

	for (auto& job : jobs) {
		for (auto& operation : job.operations) {
			vector<Machine>& eligibleMachines = machinesMap.at(
					operation.operationCode);
			//

			for (auto& eligibleMachine : eligibleMachines) {
				int index = getIndex(machines, eligibleMachine);
				operation.eligibleMachinesVector.push_back(index);
			}

			operation.eligibleMachinesSize =
					operation.eligibleMachinesVector.size();
		}
	}

	int jobSize = jobs.size();
	int machineSize = machines.size();

	float milli;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start);

	for (int iter = 0; iter < NUMBER_OF_RUNS; iter++) {

		Job** jobsData = allocateJobsOnGPU(jobs);
		Machine** machinesData = allocateMachinesOnGPU(machines);
		int**machineScheduledOperationIndexArray =
				allocateScheduledOperationIndexArrayOnGPU(machineSize);

		//
		//	gpuErrchk(cudaMallocManaged(&machineScheduledOperationIndex, machineSize * sizeof(int)));
		//	for (int i = 0; i < machineSize; i++) {
		//		machineScheduledOperationIndex[i] = 0;
		//	}

		//	Lock*lockArray = new Lock[NUMBER_OF_STREAMS];
		int* sem;
		int*sem_host = new int[machineSize];
		for (int i = 0; i < machineSize; i++) {
			sem_host[i] = 0;
		}
		cudaMalloc((void**) &sem, machineSize * sizeof(int));
		cudaMemcpy(sem, sem_host, sizeof(int) * machineSize,
				cudaMemcpyHostToDevice);

		kernel_wrapper(jobsData, machinesData, jobSize, machineSize,
				machineScheduledOperationIndexArray, sem);

		cudaDeviceSynchronize();
//
//		cudaEventRecord(stop);
//		cudaEventSynchronize(stop);
//		cudaEventElapsedTime(&milli, start, stop);
//		printf("execution time (ms): %f \n", milli);

		//	for (int i = 0; i < machineSize; i++) {
		//		cout << i << "\t" << machineScheduledOperationIndexArray[0][i] << endl;
		//	}

		//
		//	for(Job job:jobsData[0]){
		//
		//		for(int i =0;i<job.operationsSize;i++){
		//
		//			cout << job.jobIndex << "\t" << job.operationsPtr[i] << endl;
		//		}
		//	}

		//	for (int job = 0; job < jobSize; job++) {
		//
		//		for (int operation = 0; operation < jobsData[0][job].operationsSize; operation++) {
		//			cout << "J" << jobsData[0][job].jobIndex << "\t" << "O" << operation << "\t" << "opCode"
		//					<< jobsData[0][job].operationsPtr[operation].operationCode << "\t" << "opPtr"
		//					<< &(jobsData[0][job].operationsPtr[operation]) << "\t" << "ST"
		//					<< jobsData[0][job].operationsPtr[operation].operationStartTime << "\t" << "ET"
		//					<< jobsData[0][job].operationsPtr[operation].operationEndTime << "\t" << "M:"
		//					<< jobsData[0][job].operationsPtr[operation].allotedMachine <<
		//
		//					endl;
		//
		//		}
		//	}

		//	if (SHOW_MAKESPAN) {
		//
		//		vector<int> makespan;
		//
		//		for (int stream = 0; stream < NUMBER_OF_STREAMS; stream++) {
		//
		//			for (int machineIdx = 0; machineIdx < machineSize; machineIdx++) {
		//
		//				for (int schedOp = 0; schedOp < machineScheduledOperationIndexArray[stream][machineIdx]; schedOp++) {
		//
		//					cout << stream << "\t" << machineIdx << "\t" << schedOp << "\t" << "\t"
		//							<< machinesData[stream][machineIdx].scheduledOperations[schedOp].jobPtr << "\t"
		//							<< machinesData[stream][machineIdx].scheduledOperations[schedOp].operationPtr << endl;
		//
		////					makespan.push_back(
		////							machinesData[stream][machineIdx].scheduledOperations[schedOp].operationPtr->operationEndTime);
		//				}
		//			}
		//
		////			int max = *max_element(makespan.begin(), makespan.end());
		////			cout << "Makespan:" << max << endl;
		//		}
		//
		////
		//	}

		if (SHOW_MAKESPAN) {

			vector<int> makespan;

			for (int stream = 0; stream < NUMBER_OF_STREAMS; stream++) {

				for (int jobIndex = 0; jobIndex < jobSize; jobIndex++) {

					for (int op = 0;
							op < jobsData[stream][jobIndex].operationsSize;
							op++) {

						//						cout << stream << "\t" << machineIdx << "\t" << schedOp << "\t" << "\t"
						//								<< machinesData[stream][machineIdx].scheduledOperations[schedOp].jobPtr << "\t"
						//								<< machinesData[stream][machineIdx].scheduledOperations[schedOp].operationPtr << endl;

						makespan.push_back(
								jobsData[stream][jobIndex].operationsPtr[op].operationEndTime);
					}
				}

				int max = *max_element(makespan.begin(), makespan.end());
				cout << "Makespan:" << max << endl;
			}

			//
		}

		//	for(int i =0;i<machineSize;i++){
		//		cout << i << "\t" << machineScheduledOperationIndexArray[0][i] << endl;
		//	}

		//	auto finish = std::chrono::high_resolution_clock::now();
		//	std::cout << "main() took " << std::chrono::duration_cast<milli>(finish - start).count() << " milliseconds" << endl;

		//	writeCSVFile(machinesData[0], jobsData[0], machineSize,
		//			machineScheduledOperationIndexArray[0]);

		//	for (int i = 0; i < jobSize; i++) {
		//		for (int op = 0; op < (ja[i]).operations.size(); op++) {
		//			cudaFree(ja[i].operationsPtr[op].eligibleMachinesIndices);
		//		}
		//		cudaFree(ja[i].operationsPtr);
		//	}
		//	cudaFree (ja);

	}

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milli, start, stop);
	printf("execution time (ms): %f \n", milli);
	cudaDeviceReset();

	return 0;
}


