#include "kernel.cuh"

#include <math.h>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char* file, int line, bool abort = true) {
	if (code != cudaSuccess) {
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort)
			exit(code);
	}
}

//__device__ int* sem;

__device__ int countVar = 0;

__device__ void acquire_semaphore(int* lock) {
	while (atomicCAS((int *) lock, 0, 1) != 0)
		;
}

__device__ void release_semaphore(int* lock) {
	*lock = 0;
	__threadfence();
}

__global__ void get_free_time_slot_optimized(Machine* machines, int* eligibleMachineIndices, int operationIndex,
		int currentOpDuration, int prevOpEndTime, int*timeSlots) {
	int idx = threadIdx.x;

	int machineIndex = eligibleMachineIndices[idx];

	int i = prevOpEndTime;

	int start = 0;
	int end = 0;

	int suitableStartTime = -1;
//	if (prevOpEndTime >= machines[machineIndex].machineFreeTime) {
//		suitableStartTime = prevOpEndTime;
//		timeSlots[idx] = suitableStartTime;
//		return;
//	}

	while (i != timeSlotsSize) {

		while (machines[machineIndex].timeSlots[i] != 0) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		start = i;

		if (i == machines[machineIndex].machineFreeTime) {
			timeSlots[idx] = i;
			return;
		}

		while (machines[machineIndex].timeSlots[i] != 1) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		end = i;

		int timeSlotDuration = end - start;

		if (operationIndex == 0) {
			////printf("opIndex==0\n");
			if (timeSlotDuration >= currentOpDuration) {
				suitableStartTime = start;
				////printf("timeSlotDuration>=currentOpDuration\n");
				break;
			}
		} else {
			if (prevOpEndTime <= start) {
				////printf("prevOpEndTime<=start\n");
				if (timeSlotDuration >= currentOpDuration) {
					suitableStartTime = start;
					////printf("timeSlotDuration>=currentOpDuration\n");
					break;
				}
			}

			else if (prevOpEndTime >= start && prevOpEndTime <= end) {
				////printf("prevOpEndTime >= start && prevOpEndTime <= end\n");

				if (end - prevOpEndTime >= currentOpDuration) {
					suitableStartTime = prevOpEndTime;
					////printf("end - prevOpEndTime >= currentOpDuration\n");
					break;
				}
			}
		}

	}

//	int start = 0;
//	int end = 0;
//	int i = 0;
//
//	int suitableStartTime = -1;

//	while (i != timeSlotsSize) {
//		while (machines[machineIndex].timeSlots[i] != 0) {
//			i += 1;
//			if (i == timeSlotsSize)
//				break;
//		}
//
//		start = i;
//
//		if (i == timeSlotsSize)
//			break;
//
//		while (machines[machineIndex].timeSlots[i] != 1) {
//			i += 1;
//			if (i == timeSlotsSize)
//				break;
//		}
//
//		end = i;
//
//		int timeSlotDuration = end - start;
//
//		if (operationIndex == 0) {
//			////printf("opIndex==0\n");
//			if (timeSlotDuration >= currentOpDuration) {
//				suitableStartTime = start;
//				////printf("timeSlotDuration>=currentOpDuration\n");
//				break;
//			}
//		} else {
//			if (prevOpEndTime <= start) {
//				////printf("prevOpEndTime<=start\n");
//				if (timeSlotDuration >= currentOpDuration) {
//					suitableStartTime = start;
//					////printf("timeSlotDuration>=currentOpDuration\n");
//					break;
//				}
//			}
//
//			else if (prevOpEndTime >= start && prevOpEndTime <= end) {
//				////printf("prevOpEndTime >= start && prevOpEndTime <= end\n");
//
//				if (end - prevOpEndTime >= currentOpDuration) {
//					suitableStartTime = prevOpEndTime;
//					////printf("end - prevOpEndTime >= currentOpDuration\n");
//					break;
//				}
//			}
//		}
//	}

	timeSlots[idx] = suitableStartTime;
}

__global__ void get_free_time_slot_local(Machine* machines, int* eligibleMachineIndices, int operationIndex,
		int currentOpDuration, int prevOpEndTime, int* timeSlotsLocal) {

	int idx = threadIdx.x;
	int machineIndex = eligibleMachineIndices[idx];

	if (operationIndex != 0) {
		if (prevOpEndTime >= machines[machineIndex].machineFreeTime) {
			timeSlotsLocal[idx] = prevOpEndTime;
		} else {
			timeSlotsLocal[idx] = machines[machineIndex].machineFreeTime;
		}
	} else {
		timeSlotsLocal[idx] = machines[machineIndex].machineFreeTime;
	}
}

//	//printf("suitableStartTime = %d  machineIndex = %d\n",suitableStartTime,machineIndex);

__device__ int mapOperationToMachine(int machineIndex, int jobIndex, int operationIndex, int startTime,
		Machine* machines, Job* jobs, int* machineScheduledOperationIndex, int*sem) {

	Operation&currentOperation = jobs[jobIndex].operationsPtr[operationIndex];
//	int startTime = currentOperation.operationStartTime;
	Machine& eligibleMachine = machines[machineIndex];

	if (sem[machineIndex] == 1) {
		//printf("%d sem not free, returning from Thread%d\n", atomicAdd(&countVar, 1),
		//blockDim.x * blockIdx.x + threadIdx.x);
		return 1; //recompute
	}
	acquire_semaphore(&sem[machineIndex]);
	//printf("%d M%d Thread%d Op%d acquired semaphore\n", atomicAdd(&countVar, 1), machineIndex, jobIndex,
	//operationIndex);

	if (eligibleMachine.timeSlots[startTime] == 1) {
		release_semaphore(&sem[machineIndex]);
		//printf("%d %d time slot not free, MFT:%d, returning from Thread%d\n", atomicAdd(&countVar, 1), startTime,
		//eligibleMachine.machineFreeTime, blockDim.x * blockIdx.x + threadIdx.x);

		return 1; //recompute
	}

//map operation to machine

	currentOperation.operationStartTime = startTime;
	currentOperation.operationEndTime = startTime + currentOperation.operationDuration;
	currentOperation.allotedMachine = eligibleMachine.machineID;

	for (int i = startTime; i < startTime + currentOperation.operationDuration; i++) {
		eligibleMachine.timeSlots[i] = 1;
	}

	eligibleMachine.scheduledOperations[machineScheduledOperationIndex[machineIndex]].jobPtr = &jobs[jobIndex];

	//printf("%d M%d schedOp%d jobPtr %p\n", atomicAdd(&countVar, 1), machineIndex,
	//machineScheduledOperationIndex[machineIndex],
	//eligibleMachine.scheduledOperations[machineScheduledOperationIndex[machineIndex]].jobPtr);

	eligibleMachine.scheduledOperations[machineScheduledOperationIndex[machineIndex]].operationPtr =
			&jobs[jobIndex].operationsPtr[operationIndex];

	//printf("%d M%d schedOp%d opPtr %p\n", atomicAdd(&countVar, 1), machineIndex,
	//machineScheduledOperationIndex[machineIndex],
	//eligibleMachine.scheduledOperations[machineScheduledOperationIndex[machineIndex]].operationPtr);

	eligibleMachine.machineFreeTime = currentOperation.operationEndTime;

	//printf("%d Thread%d Op%d mapped to M%d from %d to %d\n", atomicAdd(&countVar, 1), jobIndex, operationIndex,
	//machineIndex, currentOperation.operationStartTime, currentOperation.operationEndTime);

	atomicAdd(&(machineScheduledOperationIndex[machineIndex]), 1);
//end mapping}

	release_semaphore(&sem[machineIndex]);

	//printf("%d M%d Thread%d Op%d released semaphore\n", atomicAdd(&countVar, 1), machineIndex, jobIndex,
	//operationIndex);

	return 0;	//mapped successfully

}

__global__ void execute_JSS(Job* jobs, Machine* machines, int jobSize, int machineSize,
		int* machineScheduledOperationIndex, int*sem) {

//	sem = new int[machineSize];
//	for (int i = 0; i < machineSize; i++) {
//		sem[i] = 0;
//	}

	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < jobSize) {
		for (int op = 0; op < jobs[idx].operationsSize; op++) {

			Job& job = jobs[idx];
			Operation& currentOperation = job.operationsPtr[op];
			Operation& previousOperation = job.operationsPtr[op - 1];

			////////////////////////////////////////////////////////////////////////////////
			if (currentOperation.mappable == true) {

				int returnA = 1;
				while (returnA) {

					int* timeSlotsLocal = new int[currentOperation.eligibleMachinesSize];

					for (int i = 1; i < currentOperation.eligibleMachinesSize; i++) {
						timeSlotsLocal[i] = 0;
					}

					if (op == 0) {
						get_free_time_slot_optimized<<<1, currentOperation.eligibleMachinesSize>>>(machines,
								currentOperation.eligibleMachinesIndices, op, currentOperation.operationDuration, 0,
								timeSlotsLocal);
					}

					else {

						get_free_time_slot_optimized<<<1, currentOperation.eligibleMachinesSize>>>(machines,
								currentOperation.eligibleMachinesIndices, op, currentOperation.operationDuration,
								previousOperation.operationEndTime, timeSlotsLocal);
					}

					cudaDeviceSynchronize();

					int minIdx = 0;
					for (int i = 0; i < currentOperation.eligibleMachinesSize; i++) {
						if (timeSlotsLocal[i] < timeSlotsLocal[minIdx])
							minIdx = i;
					}

					int eligibleMachineIndex = currentOperation.eligibleMachinesIndices[minIdx];

					int start = timeSlotsLocal[minIdx];

					delete timeSlotsLocal;

					if (op == 0) {
						returnA = mapOperationToMachine(eligibleMachineIndex, idx, op, start, machines, jobs,
								machineScheduledOperationIndex, sem);
					}

					else {
						returnA = mapOperationToMachine(eligibleMachineIndex, idx, op, start, machines, jobs,
								machineScheduledOperationIndex, sem);
					}

				}

//				if (op == 0) {
//
//					Operation* nullPtr = NULL;
//
//					int returnA = 1;
//					while (returnA) {
//
//						for (int i = 1; i < currentOperation.eligibleMachinesSize; i++) {
//							timeSlotsLocal[i] = 0;
//						}
//
//						get_free_time_slot_optimized<<<1, currentOperation.eligibleMachinesSize>>>(machines,
//								currentOperation.eligibleMachinesIndices, op, currentOperation.operationDuration, 0,
//								timeSlotsLocal);
//
//						cudaDeviceSynchronize();
//
//						int minIdx = 0;
//						for (int i = 0; i < currentOperation.eligibleMachinesSize; i++) {
//							if (timeSlotsLocal[i] < timeSlotsLocal[minIdx])
//								minIdx = i;
//						}
//
//						int eligibleMachineIndex = currentOperation.eligibleMachinesIndices[minIdx];
//
//						int start = timeSlotsLocal[minIdx];
//
//						returnA = mapOperationToMachine(eligibleMachineIndex, idx, start, &(job.operationsPtr[op]),
//						NULL, machines, jobs, op, &(machines[currentOperation.eligibleMachinesIndices[minIdx]]),
//								machineScheduledOperationIndex, sem);
//
//					}
//
//				}
//
//				else {
//					int returnA = 1;
//					while (returnA) {
//
//						for (int i = 1; i < currentOperation.eligibleMachinesSize; i++) {
//							timeSlotsLocal[i] = 0;
//						}
//
//						get_free_time_slot_local<<<1, currentOperation.eligibleMachinesSize>>>(machines,
//								currentOperation.eligibleMachinesIndices, op, currentOperation.operationDuration,
//								previousOperation.operationEndTime, timeSlotsLocal);
//
//						cudaDeviceSynchronize();
//						int minIdx = 0;
//						for (int i = 0; i < currentOperation.eligibleMachinesSize; i++) {
//							if (timeSlotsLocal[i] < timeSlotsLocal[minIdx]) {
//								minIdx = i;
//							}
//						}
//
//						//					Machine& eligibleMachine = machines[currentOperation.eligibleMachinesIndices[minIdx]];
//						int eligibleMachineIndex = currentOperation.eligibleMachinesIndices[minIdx];
//
//						int start = timeSlotsLocal[minIdx];
//
//						returnA = mapOperationToMachine(eligibleMachineIndex, idx, start, &(job.operationsPtr[op]),
//								&(job.operationsPtr[op - 1]), machines, jobs, op,
//								&(machines[currentOperation.eligibleMachinesIndices[minIdx]]),
//								machineScheduledOperationIndex, sem);
//
//						//printf("returnA = %d\n", returnA);
//					}
//
//				}

			}

			else {
				//continue;

//
				Machine& eligibleMachine = machines[currentOperation.eligibleMachinesIndices[0]];
				//printf("%d Thread%d M%d\n", atomicAdd(&countVar, 1), idx, currentOperation.eligibleMachinesIndices[0]);

				int eligibleMachineIndex = currentOperation.eligibleMachinesIndices[0];

				acquire_semaphore(&sem[eligibleMachineIndex]);
				//printf("%d semaphore acquired by M%d\n", atomicAdd(&countVar, 1), eligibleMachineIndex);

				int start = 0;
				if (op != 0) {
					start = previousOperation.operationEndTime;

				}

				currentOperation.operationStartTime = start;
				currentOperation.operationEndTime = start + currentOperation.operationDuration;
				currentOperation.allotedMachine = eligibleMachine.machineID;

				eligibleMachine.scheduledOperations[machineScheduledOperationIndex[eligibleMachineIndex]].jobPtr =
						&jobs[idx];

				eligibleMachine.scheduledOperations[machineScheduledOperationIndex[eligibleMachineIndex]].operationPtr =
						&jobs[idx].operationsPtr[op];

				atomicAdd(&(machineScheduledOperationIndex[eligibleMachineIndex]), 1);

				release_semaphore(&sem[eligibleMachineIndex]);
				//printf("%d semaphore released by M%d\n", atomicAdd(&countVar, 1), eligibleMachineIndex);

			}

			////////////////////////////////////////////////////////////////////////////////////////////

//			delete timeSlotsLocal;
		}
	}
}

void kernel_wrapper(Job** jobs, Machine** machines, int jobSize, int machineSize, int** machineScheduledOperationIndex,
		int*sem) {

//	int *sem = new int[machineSize];
//	for (int i = 0; i < machineSize; i++) {
//		sem[i] = 0;
//	}

//	sem = new int[machineSize];
//	for (int i = 0; i < machineSize; i++) {
//		sem[i] = 0;
//	}

//sem = new int[NUMBER_OF_STREAMS];

//	cudaStream_t streams[NUMBER_OF_STREAMS];
//
//	for (int i = 0; i < NUMBER_OF_STREAMS; i++) {
//		cudaStreamCreate(&streams[i]);
//		//sem[i] = 0;
//

//		// launch one worker kernel per stream
//		execute_JSS<<<jobSize, 1, 0, streams[i]>>>(jobs[i], machines[i], jobSize, machineSize,
//				machineScheduledOperationIndex[i]);
//
////	}

	execute_JSS<<<jobSize, 1>>>(jobs[0], machines[0], jobSize, machineSize, machineScheduledOperationIndex[0], sem);

	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());
}

