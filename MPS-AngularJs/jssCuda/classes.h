#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <random>
#include <chrono>
#include "kernel.cuh"
#include "device_functions.h"

using namespace std;

class Operation;
class Job;
class Machine;
class Tuple;
class ScheduledOperation;
class Managed;

const int timeSlotsSize = 2000;
const int scheduledOperationsSize = 20000;
extern int NUMBER_OF_STREAMS;


class Managed {
public:
	void* operator new(size_t len) {
		void* ptr;
		cudaMallocManaged(&ptr, len);
		cudaDeviceSynchronize();
		return ptr;
	}

	void operator delete(void* ptr) {
		cudaDeviceSynchronize();
		cudaFree(ptr);
	}
};

class String: public Managed {
	int length;
	char* data;
public:
	String() :
			length(0), data(0) {
	}

	// Constructor for C-string initializer
	String(const char* s) :
			length(0), data(0) {
		_realloc(strlen(s));
		strcpy(data, s);
	}

	// Copy constructor
	String(const String& s) :
			length(0), data(0) {
		_realloc(s.length);
		strcpy(data, s.data);
	}

	~String() {
		cudaFree(data);
	}

	// Assignment operator
	String& operator=(const char* s) {
		_realloc(strlen(s));
		strcpy(data, s);
		return *this;
	}

	// Element access (from host or device)
	__host__ __device__
	char& operator[](int pos) {
		return data[pos];
	}

	// C-string access
	__host__ __device__
	const char* c_str() const {
		return data;
	}

private:
	void _realloc(int len) {
		cudaFree(data);
		length = len;
		cudaMallocManaged(&data, length + 1);
	}
};

class Operation: public Managed {

public:
	string operationCode;
	int operationDuration;
	int operationSequence;
	int operationStartTime;
	int operationEndTime;
	int operationIndex;
	//Job *jobPtr;
	int* eligibleMachinesIndices;
	vector<int> eligibleMachinesVector;
	int eligibleMachinesSize;
	const char* operationCodeChar;
	int allotedMachine;
	bool mappable;

	__device__ __host__ void clear();

	__device__ __host__ Operation(string operationCode, int operationDuration, int operationSequence, bool mappable);

	__device__ __host__ Operation() {
	}

	__device__ __host__ void initializeEligibleMachinesIndicesArray(int size);
	friend bool operator==(const Operation& o1, const Operation& o2);

	Operation(const Operation& operation) {
		operationDuration = operation.operationDuration;
		operationCode = operation.operationCode;
		operationSequence = operation.operationSequence;
		operationStartTime = operation.operationStartTime;
		operationEndTime = operation.operationEndTime;
		eligibleMachinesIndices = operation.eligibleMachinesIndices;
		eligibleMachinesSize = operation.eligibleMachinesSize;
		eligibleMachinesVector = operation.eligibleMachinesVector;
		operationCodeChar = operationCode.c_str();
		allotedMachine = operation.allotedMachine;
		operationIndex = operation.operationIndex;
		mappable = operation.mappable;
	}

};

class ScheduledOperation: public Managed {
public:
	Job* jobPtr;
	Operation* operationPtr;

	__device__ __host__ ScheduledOperation(Job* job, Operation* operation);__device__ __host__ ScheduledOperation() {
		jobPtr = NULL, operationPtr = NULL;
	}

	friend bool operator==(const ScheduledOperation& o1, const ScheduledOperation& o2);

};

class Tuple: public Managed {
public:
	int start;
	int end;

	__device__ __host__ Tuple(int start, int end);__device__ __host__ Tuple() {
		start = 0;
		end = 0;
	}

};

class Machine: public Managed {
public:
	int machineID;
	string operationCode;
	string machineName;
	int machineFreeTime;
	//vector<ScheduledOperation> scheduledOperations;

	int timeSlots[timeSlotsSize] = { 0 };
	bool* timeSlotsPtr;
	Tuple usableTimeSlot;
	ScheduledOperation scheduledOperations[scheduledOperationsSize] = { ScheduledOperation() };
	int scheduledOperationIndex = 0;

	__device__ __host__ void clear();__device__ __host__ Machine(int machineID, string operationCode,
			string machineName);

	__device__ __host__ Machine() {

	}

	__device__ __host__ vector<Tuple> getFreeTimeSlots();__device__ __host__ Tuple* getFreeTimeSlotsPtr();

	friend bool operator==(const Machine& m1, const Machine& c2);

	Machine(const Machine& machine) {
		machineName = machine.machineName;
		machineID = machine.machineID;
		operationCode = machine.operationCode;
		machineFreeTime = machine.machineFreeTime;
		//scheduledOperations = machine.scheduledOperations;
		for (int i = 0; i < timeSlotsSize; i++) {
			timeSlots[i] = machine.timeSlots[i];

		}

		for (int i = 0; i < scheduledOperationsSize; i++) {
			scheduledOperations[i].jobPtr = machine.scheduledOperations[i].jobPtr;
			scheduledOperations[i].operationPtr = machine.scheduledOperations[i].operationPtr;
		}

	}

};

class Job: public Managed {
public:
	int jobIndex;
	int jobCode;
	vector<Operation> operations;
	int unscheduledOperationIndex;
	Operation* operationsPtr;
	int operationsSize;

	__device__ __host__ Job(int jobCode);

	__device__ __host__ void clear();

	__device__ __host__ void addOperation(Operation operation);
	friend bool operator==(const Job& j1, const Job& j2);

	Job(const Job& job) {
		jobCode = job.jobCode;

		/*for (int i = 0; i < operations.size();i++) {
		 operations[i] = *(new Operation(job.operations[i]));

		 }*/
		operations = job.operations;
		unscheduledOperationIndex = job.unscheduledOperationIndex;
		operationsPtr = job.operationsPtr;
		jobIndex = job.jobIndex;
	}

};
