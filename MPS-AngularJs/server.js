var express = require('express');
var app = express();
var path = require('path');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var fs = require('fs');

const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

var PolynomialRegression =require('ml-regression-polynomial');//for polynomial regression

// var MqttHandler=require('./sendCurrentData')
// var mqttClient = new MqttHandler()
// mqttClient.connect()

var currentData = [0];
var currentInstanceCounter = 0;
var xArr=[0],yArr=[0];//data holders

const db=require('./models/DB');

const port = process.env.PORT || 9999;

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.use(cors());

app.use(express.static(__dirname + '/public'));


var server = app.listen(port, function () {
  console.log('server running on port # ', port);
});

// ////////////socket-io stuff///////////////////
var io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) {

  // console.log('a client is added')

  // socket.on("disconnect",()=>{
  // 	console.log('A user disconnected')
  // })

  socket.on('currentDataFromMqtt', function (data) {
    console.log(data)
    socket.emit('currentDataFromServer', data)
  })
});
// //////////////////////////////////////////////

// //////////////mqtt stuff//////////////////////
var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost:1884');
client.options.reconnectPeriod = 0;

client.on('connect', function () {
  client.subscribe('nodeData', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt')
    }
  });
});

client.on('message', function (topic, message) {

  // message is Buffer
  //   console.log(message.toString())
  var i = message.toString().indexOf(',')
  if (i === null || i === undefined || i === -1) {
    return;
  };
  var j = message.toString().indexOf(',', i + 1);
  if (j === null || j === undefined || j === -1) {
    return;
  };
  //   console.log(i,j)
  var current = Number.parseFloat(message.toString().slice(i + 1, j));
  // console.log('current = ',current)
  currentData.push(current);
  currentInstanceCounter = currentData.length - 1;
  xArr.push(currentInstanceCounter);
  // currentInstance=current
  // io.sockets.emit("currentDataFromMqtt",current)

});
//////////////////////////////////////////////////////////////
/////////DB stuff/////////////////////////////////////////////


const employeeRoute=require('./routes/employeeRoute');
app.use('/employeeRoute',employeeRoute);
const inventoryRoute=require('./routes/inventoryRoute');
app.use('/inventoryRoute',inventoryRoute);
const jobsRoute=require('./routes/jobsRoute');
app.use('/jobsRoute',jobsRoute);
const workerRoute=require('./routes/workerRoute');
app.use('/workerRoute',workerRoute);
const machineRoute=require('./routes/machineRoute');
app.use('/machineRoute',machineRoute);
const jobStatusRoute=require('./routes/jobStatusRoute');
app.use('/jobStatusRoute',jobStatusRoute);


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

app.get('/hr', (req, res) => {
  console.log('getting hr req ')
  res.sendFile(path.join(__dirname + '/public/index.html'))
})
app.get('/home', (req, res) => {
  console.log('getting home req ')
  res.sendFile(path.join(__dirname + '/public/index.html'))
})
// app.get('/',(req,res)=>{
//     console.log('getting root req ')
//     res.sendFile(path.join(__dirname + '/public/index.html'))
// })
app.get('/sales', (req, res) => {
  console.log('getting sales req ')
  res.sendFile(path.join(__dirname + '/public/index.html'))
})
app.get('/inventory', (req, res) => {
  console.log('getting inventory req ')
  res.sendFile(path.join(__dirname + '/public/index.html'))
})
app.get('/currentAnalytics', (req, res) => {
  console.log('getting currentAnalytics req ')
  res.sendFile(path.join(__dirname + '/public/index.html'))
})
app.get('/newOrders', (req, res) => {
  console.log('getting newOrders req ')
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/reports', (req, res) => {
  console.log('getting reports req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/mps', (req, res) => {
  console.log('getting mps req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/factory', (req, res) => {
  console.log('getting factory req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/machines', (req, res) => {
  console.log('getting machines req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/jobs', (req, res) => {
  console.log('getting machines req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/profiling', (req, res) => {
  console.log('getting profiling req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
app.get('/worker', (req, res) => {
  console.log('getting profiling req ');
  res.sendFile(path.join(__dirname + '/public/index.html'));
});
// ////////////////////////////////////////////////////////////

app.post('/getMakespanForMPS', (req, res) => {
  // console.log(req.body)
  fs.writeFile('../inputToJss.json', JSON.stringify(req.body), 'utf8', function (err) {
    if (err) {
      res.end();
      throw err;
    }else {
      console.log('file created')

      exec('cp ../inputToJss.json ./', (error, stdOut, stdErr) => {

        if (stdOut) {
          // console.log(stdout.toString())
          console.log('ended operation')
          res.send(stdOut)
        }
        if (error) {
          console.log(error)
          console.log('ended operation')
          res.json(error)
        }else {
          res.json(req.body)
        }
        if (stdErr) {
          console.log(stdErr)
          console.log('ended operation')
          res.json(stdErr)
        }
      })
    }
  })

// res.end()
});

app.post('/buildJss', (req, res) => {

  console.log(req.body)

  exec('g++ *.cpp -o executableRun -fopenmp', (error, stdOut, stdErr) => {

    if (stdOut) {
      // console.log(stdout.toString())
      console.log('ended operation')
      res.send(stdOut)
    }
    if (error) {
      console.log(error)
      console.log('ended operation')
      res.json(error)
    }
    if (!error) {
      // res.json(req.body)
      res.send('build finished')
    }
    if (stdErr) {
      console.log(stdErr)
      console.log('ended operation')
      res.json(stdErr)
    }
  })
});

// ////////////////////////////////////////////////////////////

app.post('/generateGantt', (req, res) => {

  console.log('gonna execute csv2json')

  const subprocess = spawn('python3', ['./csvToJson.py'])
  subprocess.stdout.on('data', function (data) {
    console.log(data.toString())
    if (data.length > 50) {
      // res.sendFile(path.join(__dirname + '/temp-plot.html'))
      // res.end()
      res.sendFile(path.join(__dirname + '/public/index.html'))
    }
  //        res.write(data)
  //        res.end('end')
  })
});

app.get('/getGantt', (req, res) => {
  res.sendFile(path.join(__dirname + '/temp-plot.html'))
//    res.end()
});

app.post('/runJss', (req, res) => {

  console.log('executing JSS')

  // exec('cd jss',function(err,stdO,stdErr){})

  exec('./executableRun', function (error, stdout, stderr) {

    // res.setHeader('Content-Type', 'text/plain')
    if (stdout) {
      // console.log(stdout.toString())
      console.log('ended JSS')
      res.send(stdout)
    }
    if (error) {
      console.log(error)
      console.log('ended JSS')
      res.json('error')
    }
    if (stderr) {
      console.log(stderr)
      console.log('ended JSS')
      // res.json(stderr)
      res.json('error')
    }
  //    res.end();   
  })
});

// for sending current values to angularjs frontend
app.post('/currentData', (req, res) => {

  ///////polynomial regression////////

  // console.log('index = ',req.body.index);
  var degree = 10; // setup the maximum degree of the polynomial
  
  // x=-200;
  // for(let i=0;i<10;i++){
  //     // y=Math.random();
  //     y=0.65;
  //     x=x+200;
      // xArr.push(req.body.index);
  //     yArr.push(y);    
  // }
  
  // console.log(xArr,'\n',yArr)
  
  var regression = new PolynomialRegression(xArr, currentData, degree);
  var predictedVal=Math.abs(regression.predict(currentInstanceCounter)).toFixed(2);
  console.log('predicted = ',predictedVal);

  ////////////////////////////////////
  res.send({currentData,currentInstanceCounter,predictedVal})
// console.log(req.body.index)
// var temp=currentData
// res.send(currentInstance)
// res.send(100)
// res.end()
});
