#pragma once
#include <vector>
#include "classes.h"

using namespace std;



template <typename T>
int getIndex(const std::vector<T>& vec, const T& obj) {
	return (distance(vec.begin(), find(vec.begin(), vec.end(), obj)));
}

template <typename T>
void printVector(vector<T>& vec) {
	for (auto i : vec) {
		cout << i << " ";
	}
	cout << endl;
}

template <typename T>
void printChromosome(vector<T>& vec) {
	for (auto i : vec) {
		cout << i << ",";
	}
	cout << endl;
}

template <class Container>
std::ostream& write_container(const Container& c,
	std::ostream& out,
	char delimiter = ',') {
	bool write_sep = false;
	for (const auto& e : c) {
		if (write_sep)
			out << delimiter;
		else
			write_sep = true;
		out << e;
	}
	out << endl;
	return out;
}

void writeCSVsortedByOperation(vector<Job>& jobs);

void writeCSVsortedByMachine(vector<Machine>& machines);

void writeCSVFile(Machine* ma, Job* ja, int machineSize, int* machineScheduledOperationIndex);