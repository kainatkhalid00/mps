import csv
import os
import random
from pprint import pprint
import pandas
import pendulum
import time


def timeit(method):
    def timed(*args, **kw):
        ts = time.clock()
        result = method(*args, **kw)
        te = time.clock()

        print('%r  %3.5f ms' % (method.__name__, (te - ts) * 1000))
        return result

    return timed


def write_to_excel_and_save_image(makespan, machines, jobs, filename):
    import xlsxwriter
    current_dir = os.path.dirname(os.path.realpath(__file__))
    file_xlsx = current_dir + "\\" + filename + ".xlsx"

    workbook = xlsxwriter.Workbook(file_xlsx)
    worksheet = workbook.add_worksheet()

    mf1 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#1abc9c'})

    mf2 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#D2527F'})

    mf3 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#FABE58'})

    mf4 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#81CFE0'})

    mf5 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#BE90D4'})

    mf6 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#FDE3A7'})

    mf7 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#ECECEC'})

    mf8 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#6C7A89'})

    mf9 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'orange'})

    mf10 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'pink'})

    mf11 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#1abc9c'})

    mf12 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#D2527F'})

    mf13 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#FABE58'})

    mf14 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#81CFE0'})

    mf15 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#BE90D4'})

    mf16 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#FDE3A7'})

    mf17 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#ECECEC'})

    mf18 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#6C7A89'})

    mf19 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'orange'})

    mf20 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'pink'})

    mf21 = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'pink'})

    mf = [mf1, mf2, mf3, mf4, mf5, mf6, mf7, mf8, mf9, mf10, mf11, mf12, mf13, mf14, mf15, mf16, mf17, mf18, mf19, mf20,
          mf21, mf1, mf2, mf3, mf4, mf5, mf6, mf7, mf8, mf9, mf10, mf11, mf12, mf13, mf14, mf15, mf16, mf17, mf18, mf19,
          mf20,
          mf21, mf1, mf2, mf3, mf4, mf5, mf6, mf7, mf8, mf9, mf10, mf11, mf12, mf13, mf14, mf15, mf16, mf17, mf18, mf19,
          mf20,
          mf21, mf1, mf2, mf3, mf4, mf5, mf6, mf7, mf8, mf9, mf10, mf11, mf12, mf13, mf14, mf15, mf16, mf17, mf18, mf19,
          mf20,
          mf21, mf1, mf2, mf3, mf4, mf5, mf6, mf7, mf8, mf9, mf10, mf11, mf12, mf13, mf14, mf15, mf16, mf17, mf18, mf19,
          mf20,
          mf21]

    for i in mf:
        i.set_font_size(9)

    base_datetime = pendulum.datetime(2018, 1, 1, 8, 0, 0, 0)

    date_format_shift_1 = workbook.add_format(
        {'font_size': 9, 'align': 'left', 'rotation': 90, 'bg_color': '#F08080', 'border': 1,

         })
    date_format_shift_2 = workbook.add_format(
        {'font_size': 9, 'align': 'left', 'rotation': 90, 'bg_color': '#87CEFA', 'border': 1,

         })
    date_format_shift_3 = workbook.add_format(
        {'font_size': 9, 'align': 'left', 'rotation': 90, 'bg_color': '#98FB98', 'border': 1,

         })

    shifts = []
    shifts.append(date_format_shift_1)
    shifts.append(date_format_shift_2)
    shifts.append(date_format_shift_3)

    machine_format = workbook.add_format(
        {'font_size': 8, 'align': 'left', 'bg_color': '#DCDCDC', 'border': 1,
         })

    for i in range(makespan + 1):
        worksheet.write(0, i + 1, base_datetime.add(hours=i).format('Do MMM YYYY, HH:mm'),
                        shifts[int(base_datetime.add(hours=i).hour / 8)]
                        )

    for machineIdx, machine in enumerate(machines):
        worksheet.write(machineIdx + 1, 0,
                        "M" + str(machineIdx) + ":" + machines[machineIdx].machine_name + "(" + machines[
                            machineIdx].machine_operation_code + ")", machine_format)

        for scheduled_operation in machine.scheduled_operations:

            job = scheduled_operation.job
            operation = scheduled_operation.operation
            jobIndex = jobs.index(job)
            operationIndex = job.operations.index(operation)

            start_time = operation.operation_start_time
            end_time = operation.operation_end_time

            if (end_time - start_time == 1):
                worksheet.write(machineIdx + 1, start_time + 1, "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                                mf[jobIndex])
            else:
                worksheet.merge_range(machineIdx + 1, start_time + 1, machineIdx + 1, end_time,
                                      "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                                      mf[jobIndex])

    worksheet.set_column(1, makespan + 1, 5)
    worksheet.set_column(0, 0, 18)

    try:
        workbook.close()
    except:
        # Handle your exception here.
        print("Couldn't create xlsx file")
        # time.sleep(5)


def write_to_excel_and_save_image_new(makespan, machines, jobs, filename):
    import xlsxwriter
    current_dir = os.path.dirname(os.path.realpath(__file__))
    file_xlsx = current_dir + "\\" + filename + ".xlsx"

    workbook = xlsxwriter.Workbook(file_xlsx)
    worksheet = workbook.add_worksheet()

    formats = []
    for i in range(100):
        formats.append(workbook.add_format({
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': "#%06x" % random.randint(0, 0xFFFFFF)}))

    for format in formats:
        format.set_font_size(9)

    base_datetime = pendulum.datetime(2018, 1, 1, 8, 0, 0, 0)

    date_format_shift_1 = workbook.add_format(
        {'font_size': 9, 'align': 'left', 'rotation': 90, 'bg_color': '#F08080', 'border': 1,

         })
    date_format_shift_2 = workbook.add_format(
        {'font_size': 9, 'align': 'left', 'rotation': 90, 'bg_color': '#87CEFA', 'border': 1,

         })
    date_format_shift_3 = workbook.add_format(
        {'font_size': 9, 'align': 'left', 'rotation': 90, 'bg_color': '#98FB98', 'border': 1,

         })

    shifts = []
    shifts.append(date_format_shift_1)
    shifts.append(date_format_shift_2)
    shifts.append(date_format_shift_3)

    machine_format = workbook.add_format(
        {'font_size': 8, 'align': 'left', 'bg_color': '#DCDCDC', 'border': 1,
         })

    for i in range(makespan + 1):
        worksheet.write(0, i + 1, base_datetime.add(hours=i).format('Do MMM YYYY, HH:mm'),
                        shifts[int(base_datetime.add(hours=i).hour / 8)]
                        )

    rowIndex = 1
    machineRowStartIndex = []
    for machineIdx, machine in enumerate(machines):

        # print("MachineIndex:{},machineId:{}".format(machineIdx, machine.machine_id))

        machineRowStartIndex.append(rowIndex)
        # print(machineRowStartIndex)
        if (machine.mappable == False):

            if (len(machine.scheduled_operations) == 0):
                worksheet.write(rowIndex, 0,
                                "M" + str(machine.machine_id) + ":" + machine.machine_name +
                                "(" + str(machine.machine_operation_code) + ")", machine_format)
                rowIndex += 1

            else:
                for index, scheduled_op in enumerate(machine.scheduled_operations):
                    worksheet.write(rowIndex, 0,
                                    "M" + str(machine.machine_id) + ":" + machine.machine_name +
                                    "(" + str(machine.machine_operation_code) + ")", machine_format)

                    job = scheduled_op.job
                    operation = scheduled_op.operation
                    jobIndex = jobs.index(job)
                    operationIndex = job.operations.index(operation)
                    start_time = operation.operation_start_time
                    end_time = operation.operation_end_time
                    if (end_time - start_time == 1):
                        worksheet.write(rowIndex, start_time + 1, "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                                        formats[jobIndex])
                    else:
                        worksheet.merge_range(rowIndex, start_time + 1, rowIndex, end_time,
                                              "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                                              formats[jobIndex])


                    # for scheduled_operation in machine.scheduled_operations:
                    #     job = scheduled_operation.job
                    #     operation = scheduled_operation.operation
                    #     jobIndex = jobs.index(job)
                    #     operationIndex = job.operations.index(operation)
                    #     start_time = operation.operation_start_time
                    #     end_time = operation.operation_end_time
                    #     if (end_time - start_time == 1):
                    #         worksheet.write(rowIndex, start_time + 1, "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                    #                         formats[jobIndex])
                    #     else:
                    #         worksheet.merge_range(rowIndex, start_time + 1, rowIndex, end_time,
                    #                               "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                    #                               formats[jobIndex])

                    rowIndex += 1




        else:

            worksheet.write(rowIndex, 0,
                            "M" + str(machine.machine_id) + ":" + machine.machine_name +
                            "(" + str(machine.machine_operation_code) + ")", machine_format)

            for scheduled_operation in machine.scheduled_operations:
                job = scheduled_operation.job
                operation = scheduled_operation.operation
                jobIndex = jobs.index(job)
                operationIndex = job.operations.index(operation)
                start_time = operation.operation_start_time
                end_time = operation.operation_end_time
                if (end_time - start_time == 1):
                    worksheet.write(rowIndex, start_time + 1, "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                                    formats[jobIndex])
                else:
                    worksheet.merge_range(rowIndex, start_time + 1, rowIndex, end_time,
                                          "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
                                          formats[jobIndex])

            rowIndex += 1

        # print(machineRowStartIndex)
        # rowIdx = machineRowStartIndex[machineIdx]

        # for index, scheduled_operation in enumerate(machine.scheduled_operations):
        #
        #     job = scheduled_operation.job
        #     operation = scheduled_operation.operation
        #     jobIndex = jobs.index(job)
        #     operationIndex = job.operations.index(operation)
        #
        #     start_time = operation.operation_start_time
        #     end_time = operation.operation_end_time
        #
        #     # if (machine.mappable == False):
        #
        #     if (end_time - start_time == 1):
        #         worksheet.write(rowIdx, start_time + 1, "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
        #                         formats[jobIndex])
        #     else:
        #         worksheet.merge_range(rowIdx, start_time + 1, rowIdx, end_time,
        #                               "J" + str(jobIndex) + "_" + "O" + str(operationIndex),
        #                               formats[jobIndex])
        #
        #     rowIndex += 1

    worksheet.set_column(1, makespan + 1, 5)
    worksheet.set_column(0, 0, 18)

    try:
        workbook.close()
    except:
        # Handle your exception here.
        print("Couldn't create xlsx file")
        # time.sleep(5)


def write_csv(machines, jobs, filename):
    outfile = open(filename, 'w', newline='')
    writer = csv.writer(outfile)
    writer.writerow(['Job', 'Op', 'EligMach', 'ST', 'ET', 'Index'])

    for machine in machines:

        for scheduled_operation in machine.scheduled_operations:
            writer.writerow(
                [jobs.index(scheduled_operation.job),
                 jobs[jobs.index(scheduled_operation.job)].operations.index(scheduled_operation.operation),
                 machines.index(machine),
                 scheduled_operation.operation.operation_start_time,
                 scheduled_operation.operation.operation_end_time]
            )


def write_csv_sorted_by_machine(machines, jobs, filename):
    outfile = open(filename, 'w', newline='')
    writer = csv.writer(outfile)
    writer.writerow(['Job', 'Op', 'EligMach', 'OpCode', 'ST', 'ET'])

    mylist = []
    for machine in machines:

        for scheduled_operation in machine.scheduled_operations:
            mylist.append(
                [jobs.index(scheduled_operation.job),
                 jobs[jobs.index(scheduled_operation.job)].operations.index(scheduled_operation.operation),
                 machines.index(machine),
                 machine.machine_operation_code,
                 scheduled_operation.operation.operation_start_time,
                 scheduled_operation.operation.operation_end_time]
            )

    df = pandas.DataFrame(mylist, columns=['Job', 'Op', 'EligMach', 'OpCode', 'ST', 'ET'])
    df = df.sort_values(['EligMach', 'ST'])
    for row in df.iterrows():
        machine = row[1]['EligMach']
        op = row[1]['Op']
        job = row[1]['Job']
        opcode = row[1]['OpCode']
        ST = row[1]['ST']
        ET = row[1]['ET']

        writer.writerow([job, op, machine, opcode, ST, ET])


def write_csv_sorted_by_schedop(machines, jobs, filename):
    outfile = open(filename, 'w', newline='')
    writer = csv.writer(outfile)
    writer.writerow(['Job', 'Op', 'EligMach', 'OpCode', 'ST', 'ET'])

    mylist = []
    for machine in machines:

        for scheduled_operation in machine.scheduled_operations:
            mylist.append(
                [jobs.index(scheduled_operation.job),
                 jobs[jobs.index(scheduled_operation.job)].operations.index(scheduled_operation.operation),
                 machines.index(machine),
                 machine.machine_operation_code,
                 scheduled_operation.operation.operation_start_time,
                 scheduled_operation.operation.operation_end_time]
            )

    df = pandas.DataFrame(mylist, columns=['Job', 'Op', 'EligMach', 'OpCode', 'ST', 'ET'])
    # df = df.sort_values(['EligMach', 'ST'])
    for row in df.iterrows():
        machine = row[1]['EligMach']
        op = row[1]['Op']
        job = row[1]['Job']
        opcode = row[1]['OpCode']
        ST = row[1]['ST']
        ET = row[1]['ET']

        writer.writerow([job, op, machine, opcode, ST, ET])


def write_csv_sorted_by_operation(machines, jobs, filename):
    outfile = open(filename, 'w', newline='')
    writer = csv.writer(outfile)
    writer.writerow(['Job', 'Op', 'EligMach', 'OpCode', 'ST', 'ET'])

    mylist = []
    for machine in machines:

        for scheduled_operation in machine.scheduled_operations:
            mylist.append(
                [jobs.index(scheduled_operation.job),
                 jobs[jobs.index(scheduled_operation.job)].operations.index(scheduled_operation.operation),
                 machines.index(machine),
                 machine.machine_operation_code,
                 scheduled_operation.operation.operation_start_time,
                 scheduled_operation.operation.operation_end_time]
            )

    df = pandas.DataFrame(mylist, columns=['Job', 'Op', 'EligMach', 'OpCode', 'ST', 'ET'])
    df = df.sort_values(['Job', 'Op'])
    for row in df.iterrows():
        machine = row[1]['EligMach']
        op = row[1]['Op']
        job = row[1]['Job']
        opcode = row[1]['OpCode']
        ST = row[1]['ST']
        ET = row[1]['ET']

        writer.writerow([job, op, machine, opcode, ST, ET])


def write_to_df(machines, jobs):
    mylist = []
    for machine in machines:

        for scheduled_operation in machine.scheduled_operations:
            mylist.append(
                [jobs.index(scheduled_operation.job),
                 jobs[jobs.index(scheduled_operation.job)].operations.index(scheduled_operation.operation),
                 machines.index(machine),
                 scheduled_operation.operation.operation_start_time,
                 scheduled_operation.operation.operation_end_time]
            )

    df = pandas.DataFrame(mylist, columns=['Job', 'Op', 'EligMach', 'ST', 'ET'])
    # print(df)

    df = df.sort_values(['Job', 'Op'])
    print(df)
