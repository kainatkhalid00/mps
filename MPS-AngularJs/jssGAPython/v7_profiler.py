import csv
import time

# from job_shop_scheduler.v7_mappable 
import genetic_v7_mappable as v7

# print(v6.run_algo_profiler(5, 5))

file_name = time.strftime("%Y%m%d-%H%M%S CPU profiling") + ".csv"

outfile = open(file_name, 'w', newline='')
writer = csv.writer(outfile)
writer.writerow(['PopulationSize', 'Generations', 'Makespan', 'Time'])

popsize = [5]

generations = [1000,2000]

for pop in popsize:

    for generation in generations:

        for i in range(10):
            makespan, time = v7.run_algo_profiler(pop, generation)
            writer.writerow([pop, generation, makespan, time])
