import multiprocessing
from collections import OrderedDict
from functools import partial
from multiprocessing import Pool
import pendulum
import operator
import random
import math
import json
import time
from pathlib import Path
from line_profiler import *
from helperfunctions import *

_pool = None
# FILE_NAME = "42_one_qty.json"
FILE_NAME = "input.json"
# FILE_NAME = str((Path(__file__).parents[2])) + "/150.json"

INITIAL_POP_SIZE = 16

# totalExecutionTime=0


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' %
                  (method.__name__, (te - ts) * 1000))
        return result

    return timed


def fun(f, q_in, q_out):
    while True:
        i, x = q_in.get()
        if i is None:
            break
        q_out.put((i, f(x)))


def parmap(f, X, nprocs=multiprocessing.cpu_count()):
    q_in = multiprocessing.Queue(1)
    q_out = multiprocessing.Queue()

    proc = [multiprocessing.Process(target=fun, args=(f, q_in, q_out))
            for _ in range(nprocs)]
    for p in proc:
        p.daemon = True
        p.start()

    sent = [q_in.put((i, x)) for i, x in enumerate(X)]
    [q_in.put((None, None)) for _ in range(nprocs)]
    res = [q_out.get() for _ in range(len(sent))]

    [p.join() for p in proc]

    return [x for i, x in sorted(res)]


class Machine:
    def __init__(self, machine_name, machine_operation_code, machine_id, mappable, machine_free_time=0):
        self.machine_name = machine_name
        self.machine_operation_code = machine_operation_code
        self.machine_id = machine_id
        self.machine_free_time = machine_free_time
        self.scheduled_operations = []
        self.time_slots = [False] * 1000
        self.mappable = mappable

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return str({"machine_name": self.machine_name,
                    "machine_operation_code": self.machine_operation_code,
                    "machine_free_time": self.machine_free_time
                    })

    def __getitem__(self, key):
        return getattr(self, key)

    def schedule_operation(self, scheduled_operation):
        self.scheduled_operations.append(scheduled_operation)
        # self.scheduled_operations.sort(key=operator.attrgetter("operation.operation_start_time"))
        self.machine_free_time = self.scheduled_operations[-1].operation.operation_end_time

        # print(scheduled_operation.operation.operation_start_time,
        #                scheduled_operation.operation.operation_end_time)

        for i in range(scheduled_operation.operation.operation_start_time,
                       scheduled_operation.operation.operation_end_time):
            self.time_slots[i] = True

    def get_free_time_slots(self):

        slots = []

        i = 0
        while (i != len(self.time_slots)):

            while (self.time_slots[i] != False):
                i += 1
                if (i == len(self.time_slots)):
                    break

            start = i

            if (i == len(self.time_slots)):
                break

            while (self.time_slots[i] != True):
                i += 1
                if (i == len(self.time_slots)):
                    break

            end = i

            slots.append((start, end))

        return slots

    def print_scheduled_operations(self, jobs):

        for scheduled_op in self.scheduled_operations:
            job = scheduled_op.job
            operation = scheduled_op.operation
            job_idx = jobs.index(job)
            operation_idx = job.operations.index(operation)
            print("\nJ" + str(job_idx) + "O" + str(operation_idx) + " ST:" + str(
                operation.operation_start_time) + " ET:" +
                  str(operation.operation_end_time))


class Job:
    def __init__(self, job_code, section):
        self.operations = []
        self.job_code = job_code
        self.section = section

    def add_operation(self, Operation):
        self.operations.append(Operation)
        self.operations.sort(key=operator.itemgetter('operation_sequence'))

    def __repr__(self):
        return str({"job_code": self.job_code,
                    "operations": self.operations})

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __getitem__(self, key):
        return getattr(self, key)


class Operation:
    def __init__(self, operation_code, operation_duration, operation_sequence, mappable,
                 operation_start_time=0, operation_end_time=0):
        self.operation_code = operation_code
        self.operation_duration = operation_duration
        self.operation_sequence = operation_sequence
        self.mappable = mappable
        self.operation_start_time = operation_start_time
        self.operation_end_time = operation_end_time

    def __getitem__(self, key):
        return getattr(self, key)


class ScheduledOperation:

    def __init__(self, job, operation, start_time, end_time):
        self.job = job
        operation.operation_start_time = start_time
        operation.operation_end_time = end_time
        self.operation = operation


class Section:
    def __init__(self, section_id, priority_index):
        self.section_id = section_id
        self.priority_index = priority_index
        self.subpriorities = []

    def add_subpriority(self, subpriority):
        self.subpriorities.append(subpriority)


class SubPriority:
    def __init__(self, subpriority_name, priority_index):
        self.subpriority_name = subpriority_name
        self.priority_index = priority_index


# @timeit
def load_jobs_data(jobs, json_data):
    # for (json job : jsonData["fabricationOrders"]) {
	# 	int jobcode = job["FOID"];

	# 	Job newJob(jobcode);

	# 	for (json operation : job["operations"]) {
	# 		Operation newOperation(operation["operationType"],
	# 			int(ceil(operation["operationDuration"].get<int>() / (double)BASE_TIME_UNIT_MINUTES)),
	# 			operation["operationID"], operation["operationSequence"], operation["mappable"]);
	# 		newJob.addOperation(newOperation);
	# 	}
	# 	jobs.push_back(newJob);
	# 	jobs.back().jobIndex = getIndex(jobs, newJob);
	# }
    ####################################################
    for job in json_data['fabricationOrders']:
        sectionId=0
        new_job=Job(job['FOID'], sectionId)
        for operation in job['operations']:
            new_operation=Operation(operation['operationType'],
                            math.ceil(operation['operationDuration'] / 60),
                            operation['operationSequence'], operation['mappable'])
            new_job.add_operation(new_operation)
        jobs.append(new_job)
    ####################################################
    #/////////////////old///////////////////////////////
    # for section in json_data['fos']:

    #     for job in section['jobs']:

    #         new_job = Job(job['jobcode'], section['sectionId'])

    #         for operation in job['operations']:
    #             new_operation = Operation(operation['type'],
    #                                       math.ceil(operation['duration'] / 60),
    #                                       operation['sequence'], operation['mappable'])

    #             new_job.add_operation(new_operation)

    #         jobs.append(new_job)
    #///////////////////////////////////////////////////////
    # for job in json_data['jobs']:
    #
    #     new_job = Job(job['jobcode'], job['section'])
    #
    #     for operation in job['operations']:
    #         new_operation = Operation(operation['code'],
    #                                   operation['duration'],
    #                                   operation['sequence'])
    #
    #         new_job.add_operation(new_operation)
    #
    #     jobs.append(new_job)


# @timeit
def load_machines_data(machines, json_data):
    # //////////////c++///////////////////////////////

	# for (json machine : jsonData["machines"]) {
	# 	Machine newMachine(machine["machineID"], machine["machineOperationCode"], machine["machineName"]);
	# 	//cout << machine["machineID"]<< machine["machineOperationCode"]<<machine["machineName"]<< endl;

	# 	machines.push_back(newMachine);
	# }
    #/////////////////////////////////////////////////
    ##################################################
    for machine_obj in json_data['machines']:
        new_machine=Machine(machine_obj['machineName'], machine_obj['machineOperationCode'], machine_obj['machineID'], machine_obj['mappable'])

        if (new_machine not in machines):
            machines.append(new_machine)

    ##################################################
    ##########################old#####################
    # for machine_obj in json_data['availableMachines']:

    #     for machine in machine_obj['machines']:
    #         new_machine = Machine(machine['name'], machine['code'], machine['machineId'], machine['mappable'])

    #         if (new_machine not in machines):
    #             machines.append(new_machine)

    ##################################################

    # for operation_code in json_data['availableMachines']:
    #
    #     for machine in json_data["machines"][operation_code]:
    #
    #         new_machine = Machine(machine['name'], operation_code)
    #
    #         if (new_machine not in machines):
    #             machines.append(new_machine)


def load_priorities_data(sections, json_data):
    for section in json_data['priorities']:

        new_section = Section(section['sectionId'], section['sectionPriority'])

        for subpriority in section['subPriorities']:
            new_subpriority = SubPriority(subpriority['opCode'], subpriority['opPriority'])

            new_section.add_subpriority(new_subpriority)

        sections.append(new_section)


def clear_jobs_and_machines_data(jobs, machines):
    for job in jobs:
        for operation in job.operations:
            operation.operation_start_time = 0
            operation.operation_end_time = 0

    for machine in machines:
        machine.scheduled_operations.clear()
        machine.machine_free_time = 0
        machine.time_slots = [False] * 5000


# @timeit
def generate_chromosome(jobs):
    chromosome = []

    for idx, job in enumerate(jobs):
        chromosome.extend([idx] * len(job.operations))

    random.shuffle(chromosome)

    return chromosome


def get_machines_for_operation(operation_code, machines):
    temp_list = []
    for machine in machines:
        if machine.machine_operation_code == operation_code:
            temp_list.append(machine)
    return temp_list


# @timeit
def one_point_crossover(ch1, ch2):
    localCh1 = ch1[:]
    localCh2 = ch2[:]
    splittingPoint = math.floor(len(localCh1) / 2)
    substring1 = localCh1[0:splittingPoint]

    offspring1 = substring1

    for gene in offspring1:
        localCh2.remove(gene)

    for gene in localCh2:
        offspring1.append(gene)

    return offspring1


def mutation(ch):
    import random
    number = random.uniform(0, 1)

    if number <= 0.25:
        index1 = random.randint(0, len(ch) - 1)
        index2 = random.randint(0, len(ch) - 1)

        ch[index1], ch[index2] = ch[index2], ch[index1]

    return (ch)


def get_first_suitable_time_slot_optimized(machine, operation_index, op_duration, prev_op_end_time=0):
    start_index = 0
    count_flag = 0
    for i in range(prev_op_end_time, len(machine.time_slots)):
        if (machine.time_slots[i] == 1):
            count_flag = 0
        else:
            if (count_flag == 0):
                start_index = i
            count_flag = count_flag + 1
            if (count_flag == op_duration):
                break

    return start_index

    # i = prev_op_end_time
    # if (prev_op_end_time >= machine.machine_free_time):
    #     return prev_op_end_time
    # length = len(machine.time_slots)
    # while (i != length):
    #
    #     while (machine.time_slots[i] != False):
    #         i += 1
    #         if (i == length):
    #             break
    #
    #     start = i
    #
    #     # if (i == length):
    #     #     break
    #
    #     if (i == machine.machine_free_time):
    #         return i
    #
    #     while (machine.time_slots[i] != True):
    #         i += 1
    #         if (i == length):
    #             break
    #
    #     end = i
    #
    #     if (operation_index == 0):
    #         if (end - start >= op_duration):
    #             return start
    #
    #     else:
    #
    #         if (prev_op_end_time <= start):
    #
    #             if (end - start >= op_duration):
    #                 return start
    #
    #         elif (prev_op_end_time >= start and prev_op_end_time <= end):
    #
    #             if (end - prev_op_end_time >= op_duration):
    #                 return (prev_op_end_time)
    #
    # return -1


# @profile
# @timeit
def get_makespan_local_machines_copy(chromosome, jobs, machines,totalExecutionTime):
    ts = time.clock()

    local_machines_copy = machines[:]

    operation_of_job_count = [-1] * len(jobs)

    ordered_operations = []
    for genes in chromosome:
        operation_of_job_count[genes] += 1

        ordered_operations.append((genes,
                                   operation_of_job_count[genes],
                                   jobs[genes].operations[operation_of_job_count[genes]].operation_code,
                                   jobs[genes].operations[operation_of_job_count[genes]].operation_duration))

    for ordered_operation in ordered_operations:

        job_idx = ordered_operation[0]
        operation_idx = ordered_operation[1]
        operation_code = ordered_operation[2]
        duration = ordered_operation[3]
        job = jobs[job_idx]
        operation = job.operations[operation_idx]

        # if job_idx == 1:
        #     print("a")

        eligible_machines = get_machines_for_operation(operation_code, local_machines_copy)
        import operator

        if (operation_idx != 0):
            previous_operation_idx = operation_idx - 1
            previous_operation_end_time = jobs[job_idx].operations[previous_operation_idx].operation_end_time
        else:
            previous_operation_end_time = 0

        # time_slots = _pool2.map(
        #     partial(get_first_suitable_time_slot_optimized, operation_index=operation_idx, op_duration=duration,
        #             prev_op_end_time=previous_operation_end_time),
        #     eligible_machines)

        # time_slots = Parallel(n_jobs=-1)(
        #     delayed(get_first_suitable_time_slot_optimized)(machine, operation_idx, duration,
        #                                                     previous_operation_end_time) for machine in
        #     eligible_machines)

        if (operation.mappable == True):
            time_slots = []
            for machine in eligible_machines:
                time_slots.append(
                    get_first_suitable_time_slot_optimized(machine, operation_idx, duration,
                                                           previous_operation_end_time))

            time_slot_start = min(time_slots)

            best_machine_index = time_slots.index(min(time_slots))

            best_suitable_machine = eligible_machines[best_machine_index]

            best_suitable_machine.schedule_operation(
                ScheduledOperation(job, operation, time_slot_start, time_slot_start + duration))

        else:

            if (operation_idx == 0):

                eligible_machines[0].schedule_operation(ScheduledOperation(job, operation, 0, duration))



            else:
                previous_operation_idx = operation_idx - 1
                previous_operation_end_time = jobs[job_idx].operations[previous_operation_idx].operation_end_time

                eligible_machines[0].schedule_operation(ScheduledOperation(job, operation, previous_operation_end_time,
                                                                           previous_operation_end_time + duration))

    tempList = []

    for job in jobs:
        tempList.append(job.operations[-1].operation_end_time)
        # for operation in job.operations:
        #     tempList.append(operation.operation_end_time)

    # for machine in local_machines_copy:
    #
    #     if (len(machine.scheduled_operations) != 0):
    #         tempList.append(machine.scheduled_operations[-1].operation.operation_end_time)
    makespan = max(tempList)

    # for machine in machines:
    #     print(machine.machine_name)
    #     machine.print_scheduled_operations(jobs)
    #     print("***************************************************************************")

    # COMMENT THIS
    # write_to_excel_and_save_image(makespan, machines, jobs, time.strftime("%Y%m%d-%H%M%S"))
    # write_to_excel_and_save_image_new(makespan, machines, jobs, time.strftime("%Y%m%d-%H%M%S"))

    # write_csv_sorted(machines, jobs, os.path.dirname(os.path.realpath(__file__)) + "\\" + time.strftime("%Y%m%d-%H%M%S") + "_makespan-eligiblemachines-new.csv")
    # print("Job\tOperation\tST\tET\tM")
    # write_csv_sorted_by_machine(machines, jobs,
    #                             os.path.dirname(os.path.realpath(__file__)) + "\\" + time.strftime("%Y%m%d-%H%M%S") + "_sortedByMachine.csv")
    #
    # write_csv_sorted_by_schedop(machines, jobs,
    #                             os.path.dirname(os.path.realpath(__file__)) + "\\" + time.strftime("%Y%m%d-%H%M%S") + "_sortedBySchedOp.csv")
    #
    # write_csv_sorted_by_operation(machines, jobs,
    #                               os.path.dirname(os.path.realpath(__file__)) + "\\" + time.strftime("%Y%m%d-%H%M%S") + "_sortedByOperation.csv")

    # UNCOMMENT THIS
    clear_jobs_and_machines_data(jobs, machines)
    # print(multiprocessing.current_process(), makespan)
    # print('get_makespan took %f' % (time.clock() - ts))
    totalExecutionTime=totalExecutionTime+float(time.clock() - ts)*1000
    # print(totalExecutionTime)
    return makespan,totalExecutionTime


def map_chromosome_to_machines(chromosome, jobs, machines):
    operation_of_job_count = [-1] * len(jobs)

    ordered_operations = []
    # print(chromosome)
    for genes in chromosome:
        operation_of_job_count[genes] += 1
        # orderedOperations = [(job, operation, operation_code, duration), (), ...]

        # job = genes
        # operation = operation_of_job_count[genes]
        # operation_code = jobs[genes].operations[operation_of_job_count[genes]].operation_code
        # duration = jobs[genes].operations[operation_of_job_count[genes]].operation_duration

        ordered_operations.append((genes,
                                   operation_of_job_count[genes],
                                   jobs[genes].operations[operation_of_job_count[genes]].operation_code,
                                   jobs[genes].operations[operation_of_job_count[genes]].operation_duration))

    for ordered_operation in ordered_operations:

        job_idx = ordered_operation[0]
        operation_idx = ordered_operation[1]
        operation_code = ordered_operation[2]
        duration = ordered_operation[3]
        job = jobs[job_idx]
        operation = job.operations[operation_idx]

        # if job_idx == 1:
        #     print("a")

        eligible_machines = get_machines_for_operation(operation_code, machines)
        import operator

        if (operation_idx != 0):
            previous_operation_idx = operation_idx - 1
            previous_operation_end_time = jobs[job_idx].operations[previous_operation_idx].operation_end_time
        else:
            previous_operation_end_time = 0

        # time_slots = _pool.map(
        #     partial(get_first_suitable_time_slot_optimized, operation_index=operation_idx, op_duration=duration,
        #             prev_op_end_time=previous_operation_end_time),
        #     eligible_machines)

        if (operation.mappable == True):
            time_slots = []
            for machine in eligible_machines:
                time_slots.append(
                    get_first_suitable_time_slot_optimized(machine, operation_idx, duration,
                                                           previous_operation_end_time))

            time_slot_start = min(time_slots)

            best_machine_index = time_slots.index(min(time_slots))

            best_suitable_machine = eligible_machines[best_machine_index]

            best_suitable_machine.schedule_operation(
                ScheduledOperation(job, operation, time_slot_start, time_slot_start + duration))

        else:

            if (operation_idx == 0):

                eligible_machines[0].schedule_operation(ScheduledOperation(job, operation, 0, duration))



            else:
                previous_operation_idx = operation_idx - 1
                previous_operation_end_time = jobs[job_idx].operations[previous_operation_idx].operation_end_time

                eligible_machines[0].schedule_operation(ScheduledOperation(job, operation, previous_operation_end_time,
                                                                           previous_operation_end_time + duration))


def get_jobs_by_section(jobs, section):
    jobs_by_section = []

    for job in jobs:
        if (job.section == section):
            jobs_by_section.append(job)

    return jobs_by_section


def get_jobs_by_operation(jobs, operation):
    jobs_by_operation = []

    for job in jobs:
        for each_operation in job.operations:
            if (each_operation.operation_code == operation):
                jobs_by_operation.append(job)
                break

    return jobs_by_operation


def longest(l):
    if (not isinstance(l, list)): return (0)
    return (max([len(l), ] + [len(subl) for subl in l if isinstance(subl, list)] +
                [longest(subl) for subl in l]))


def get_longest_list(list_of_lists):
    length_list = []
    for list in list_of_lists:
        length_list.append(len(list))

    max_length = max(length_list)

    max_length_index = length_list.index(max_length)

    longest_list = list_of_lists[max_length_index]

    return longest_list


def get_section_by_section_priority(section_priority, list_of_sections):
    section_list = []

    for section in list_of_sections:
        if (section.priority_index == section_priority):
            section_list.append(section)

    return section_list


def get_subpriority_by_subpriority_index(subpriority_index, list_of_sections):
    subpriority_list = []

    for section in list_of_sections:

        for subpriority in section.subpriorities:

            if (subpriority.priority_index == subpriority_index):
                subpriority_list.append(subpriority)

    return subpriority_list


def divide_jobs_in_batches(sections, jobs):
    temp_priority_container = {}

    unique_priority_indices = []
    for section in sections:
        if (section.priority_index not in unique_priority_indices):
            unique_priority_indices.append(section.priority_index)

    for unique_priority_index in unique_priority_indices:
        temp_priority_container[unique_priority_index] = []

    for section_idx, section in enumerate(sections):

        unique_subpriority_indices = []
        for subpriority in section.subpriorities:
            if (subpriority.priority_index not in unique_subpriority_indices):
                unique_subpriority_indices.append(subpriority.priority_index)

        unique_subpriority_indices.sort()
        temp_priority_container[section.priority_index].append(unique_subpriority_indices)

    unique_priority_count = {}
    for key in temp_priority_container:
        unique_priority_count[key] = longest(temp_priority_container[key])

    unique_priority_container = {}

    for key, value in temp_priority_container.items():
        unique_priority_container[key] = get_longest_list(value)

    section_id_container = {}

    for key in unique_priority_count:
        sections_list = []
        for section in sections:
            if (section.priority_index == key):
                sections_list.append(section.section_id)

        section_id_container[key] = sections_list

    subpriority_name_container = OrderedDict()
    for key in unique_priority_count:
        subpriority_name_container[key] = OrderedDict()

        for unique_subpriority in unique_priority_container[key]:
            subpriority_name_container[key][unique_subpriority] = []

    for section_priority_index in subpriority_name_container:

        sections_list = get_section_by_section_priority(section_priority_index, sections)

        for subpriority_index in unique_priority_container[section_priority_index]:

            subpriorities_list = get_subpriority_by_subpriority_index(subpriority_index, sections_list)

            for subpriority in subpriorities_list:

                if (subpriority.subpriority_name not in subpriority_name_container[section_priority_index][
                    subpriority_index]):
                    subpriority_name_container[section_priority_index][subpriority_index].append(
                        subpriority.subpriority_name)

    # print(subpriority_name_container)

    job_batches = []
    for section_priority_index in subpriority_name_container:

        sectioned_jobs = []
        section_id_list = section_id_container[section_priority_index]

        for section_id in section_id_list:
            sectioned_jobs.extend(get_jobs_by_section(jobs, section_id))

        for subpriority_index in subpriority_name_container[section_priority_index]:

            list_of_subpriorities = subpriority_name_container[section_priority_index][subpriority_index]

            for subpriority in list_of_subpriorities:
                temp = []
                temp.extend(get_jobs_by_operation(sectioned_jobs, subpriority))

            if (len(temp) != 0):
                job_batches.append(temp)
                for job in temp:
                    sectioned_jobs.remove(job)

        if (len(sectioned_jobs) != 0):
            job_batches.append(sectioned_jobs)

    return job_batches


def map_batch_to_machines(batch_of_jobs, machines, population_size, max_generations, batch_index):
    # if (not machines_iteration_var):
    #     machines_iteration_var = empty_machines[:]

    pop_size = population_size
    INITIAL_POP_SIZE = population_size
    max_gen = max_generations
    gen = 0
    parent_population_best_chromosome_fitness = 999999999
    parent_fitness = []
    child_population = []
    child_fitness = []
    parent_population_best_chromosome = None

    parent_population = [generate_chromosome(batch_of_jobs) for i in range(INITIAL_POP_SIZE)]

    print("Batch:", batch_index)

    while (gen <= max_gen):
        # print(gen)
        ts = time.time()

        # step2a compute fitness for every chromosome
        parent_fitness.clear()
        child_fitness.clear()

        parent_fitness = _pool.map(partial(get_makespan_local_machines_copy, jobs=batch_of_jobs, machines=machines),
                                   parent_population)

        # print("Gen:{gen},Computing fitness for parent chromosomes".format(gen=gen))
        # parent_fitness = parmap(partial(get_makespan_local_machines_copy, jobs=batch_of_jobs, machines=machines),
        #                         parent_population)

        # step2b choose fittest chromosome
        if min(parent_fitness) < parent_population_best_chromosome_fitness:
            parent_population_best_chromosome_fitness = min(parent_fitness)
            parent_population_best_chromosome_index = parent_fitness.index(min(parent_fitness))
            parent_population_best_chromosome = parent_population[parent_population_best_chromosome_index]
        import numpy
        if (gen == 0):
            indexes = numpy.argsort(parent_fitness)
            indexes_top = indexes[0:pop_size]

            parent_pop_top = [parent_population[i] for i in indexes_top]

            parent_population.clear()
            parent_population = parent_pop_top[:]

        # print("***********************************************************")
        # print("makespan:", parent_population_best_chromosome_fitness)
        # print("***********************************************************")
        print("***********************************************************")
        print("Gen:", gen, " makespan:", parent_population_best_chromosome_fitness)
        print("***********************************************************")

        if (gen == max_gen):
            break

        # step3 crossover

        child_population.clear()
        parent1 = parent_population[::2]
        parent2 = parent_population[1::2]
        offspring1 = [one_point_crossover(p1, p2) for p1, p2 in zip(parent1, parent2)]
        offspring2 = [one_point_crossover(p2, p1) for p1, p2 in zip(parent1, parent2)]
        child_population = offspring1 + offspring2

        # step4 mutation
        mutated_child_population = _pool.map(mutation, child_population)

        child_population.clear()
        child_population = mutated_child_population[:]

        child_fitness = _pool.map(partial(get_makespan_local_machines_copy, jobs=batch_of_jobs, machines=machines),
                                  child_population)

        # print("Gen:{gen},Computing fitness for child chromosomes".format(gen=gen))

        # child_fitness = parmap(partial(get_makespan_local_machines_copy, jobs=batch_of_jobs, machines=machines),
        #                        child_population)

        child_population_best_chromosome_fitness = min(child_fitness)

        child_population_worst_chromosome_index = child_fitness.index(max(child_fitness))

        if (parent_population_best_chromosome_fitness < child_population_best_chromosome_fitness):
            # replace child's worst ch with parent's best ch
            child_population[child_population_worst_chromosome_index] = parent_population_best_chromosome

        # copy child population to parent population for next generation
        parent_population = child_population[:]

        # print("Gen %d took %fs" % (gen, (time.time() - ts)))
        # print("*" * 100)

        gen = gen + 1

    parent_fitness.clear()

    parent_fitness = _pool.map(partial(get_makespan_local_machines_copy, jobs=batch_of_jobs, machines=machines),
                               parent_population)
    # parent_fitness = parmap(partial(get_makespan_local_machines_copy, jobs=batch_of_jobs, machines=machines),
    #                         parent_population)

    final_best_chromosome_index = parent_fitness.index(min(parent_fitness))
    final_best_chromosome = parent_population[final_best_chromosome_index]

    map_chromosome_to_machines(final_best_chromosome, batch_of_jobs, machines)


def get_schedule_from_machines(machines):
    new_schedule = {}
    for machine in machines:
        machine_id = machine.machine_id
        machine_name = machine.machine_name
        new_schedule[machine_name + "_" + str(machine_id)] = []
        operations = machine.scheduled_operations

        for scheduled_operation in operations:
            base_datetime = pendulum.datetime(2018, 1, 1, 8, 0, 0, 0)
            start_time = base_datetime.add(hours=scheduled_operation.operation.operation_start_time)
            end_time = base_datetime.add(hours=scheduled_operation.operation.operation_end_time)
            job_name = scheduled_operation.job.job_code
            operation_name = scheduled_operation.operation.operation_code
            new_schedule[machine_name + "_" + str(machine_id)].append(
                {'job': job_name, 'operation': operation_name, 'start': start_time.to_atom_string(),
                 'end': end_time.to_atom_string()})
            # job_number = operation["job"]
            # op_number = operation["operation"]
            # operation["name"] = jobs[job_number].operations[op_number].operation_code
            # new_schedule[machine_name] = operation
            #
            # schedule[machine_name] = schedule[machine]
            # del schedule[machine]
    # pprint(new_schedule)

    return new_schedule


# @timeit
def get_makepsan_from_machines(machines):
    makespan = []
    for machine in machines:
        for scheduled_op in machine.scheduled_operations:
            makespan.append(scheduled_op.operation.operation_end_time)

    return max(makespan)


# @timeit
def run_algo(population_size=10, generations=5):
    global _pool

    if _pool is None:
        _pool = Pool()
    json_data = json.load(open(FILE_NAME))

    machines = []
    jobs = []
    sections = []

    pop = population_size
    gen = generations

    load_jobs_data(jobs, json_data)
    load_machines_data(machines, json_data)
    # load_priorities_data(sections, json_data)
    # job_batches = divide_jobs_in_batches(sections, jobs)

    # for idx, batch in enumerate(job_batches):
    #     map_batch_to_machines(batch, machines, pop, gen, idx)

    schedule = (get_schedule_from_machines(machines))

    print(get_makepsan_from_machines(machines))

    # from ExcelGraphMappable import write_to_excel_and_save_image

    # write_to_excel_and_save_image(get_makepsan_from_machines(machines), machines, jobs, "v7_excel")

    # write_csv_sorted_by_operation(machines, jobs, "sorted_output_operations.csv")
    # write_csv_sorted_by_machine(machines, jobs, "sorted_output_machine.csv")


def run_algo_profiler(pop, gen):
    global _pool
    ts = time.time()
    if _pool is None:
        _pool = Pool()
    json_data = json.load(open(FILE_NAME))

    machines = []
    jobs = []
    sections = []

    load_jobs_data(jobs, json_data)
    load_machines_data(machines, json_data)
    # load_priorities_data(sections, json_data)
    # job_batches = divide_jobs_in_batches(sections, jobs)

    # for idx, batch in enumerate(job_batches):
    #     map_batch_to_machines(batch, machines, pop, gen, idx)

    return (get_makepsan_from_machines(machines), '%2.4f' % (time.time() - ts))


# @timeit
def test(popSize,genSize):
    ts = time.clock()
    json_data = json.load(open(FILE_NAME))
    machines = []
    jobs = []
    sections = []
    load_jobs_data(jobs, json_data)
    load_machines_data(machines, json_data)
    totalExecutionTime=0
    bestMakespan=999999


    for g in range(genSize):
        bestgen=888888
        for p in range(popSize):
            ch = generate_chromosome(jobs)
            makespan,totalExecutionTime = get_makespan_local_machines_copy(ch, jobs, machines,totalExecutionTime)
            print('gen = ',g,'population = ',p,',makespan = ',makespan*60/15)
            if(makespan<bestgen):
                bestgen=makespan
        if(bestgen<bestMakespan):
            bestMakespan=bestgen

    print('bestMakespan = ',bestMakespan*60/15)
    # global totalExecutionTime
    #print('totalExecutionTime = ',totalExecutionTime)

    print('totalExecutionTime = ',(time.clock()-ts)*1000)

    # mappable = 0

    # ////////////////////////////////////////////
    # for job in jobs:
    #     for operation in job.operations:
    #         if(operation.mappable):
    #             mappable +=1
    # count = 0
    # for job in jobs:
    #     count = count + len(job.operations)
    # print(mappable)
    # print(count)
    # ////////////////////////////////////////////
    # chromosome = [35, 32, 34, 34, 41, 30, 13, 25, 4, 10, 4, 23, 40, 20, 23, 30, 23, 1, 22, 3, 26, 18, 41, 28, 18, 17, 20, 36, 5, 6, 9, 41, 5, 29, 11,
    #               16, 31, 30, 26, 24, 26, 30, 14, 3, 23, 0, 22, 8, 8, 36, 38, 41, 21, 20, 40, 33, 10, 33, 22, 38, 40, 25, 33, 12, 33, 5, 28, 13, 40,
    #               4, 14, 5, 20, 3, 35, 37, 31, 2, 33, 19, 10, 15, 39, 21, 6, 17, 3, 37, 39, 27, 25, 11, 34, 17, 1, 16, 32, 7, 40, 10, 7, 35, 34, 25,
    #               33, 27, 27, 9, 37, 35, 40, 2, 11, 6, 28, 20, 25, 31, 4, 13, 22, 35, 32, 40, 37, 10, 41, 1, 37, 30, 26, 28, 34, 16, 7, 35, 8, 21, 38,
    #               25, 0, 13, 28, 7, 36, 23, 31, 22, 1, 22, 25, 28, 4, 25, 12, 33, 38, 6, 38, 36, 5, 25, 6, 1, 0, 3, 26, 26, 19, 14, 5, 4, 34, 23, 36,
    #               36, 32, 14, 21, 36, 36, 32, 21, 31, 41, 39, 14, 26, 31, 13, 16, 15, 3, 15, 33, 20, 29, 10, 0, 17, 5, 19, 31, 15, 37, 24, 18, 25, 2,
    #               20, 20, 12, 23, 0, 19, 29, 20, 19, 37, 10, 15, 28, 23, 2, 27, 24, 0, 22, 37, 28, 24, 3, 30, 17, 38, 27, 36, 27, 20, 16, 32, 16, 35,
    #               18, 0, 0, 25, 8, 12, 18, 39, 19, 7, 10, 9, 28, 32, 26, 26, 14, 13, 35, 37, 40, 27, 12, 26, 37, 30, 22, 3, 28, 20, 20, 8, 9, 20, 20,
    #               30, 41, 13, 11, 19, 5, 16, 10, 27, 35, 41, 9, 29, 29, 31, 19, 8, 0, 14, 4, 39, 34, 29, 16, 22, 1, 21, 5, 19, 36, 2, 10, 38, 38, 32,
    #               31, 6, 5, 33, 17, 6, 3, 35, 16, 35, 38, 32, 39, 11, 9, 40, 16, 36, 30, 36, 1, 39, 26, 18, 29, 0, 5, 40, 24, 17, 22, 9, 0, 12, 24,
    #               23, 8, 17, 17, 12, 33, 33, 28, 32, 6, 21, 23, 17, 6, 26, 25, 8, 14, 4, 20, 4, 27, 18, 38, 33, 9, 35, 25, 14, 2, 27, 35, 36, 18, 41,
    #               5, 4, 1, 26, 11, 32, 8, 39, 7, 35, 31, 5, 37, 34, 37, 8, 34, 29, 6, 19, 0, 3, 19, 24, 4, 17, 27, 32, 12, 16, 29, 31, 28, 16, 10, 29,
    #               3, 34, 31, 20, 15, 4, 9, 8, 36, 41, 36, 16, 29, 24, 25, 32, 14, 27, 19, 8, 7, 3, 12, 7, 23, 9, 6, 9, 10, 14, 14, 36, 7, 33, 40, 36,
    #               17, 3, 21, 7, 22, 40, 2, 33, 40, 18, 15, 5, 27, 32, 24, 11, 2, 0, 41, 39, 35, 18, 38, 4, 16, 2, 13, 22, 17, 9, 34, 28, 34, 1, 15,
    #               20, 4, 16, 28, 25, 40, 17, 1, 12, 34, 36, 28, 12, 10, 7, 13, 27, 36, 9, 39, 15, 11, 27, 9, 20, 33, 20, 23, 38, 28, 37, 1, 27, 31,
    #               30, 36, 3, 8, 37, 19, 3, 26, 29, 21, 13, 20, 26, 15, 34, 14, 17, 17, 3, 14, 32, 4, 35, 31, 8, 15, 2, 36, 12, 9, 30, 6, 41, 4, 5, 41,
    #               21, 39, 4, 24, 41, 10, 40, 21, 5, 37, 22, 15, 7, 27, 32, 18, 31, 29, 27, 2, 3, 39, 6, 30, 6, 11, 26, 9, 10, 14, 7, 30, 10, 9, 41, 2,
    #               25, 5, 24, 12, 14, 15, 5, 20, 22, 28, 35, 22, 23, 7, 11, 22, 31, 41, 6, 27, 24, 37, 41, 39, 34, 4, 33, 1, 23, 19, 18, 7, 41, 37, 35,
    #               40, 15, 38, 35, 19, 21, 39, 16, 19, 21, 9, 19, 30, 28, 39, 13, 40, 15, 19, 30, 5, 1, 3, 38, 8, 4, 0, 35, 12, 21, 39, 36, 10, 8, 39,
    #               8, 32, 25, 39, 5, 37, 10, 14, 4, 39, 25, 11, 20, 17, 16, 32, 24, 5, 13, 39, 19, 6, 18, 33, 4, 9, 11, 30, 1, 21, 13, 30, 18, 30, 1,
    #               35, 26, 23, 36, 5, 0, 29, 29, 28, 3, 41, 27, 13, 11, 35, 12, 12, 5, 17, 24, 14, 5, 0, 22, 36, 7, 6, 31, 34, 1, 11, 5, 15, 6, 13, 16,
    #               3, 15, 16, 0, 12, 37, 27, 24, 2, 39, 30, 21, 17, 6, 34, 21, 22, 32, 41, 28, 29, 36, 37, 25, 2, 18, 19, 11, 26, 29, 17, 23, 26, 10,
    #               23, 30, 12, 17, 26, 3, 41, 41, 7, 0, 17, 11, 2, 29, 14, 41, 13, 10, 18, 8, 40, 40, 33, 11, 12, 27, 19, 24, 1, 31, 41, 40, 1, 29, 18,
    #               40, 13, 9, 30, 2, 11, 22, 16, 39, 0, 30, 29, 37, 24, 13, 12, 18, 38, 41, 6, 6, 25, 35, 25, 21, 38, 18, 0, 32, 24, 8, 24, 28, 9, 13,
    #               9, 20, 30, 18, 2, 12, 40, 41, 5, 2, 37, 29, 22, 31, 1, 21, 15, 10, 27, 32, 38, 0, 8, 38, 5, 26, 36, 7, 11, 12, 23, 7, 28, 15, 35, 7,
    #               11, 18, 13, 31, 28, 27, 31, 23, 1, 35, 0, 20, 19, 38, 13, 22, 15, 23, 25, 29, 12, 34, 21, 33, 2, 34, 7, 7, 41, 25, 3, 35, 11, 14,
    #               13, 26, 41, 33, 32, 10, 39, 31, 34, 40, 8, 36, 14, 27, 34, 36, 2, 38, 17, 33, 8, 21, 25, 15, 16, 37, 6, 35, 41, 20, 26, 32, 23, 16,
    #               32, 11, 38, 35, 38, 37, 24]

    # ch1 = [7, 7, 1, 8, 7, 8, 2, 6, 7, 5, 9, 5, 5, 5, 9, 3, 8, 6, 4, 7, 7, 2, 7, 5, 1, 3, 5, 1, 4, 8, 0, 2, 8, 2, 5, 8, 0, 0, 5, 3, 2, 8, 0, 6, 8, 6,
    #        8, 6, 1, 6, 6, 9, 3, 9, 9, 4, 2, 3, 8, 4, 2, 1, 9, 1, 0, 0, 6, 1, 7, 9, 0, 4, 7, 1, 4, 5, 6, 3, 0, 9, 5, 1, 6, 7, 5, 5, 6, 3, 5, 1, 5, 7,
    #        6, 5, 0, 7, 6, 3, 8, 1, 4, 0, 9, 2, 4, 4, 0, 4, 2, 9, 3, 7, 5, 2, 4, 5, 9, 5, 3, 9, 3, 4, 6, 8, 9, 6, 6, 3, 3, 3, 2, 0, 6, 8, 3, 2, 2, 2,
    #        8, 0, 3, 1, 0, 8, 0, 8, 7, 0, 2, 1, 3, 4, 8, 8, 7, 7, 7, 1, 5, 5, 6, 0, 4, 5, 3, 5, 4, 8, 4, 5, 1, 0, 0, 9, 3, 9, 6, 7, 2, 2, 8, 6, 2, 4,
    #        2, 2, 5, 9, 9, 4, 3, 8, 9, 1, 9, 3, 1, 4, 0, 5, 5, 1, 5, 9, 7, 1, 0, 9, 8, 0, 9, 3, 6, 7, 4, 4, 7, 6, 7, 1]
    # ch2 = [7, 3, 6, 3, 2, 2, 0, 6, 2, 4, 4, 5, 7, 4, 9, 0, 9, 4, 9, 9, 9, 3, 2, 7, 0, 0, 3, 5, 5, 1, 7, 2, 4, 3, 2, 5, 6, 7, 6, 4, 9, 2, 8, 5, 2, 0,
    #        8, 7, 8, 8, 0, 4, 8, 1, 5, 1, 6, 3, 8, 2, 3, 7, 8, 0, 0, 2, 0, 8, 1, 7, 7, 4, 5, 9, 9, 6, 4, 1, 9, 7, 0, 9, 6, 7, 6, 4, 5, 6, 3, 5, 0, 3,
    #        2, 5, 7, 9, 4, 0, 4, 5, 5, 8, 1, 3, 7, 7, 8, 3, 8, 4, 5, 9, 9, 3, 4, 2, 3, 5, 0, 8, 6, 0, 3, 9, 1, 6, 4, 5, 7, 5, 8, 7, 3, 2, 4, 1, 6, 5,
    #        9, 5, 0, 3, 6, 6, 3, 6, 2, 9, 4, 5, 7, 3, 3, 0, 1, 3, 7, 9, 5, 5, 6, 1, 1, 3, 2, 7, 9, 8, 8, 4, 1, 5, 5, 8, 0, 8, 1, 4, 1, 1, 5, 6, 6, 9,
    #        6, 3, 4, 5, 2, 8, 0, 6, 6, 2, 4, 1, 7, 0, 1, 2, 7, 0, 9, 2, 1, 8, 1, 0, 2, 6, 8, 8, 0, 5, 8, 9, 1, 9, 7, 5]
    #
    # ch3 = [7, 7, 1, 8, 7, 8, 2, 6, 7, 5, 9, 5, 5, 5, 9, 3, 8, 6, 4, 7, 7, 2, 7, 5, 1, 3, 5, 1, 4, 8, 0, 2, 8, 2, 5, 8, 0, 0, 5, 3, 2, 8, 0, 6, 8, 6,
    #        8, 6, 1, 6, 6, 9, 3, 9, 9, 4, 2, 3, 8, 4, 2, 1, 9, 1, 0, 0, 6, 1, 7, 9, 0, 4, 7, 1, 4, 5, 6, 3, 0, 9, 5, 1, 6, 7, 5, 5, 6, 3, 5, 1, 5, 7,
    #        6, 5, 0, 7, 6, 3, 8, 1, 4, 0, 9, 2, 4, 4, 0, 4, 2, 9, 2, 3, 2, 9, 4, 0, 4, 3, 7, 7, 3, 4, 9, 9, 3, 4, 2, 3, 0, 8, 0, 3, 9, 4, 7, 5, 8, 7,
    #        3, 2, 4, 5, 9, 5, 0, 3, 6, 3, 6, 2, 9, 4, 5, 7, 3, 3, 0, 3, 7, 9, 5, 5, 6, 3, 2, 7, 9, 8, 8, 4, 1, 5, 5, 8, 0, 8, 1, 4, 1, 1, 5, 6, 6, 9,
    #        6, 3, 4, 5, 2, 8, 0, 6, 6, 2, 4, 1, 7, 0, 1, 2, 7, 0, 9, 2, 1, 8, 1, 0, 2, 6, 8, 8, 0, 5, 8, 9, 1, 9, 7, 5]
    #
    # ch4 = [7, 3, 6, 3, 2, 2, 0, 6, 2, 4, 4, 5, 7, 4, 9, 0, 9, 4, 9, 9, 9, 3, 2, 7, 0, 0, 3, 5, 5, 1, 7, 2, 4, 3, 2, 5, 6, 7, 6, 4, 9, 2, 8, 5, 2, 0,
    #        8, 7, 8, 8, 0, 4, 8, 1, 5, 1, 6, 3, 8, 2, 3, 7, 8, 0, 0, 2, 0, 8, 1, 7, 7, 4, 5, 9, 9, 6, 4, 1, 9, 7, 0, 9, 6, 7, 6, 4, 5, 6, 3, 5, 0, 3,
    #        2, 5, 7, 9, 4, 0, 4, 5, 5, 8, 1, 3, 7, 7, 8, 3, 8, 4, 1, 1, 1, 6, 6, 5, 1, 5, 6, 5, 6, 1, 5, 5, 5, 9, 6, 8, 9, 6, 6, 3, 3, 3, 6, 8, 3, 2,
    #        2, 2, 8, 0, 3, 1, 0, 8, 0, 8, 0, 2, 1, 3, 8, 8, 7, 7, 7, 1, 5, 5, 6, 0, 4, 5, 3, 5, 4, 8, 4, 5, 1, 0, 0, 9, 3, 9, 6, 7, 2, 2, 8, 6, 2, 4,
    #        2, 2, 5, 9, 9, 4, 3, 8, 9, 1, 9, 3, 1, 4, 0, 5, 5, 1, 5, 9, 7, 1, 0, 9, 8, 0, 9, 3, 6, 7, 4, 4, 7, 6, 7, 1]
    # makespan = get_makespan_local_machines_copy(ch1, jobs, machines)
    # makespan2 = get_makespan_local_machines_copy(ch2, jobs, machines)
    # makespan3 = get_makespan_local_machines_copy(ch3, jobs, machines)
    # makespan4 = get_makespan_local_machines_copy(ch4, jobs, machines)
    # print(makespan)
    # print(makespan2)
    # print(makespan3)
    # print(makespan4)


if __name__ == '__main__':
    test(16,100)
    # _pool = Pool()

    # run_algo(4, 100)

# try:
#     _pool = Pool()
#
#     run_algo(10, 500)
#
# except KeyboardInterrupt:
#     _pool.close()
#     _pool.join()
