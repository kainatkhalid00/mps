#pragma once
#include <vector>
#include <chrono>
#include <algorithm>
#include <random>
#include <iostream>
#include "classes.h"
#include <map>
#include <unordered_map> 


using namespace std;

extern vector<Job> jobs;
extern vector<Machine>machines;
extern unordered_map<string, vector<int>> machinesMap;
extern vector<int> timeTaken;


std::vector<int> generateChromosome(vector<Job>& jobs);

void mutate(vector<int>& chromosome);

vector<int> onePointCrossover(vector<int> parent1, vector<int> parent2);

int getMakespan(const vector<int> &chromosome, vector<Job> jobs, vector<Machine> machines);

vector<int> getMachinesForOperation(string operationCode);

void loadData();

void runGA(int popSize, int maxGenerations);