#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include <random>
#include <chrono>
#include <string>

#include"classes.h"

using namespace std;

void sortScheduledOperations(vector<ScheduledOperation>& scheduledOperations);

template <typename T>
bool AreVectorsSame(const std::vector<T>& i_Vec1, const std::vector<T>& i_Vec2) {
	if (i_Vec1.size() != i_Vec2.size())
		return false;

	for (size_t i = 0; i < i_Vec1.size(); i++)
		if (i_Vec1[i] == i_Vec2[i]) {
		}
		else
			return false;

	return true;
}

Tuple::Tuple(int start, int end) {
	this->start = start;
	this->end = end;
}

void Machine::scheduleOperation(vector<Job>& jobs, TimeSlotIndex& bestTimeSlot, Operation& currentOperation, int jobIdx) {

	int startTime = bestTimeSlot.operationStartTime;
	// int timeSlotIndex = bestTimeSlot.index;
	int slotStartTime = bestTimeSlot.timeSlot.startTime;
	int slotEndTime = bestTimeSlot.timeSlot.startTime + bestTimeSlot.timeSlot.duration;

	currentOperation.operationStartTime = startTime;
	currentOperation.operationEndTime = startTime + currentOperation.operationDuration;
	//cout << currentOperation.operationEndTime << endl;
	currentOperation.allotedMachine = this->machineID;

	this->clearTimeSlot(bestTimeSlot.timeSlot);


	//left timeslot
	if (currentOperation.operationStartTime > slotStartTime) {
		// cout << "Adding left time slot..." << endl;
		this->addNewTimeSlot(slotStartTime, currentOperation.operationStartTime - slotStartTime);
	}
	//right timeslot
	if (currentOperation.operationEndTime < slotEndTime) {
		// cout << "Adding right time slot..." << endl;

		this->addNewTimeSlot(currentOperation.operationEndTime,
			slotEndTime - currentOperation.operationEndTime);

	}



	//ScheduledOperation scheduledOperation(jobs[jobIdx], currentOperation);


	//this->scheduledOperations.push_back(scheduledOperation);

	// cout << "Scheduled operation from " << startTime << ":" << startTime + currentOperation.operationDuration << " on machine " << machineID << endl;
	// cout << "New Time Slots:" << endl;

	// for (auto &timeslot : this->newTimeSlots){
	//
	// 	cout << timeslot.startTime << ":" << timeslot.duration << endl;
	//
	// }

	// this->machineFreeTime = currentOperation.operationEndTime;
}

void Machine::scheduleOperation(vector<Job>& jobs, int operationStartTime, int operationDuration, Operation& currentOperation, int jobIdx) {

	currentOperation.operationStartTime = operationStartTime;
	currentOperation.operationEndTime = operationStartTime + currentOperation.operationDuration;
	//cout << currentOperation.operationEndTime << endl;

	currentOperation.allotedMachine = this->machineID;

	//ScheduledOperation scheduledOperation(jobs[jobIdx], currentOperation);


	//this->scheduledOperations.push_back(scheduledOperation);

	// this->machineFreeTime = currentOperation.operationEndTime;

}

void Machine::clear() {
	machineFreeTime = 0;
	//scheduledOperations.clear();
}

Machine::Machine(int machineID, string operationCode, string machineName) {
	this->machineID = machineID;
	this->operationCode = operationCode;
	this->machineName = machineName;
	this->newTimeSlots.insert(TimeSlot(0, 5000));
	// machineFreeTime = 0;
	// this->newTimeSlots[0].startTime = 0;
	// this->newTimeSlots[0].duration = 50000;
	//scheduledOperations.reserve(500);

	//timeSlots.assign(5000, 0);

}

//void Machine::scheduleOperationOnMachine(ScheduledOperation op) {
//	this->scheduledOperations.push_back(op);
//	sortScheduledOperations(this->scheduledOperations);
//	this->machineFreeTime = this->scheduledOperations.back().operationPtr->operationEndTime;
//
//	for (int i = op.operationPtr->operationStartTime; i < op.operationPtr->operationEndTime; i++) {
//		this->timeSlots[i] = 1;
//	}
//}

//void Machine::clearSchedule() {
//	scheduledOperations.clear();
//}

bool operator==(const Machine& m1, const Machine& m2) {
	return (m1.machineID == m2.machineID && m1.operationCode == m2.operationCode && m1.machineName == m2.machineName);

}

bool operator==(const Job& job1, const Job& job2) {
	return (job1.jobCode == job2.jobCode && job1.operations == job2.operations
		&& job1.unscheduledOperationIndex == job2.unscheduledOperationIndex
		&& job1.operationsPtr == job2.operationsPtr

		);
}

//Operation Machine::getLastScheduledOperation() {
//	if (!this->scheduledOperations.empty())
//		return *this->scheduledOperations.back().operationPtr;
//	return Operation();
//}
//
//Operation Machine::getFirstScheduledOperation() {
//	return *scheduledOperations.front().operationPtr;
//}

vector<Tuple> Machine::getFreeTimeSlots() {

	vector<Tuple> slots;
	int start = 0;
	int end = 0;
	int i = 0;
	while (i != timeSlotsSize) {

		while (this->timeSlots[i] != 0) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		start = i;

		if (i == timeSlotsSize)
			break;

		while (this->timeSlots[i] != 1) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		end = i;

		slots.push_back(Tuple(start, end));

	}

	return slots;
}

//__device__ __host__ Tuple* Machine::getFreeTimeSlotsPtr() {
//	vector<Tuple> timeSlots = getFreeTimeSlots();
//	return &timeSlots[0];
//
//}

ostream& operator<<(ostream& os, const Machine& machine) {
	os << machine.machineName << "(" << machine.operationCode << ")";

	return os;
}

void Operation::clear() {
	operationStartTime = 0;
	operationEndTime = 0;
}

Operation::Operation(string operationCode, int operationDuration, int operationId, int operationIndex, bool mappable, int FOQtyIndex) {
	this->operationCode = operationCode;
	this->operationDuration = operationDuration;
	this->operationIndex = operationIndex;
	this->operationId = operationId;

	// this->operationSequence = operationSequence;
	this->mappable = mappable;
	operationStartTime = 0;
	operationEndTime = 0;
	this->FOQtyIndex = FOQtyIndex;
}

// void Operation::initializeEligibleMachinesIndicesArray(int size) {
// 	eligibleMachinesIndices = new int[size];
// 	for (int i = 0; i < size; i++) {
// 		eligibleMachinesIndices[i] = 0;
// 	}
//
// 	/*for (int i = 0; i < size; i++) {
// 	 cout << eligibleMachinesIndices[i] << endl;
// 	 }*/
//
// }

bool operator==(const Operation& operation1, const Operation& operation2) {
	return (operation1.operationCode == operation2.operationCode
		&& operation1.operationDuration == operation2.operationDuration
		&& operation1.operationId == operation2.operationId
		&& operation1.operationIndex == operation2.operationIndex
		&& operation1.operationStartTime == operation2.operationStartTime
		&& operation1.operationEndTime == operation2.operationEndTime
		);
}

bool operator==(const ScheduledOperation& o1, const ScheduledOperation& o2) {
	return (o1.jobRef == o2.jobRef && o1.operationRef == o2.operationRef);
}

ostream& operator<<(ostream& os, const Operation& operation) {
	os << "\t" << operation.operationIndex << " " << " " << operation.operationCode << " "
		<< operation.operationDuration;

	return os;
}

Job::Job(int jobCode, int quantity, int pr) {
	this->jobCode = jobCode;
	this->unscheduledOperationIndex = 0;
	this->quantity = quantity;
	this->priority = pr;
}

void Job::clear() {
	for (Operation& operation : this->operations) {
		operation.clear();
	}
}

void Job::addOperation(Operation operation) {
	this->operations.push_back(operation);
}

// ScheduledOperation::ScheduledOperation(Job* job, Operation* operation) {
// 	this->jobPtr = job;
// 	this->operationPtr = operation;
// }

void sortMachines(vector<Machine>& machines) {
	sort(machines.begin(), machines.end(), [](const Machine& lhs, const Machine& rhs) {
		return lhs.machineFreeTime < rhs.machineFreeTime;
	});
}

void sortScheduledOperations(vector<ScheduledOperation>& scheduledOperations) {
	sort(scheduledOperations.begin(), scheduledOperations.end(),
		[](const ScheduledOperation& lhs, const ScheduledOperation& rhs) {
		return lhs.operationRef.operationStartTime < rhs.operationRef.operationStartTime;
	});
}