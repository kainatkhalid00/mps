#include "classes.h"
// #include <sys/time.h>
#include "HelperFunctions.h"
#include "Genetic.h"
#include "ParseJSON.h"

using namespace std;
using namespace std::chrono;

// struct timeval t1, t2;


// void loadJobsDataNew(vector<Job>& jobs, json& jsonData) {
// 	jobs.clear();
// 	using milli = std::chrono::milliseconds;
// 	auto start = std::chrono::high_resolution_clock::now();
//
// 	for (json section : jsonData["fos"]) {
// 		for (json job : section["jobs"]) {
// 			int jobcode = job["jobcode"];
// 			int qty = job["qty"];
// 			int pr = job["priority"];
// 			prioritiesContainer.push_back(pr);
// 			qty = qty * AIRCRAFTS_PER_BATCH;
//
// 			for (int i = 0; i < qty; i++) {
//
// 				Job newJob(jobcode * i + jobcode, 1, pr);
//
// 				//							PartsQty += qty;
// 				PartsQty += 1;
//
// 				for (json operation : job["operations"]) {
//
// 					Operation newOperation(operation["type"], ceil(operation["duration"].get<int>() / (double)15),
// 					                       operation["sequence"], operation["mappable"], 0);
//
// 					//				newOperation.operationIndex = newOperation.operationSequence;
//
// 					newJob.addOperation(newOperation);
// 					//cout << operation["type"] << ceil(operation["duration"].get<int>() / (double) 60) << operation["sequence"] << operation["mappable"] << endl;
//
// 				}
// 				jobs.push_back(newJob);
// 				jobs.back().jobIndex = getIndex(jobs, newJob);
//
// 			}
//
// 		}
// 	}
//
// 	auto finish = std::chrono::high_resolution_clock::now();
// 	// std::cout << "loadJobsData() took "
// 	// 	<< std::chrono::duration_cast<milli>(finish - start).count()
// 	// 	<< " milliseconds\n";
// }


char* getCmdOption(char** begin, char** end, const std::string& option) {

	char** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end) {
		return *itr;
	}
	return 0;
}


bool cmdOptionExists(char** begin, char** end, const std::string& option) {
	return std::find(begin, end, option) != end;
}


void testFunc() {

	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	vector<vector<int>> parentPop;
	for (int i = 0; i < 24; i++) {
		parentPop.push_back(generateChromosome(jobs));
	}

	// auto finish = std::chrono::high_resolution_clock::now();

	// std::cout << "generation took "
	// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
	// 	<< " milliseconds\n" << endl;

	for (auto ch : parentPop)
		getMakespan(ch, jobs, machines);

	// std::cout << "getMakespan took "
	// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
	// 	<< " milliseconds\n" << endl;

	vector<vector<int>> childPop;
	for (int i = 0; i < 24; i += 2) {
		childPop.push_back(onePointCrossover(parentPop[i], parentPop[i + 1]));
		childPop.push_back(onePointCrossover(parentPop[i], parentPop[i + 1]));
	}
	//
	// std::cout << "crossover took "
	// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
	// 	<< " milliseconds\n" << endl;


	for (auto ch : childPop) {
		mutate(ch);
	}

	// std::cout << "mutate  took "
	// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
	// 	<< " milliseconds\n" << endl;

	for (auto ch : parentPop)
		getMakespan(ch, jobs, machines);

	// std::cout << "getMakespan took "
	// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
	// 	<< " milliseconds\n" << endl;

	auto finish = std::chrono::high_resolution_clock::now();

	std::cout << __func__ << " took "
		<< std::chrono::duration_cast<chrono::milliseconds>(finish - start).count()
		<< " milliseconds\n";


}

vector<int> getTokensofLine(string s) {
	vector<int> tokens;
	std::string delimiter = ",";

	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
		token = s.substr(0, pos);
		if (token.compare("")) {
			tokens.push_back(stoi(token));
		}
		s.erase(0, pos + delimiter.length());
	}

	return tokens;
}


int main(int argc, char* argv[]) {
	// char* filename = getCmdOption(argv, argv + argc, "-f");
	char* streamsChar = getCmdOption(argv, argv + argc, "-s");


	loadData();

	/*for (int i = 0; i < 5000; i++)
		cout << i << ":" << getMakespan(generateChromosome(jobs), jobs, machines) << endl;*/


	runGA(16,1);
	return 0;
}
