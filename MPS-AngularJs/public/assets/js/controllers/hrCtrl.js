app.controller('hrCtrl',function($scope,$interval,$http){
    console.log('hello from hrCtrl');
    
    $scope.employees=[
      // {
      //   employeeId:1,
      //   name:'Ali',
      //   designation:'Worker',
      //   department:'Sales',
      //   gender:'Male',
      //   salary:5000
      // }
    ];

    // $http.get('/employeeRoute').then(function(resp){
    //   console.log(resp.data);
    //   $scope.employees=resp.data;
    // });

    


    $scope.employeeSalesMF=[13,25];
    $scope.employeeEngMF=[113,27];
    $scope.employeeInventoryMF=[53,26];
    $scope.employeeHrMF=[30,43];


    $scope.employeeSales=$scope.employeeSalesMF[0]+$scope.employeeSalesMF[1];
    $scope.employeeHr=$scope.employeeHrMF[0]+$scope.employeeHrMF[1];
    $scope.employeeEng=$scope.employeeEngMF[0]+$scope.employeeEngMF[1];
    $scope.employeeInventory=$scope.employeeInventoryMF[0]+$scope.employeeInventoryMF[1];

    $scope.employeesCount=$scope.employeeSales+$scope.employeeHr+$scope.employeeInventory+$scope.employeeEng;
    
    $scope.disabled=true;

    $scope.edit=function(){
        $scope.disabled=false;
    };

    var emp={
      employeeId:-1,
      name:'abc',
      designation:'designation',
      department:'dept',
      gender:'gender',
      salary:0
    };

    $scope.addNew=function(){
      $scope.employees.push(emp);
      // $http.post('/addEmployee',emp);
    }

    $scope.update=function(obj){

      console.log(obj);

      $http.post('/employeeRoute',{employee:obj})
      .then(function(resp){
        console.log(resp);
      });

      $scope.disabled=true;
    };

    $scope.remove=function(obj,id){

      // console.log(obj);
      // return;

      console.log('before',$scope.employees);
      $http.post('/employeeRoute/delete',{employee:obj})
        .then(function(resp){
          console.log(resp);
          $scope.employees.splice(id,1);
          console.log('after',$scope.employees);
      });
    }
    
    //donut for sales
    $scope.salesDonut = {
        chart: {
          doughnutRadius:"70",
          pieRadius:"100",
          caption: "Sales Department",
          subcaption: "Males vs Females",
          showpercentvalues: "1",
          defaultcenterlabel: "Employees",
          aligncaptionwithcanvas: "0",
          captionpadding: "0",
          decimals: "1",
          plottooltext:
            "<b>$percentValue</b> of our sales <br>department are <b>$label</b>",
          centerlabel: "Total $label : $value",
          theme: "fusion"
        },
        data: [
          {
            label: "Males",
            value: ""+$scope.employeeSalesMF[0],
            color:"#123456"
          },
          {
            label: "Females",
            value: ""+$scope.employeeSalesMF[1],
          }
        ]
    };
    //donut for HR
    $scope.hrDonut = {
        chart: {
          doughnutRadius:"70",
          pieRadius:"100",
          caption: "HR Department",
          subcaption: "Males vs Females",
          showpercentvalues: "1",
          defaultcenterlabel: "Employees",
          aligncaptionwithcanvas: "0",
          captionpadding: "0",
          decimals: "1",
          plottooltext:
            "<b>$percentValue</b> of our hr <br>department are <b>$label</b>",
          centerlabel: "Total $label : $value",
          theme: "fusion"
        },
        data: [
          {
            label: "Males",
            value: ""+$scope.employeeHrMF[0],
            color:"#123456"
          },
          {
            label: "Females",
            value: ""+$scope.employeeHrMF[1],
          }
        ]
    };
    //donut for engineering
    $scope.engineeringDonut = {
        chart: {
          doughnutRadius:"70",
          pieRadius:"100",
          caption: "Engineering Department",
          subcaption: "Males vs Females",
          showpercentvalues: "1",
          defaultcenterlabel: "Employees",
          aligncaptionwithcanvas: "0",
          captionpadding: "0",
          decimals: "1",
          plottooltext:"<b>$percentValue</b> of our engineering <br>department are <b>$label</b>",
          centerlabel: "Total $label : $value",
          theme: "fusion"
        },
        data: [
          {
            label: "Males",
            value: ""+$scope.employeeEngMF[0],
            color:"#123456"
          },
          {
            label: "Females",
            value: ""+$scope.employeeEngMF[1],
          }
        ]
    };
    //donut for inventory
    $scope.inventoryDonut = {
        chart: {
          doughnutRadius:"70",
          pieRadius:"100",
          caption: "Inventory Department",
          subcaption: "Males vs Females",
          showpercentvalues: "1",
          defaultcenterlabel: "Employees",
          aligncaptionwithcanvas: "0",
          captionpadding: "0",
          decimals: "1",
          plottooltext:
            "<b>$percentValue</b> of our inventory department <br>are <b>$label</b>",
          centerlabel: "Total $label : $value",
          theme: "fusion"
        },
        data: [
          {
            label: "Males",
            value: ""+$scope.employeeInventoryMF[0],
            color:"#123456"
          },
          {
            label: "Females",
            value: ""+$scope.employeeInventoryMF[1],
          }
        ]
    };

    //2d bar chart for salaries ranges
    $scope.barChartSalaryRanges={
        chart:{
            "theme": "fusion",
            "caption": "No. Of Employees by Monthly Salary",
            "subCaption": "Last month salaries",
            "yAxisName": "No. Of Employees",
            "numberPrefix": "",
            "alignCaptionWithCanvas": "0",
            "plottooltext":"No. of employees having <br> salaries $label are <b>$value</b>",
        },
        data:[
            {
                "label": "greater than 2,00,000",
                "value": "33"
            },
            {
                "label": "between 1,00,000 & 2,00,000",
                "value": "15"
            },
            {
                "label": "less than 1,00,000",
                "value": "80"
            },
        ]
    };

    //donut of total employees per department
    $scope.totolEmployeesDonut = {
        chart: {
          doughnutRadius:"70",
          pieRadius:"100",
          caption: "Employees Composition by Department",
          subcaption: "Sales vs Engineering vs HR vs Inventory",
          showpercentvalues: "1",
          defaultcenterlabel: "Employees",
          aligncaptionwithcanvas: "0",
          captionpadding: "0",
          decimals: "1",
          plottooltext:
            "<b>$percentValue</b> of our total employees <br> are from <b>$label</b> Dept.",
          centerlabel: "Total $label Employees : $value",
          theme: "fusion"
        },
        data: [
          {
            label: "Sales",
            value: ""+$scope.employeeSales,
            color:"#123456"
          },
          {
            label: "Engineering",
            value: ""+$scope.employeeEng
          },
          {
            label: "HR",
            value: ""+$scope.employeeHr
          },
          {
            label: "Inventory",
            value: ""+$scope.employeeInventory
          }
        ]
    };

    //employees per last 5 years chart
    $scope.employeesPerYearChart = {
        chart: {
          caption: "Number Of Employees",
          subcaption: "Employees Count",
          xaxisname: "Year",
          yaxisname: "employees Count",
          numbersuffix: "",
          theme: "fusion",
          "plottooltext":"No. of employees <br> $label are <b>$value</b>",
        },
        data: [
          {
            label: "Sales",
            value: ""+$scope.employeeSales
          },
          {
            label: "Engineering",
            value: ""+$scope.employeeEng
          },
          {
            label: "HR",
            value: ""+$scope.employeeHr
          },
          {
            label: "Inventory",
            value: ""+$scope.employeeInventory
          }
        ]
    };


    
    // $interval(function(){
      // console.log('fetching employees');

      $scope.employees.forEach(element => {

        // console.log(element)

        if(element.department=="Sales"){
          $scope.employeeSales=$scope.employeeSales+1;
          if(element.gender=="Male"){
            $scope.employeeSalesMF[0]=$scope.employeeSalesMF[0]+1;
          }else{
            $scope.employeeSalesMF[1]=$scope.employeeSalesMF[1]+1;
          }
        }else if(element.department=="HR"){
          $scope.employeeHr=$scope.employeeSales+1;
          if(element.gender=="Male"){
            $scope.employeeHrMF[0]=$scope.employeeHrMF[0]+1;
          }else{
            $scope.employeeHrMF[1]=$scope.employeeHrMF[1]+1;
          }
        }else if(element.department=="Inventory"){
          $scope.employeeInventory=$scope.employeeSales+1;
          if(element.gender=="Male"){
            $scope.employeeInventoryMF[0]=$scope.employeeInventoryMF[0]+1;
          }else{
            $scope.employeeInventoryMF[1]=$scope.employeeInventoryMF[1]+1;
          }
        }else if(element.department=="Engineering"){
          $scope.employeeEng=$scope.employeeSales+1;
          if(element.gender=="Male"){
            $scope.employeeEngMF[0]=$scope.employeeEngMF[0]+1;
          }else{
            $scope.employeeEngMF[1]=$scope.employeeEngMF[1]+1;
          }
        }
      });


    // },5000)

    $scope.refresh=function(){
      $http.get('/employeeRoute').then(function(resp){
        console.log(resp.data);
        $scope.employees=resp.data;
      });
    };

});