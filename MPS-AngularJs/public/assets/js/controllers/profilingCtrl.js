app.controller('profilingCtrl',function($scope,$rootScope){
    console.log('hello from profilingCtrl');

  $scope.profilingBarChart1 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "2 Instances & 1 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "2"
        },
        {
          label: "Python",
          value: "4"
        },
        {
          label: "C++ Serial",
          value: "63"
        },
        {
          label: "C++ Threaded",
          value: "1"
        }
      ]
  };
  $scope.profilingBarChart2 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "2 Instances & 10 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "8"
        },
        {
          label: "Python",
          value: "29"
        },
        {
          label: "C++ Serial",
          value: "348"
        },
        {
          label: "C++ Threaded",
          value: "13"
        }
      ]
  };
  $scope.profilingBarChart3 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "2 Instances & 100 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "44"
        },
        {
          label: "Python",
          value: "274"
        },
        {
          label: "C++ Serial",
          value: "3219"
        },
        {
          label: "C++ Threaded",
          value: "97"
        }
      ]
  };
  $scope.profilingBarChart4 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "8 Instances & 1 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "10"
        },
        {
          label: "Python",
          value: "12"
        },
        {
          label: "C++ Serial",
          value: "258"
        },
        {
          label: "C++ Threaded",
          value: "5"
        }
      ]
  };
  $scope.profilingBarChart5 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "8 Instances & 10 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "27"
        },
        {
          label: "Python",
          value: "109"
        },
        {
          label: "C++ Serial",
          value: "1398"
        },
        {
          label: "C++ Threaded",
          value: "43"
        }
      ]
  };
  $scope.profilingBarChart6 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "8 Instances & 100 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "125"
        },
        {
          label: "Python",
          value: "1093"
        },
        {
          label: "C++ Serial",
          value: "12818"
        },
        {
          label: "C++ Threaded",
          value: "357"
        }
      ]
  };
  $scope.profilingBarChart7 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "16 Instances & 1 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "11"
        },
        {
          label: "Python",
          value: "23"
        },
        {
          label: "C++ Serial",
          value: "505"
        },
        {
          label: "C++ Threaded",
          value: "10"
        }
      ]
  };
  $scope.profilingBarChart8 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "16 Instances & 10 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "44"
        },
        {
          label: "Python",
          value: "223"
        },
        {
          label: "C++ Serial",
          value: "2768"
        },
        {
          label: "C++ Threaded",
          value: "76"
        }
      ]
  };
  $scope.profilingBarChart9 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "16 Instances & 100 Iteration",
        xaxisname: "Technology",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label is <b>$value</b> ms",
      },
      data: [
        {
          label: "Tabu Search",
          value: "236"
        },
        {
          label: "Python",
          value: "2187"
        },
        {
          label: "C++ Serial",
          value: "25463"
        },
        {
          label: "C++ Threaded",
          value: "696"
        }
      ]
  };
  $scope.profilingBarChart10 = {
      chart: {
        caption: "Profiling Results",
        subcaption: "Using 1 Stream With CUDA",
        xaxisname: "No Of Runs",
        yaxisname: "Processing Time (ms)",
        numbersuffix: "",
        theme: "fusion",
        "plottooltext":"Processing Time for $label iterations is <b>$value</b> ms",
      },
      data: [
        {
          label: "1",
          value: "160"
        },
        {
          label: "10",
          value: "500"
        },
        {
          label: "100",
          value: "1100"
        },
        {
          label: "1000",
          value: "5200"
        }
      ]
  };

});
