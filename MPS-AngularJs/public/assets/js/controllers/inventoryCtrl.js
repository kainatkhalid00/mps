app.controller('inventoryCtrl',function($rootScope,$scope,$http,$location){
    console.log('hello from inventoryCtrl');

    
    var products = [
        {
            id: 1,
            description: 'Fuselage',
            inStockQuantity: 34,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 2,
            description: 'Wing',
            inStockQuantity: 14,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 3,
            description: 'Tail',
            inStockQuantity: 64,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 4,
            description: 'Elevator',
            inStockQuantity: 44,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 5,
            description: 'Rudder',
            inStockQuantity: 24,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 6,
            description: 'Flaps',
            inStockQuantity: 3,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 7,
            description: 'Engine',
            inStockQuantity: 34,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 8,
            description: 'Propeller',
            inStockQuantity: 341,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 9,
            description: 'Spinner',
            inStockQuantity: 34,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 10,
            description: 'Cockpit',
            inStockQuantity: 344,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        }
    ];

    $rootScope.products=[];

    $http.get('/inventoryRoute').then(function(resp){
        console.log(resp.data);
        $scope.products=resp.data;
    });

    $scope.disabled=true;

    $scope.edit=function(){
        $scope.disabled=false;
    };

    $scope.addNew=function(){
        $scope.disabled=false;
        var arr=$rootScope.products;
        $rootScope.products.push(
            {
                id: arr.length+1,
                description: 'abc',
                inStockQuantity: 0,
                newOrderQuantity: 1,
                actualQuantity:0,
                avaiableRaw:5,
                requestedRaw:0,
                daysToRecieveRaw:0
            }
        );
    };

    $scope.remove=function(obj){
        console.log(obj);
        products.splice(obj,1);
    };
    
    $scope.update=function(id,qty,rqty,obj){
        // var arr=$scope.products;
        // var obj=arr[id-1];
        // console.log('before = ',arr[id-1]);
        // console.log('before = ',$rootScope.products[id-1].inStockQuantity);
        // $rootScope.products[id-1].inStockQuantity=qty;
        // $rootScope.products[id-1].avaiableRaw=rqty;
        // console.log('after = ',$rootScope.products[id-1].inStockQuantity);
        // products[i-1].id=id;
        // products[i-1].description=d;
        // arr.splice(id,1);
        // $rootScope.push(obj);
        $http.post('/inventoryRoute',{products:obj})
        .then(function(resp){
          console.log(resp);
          location.reload();
        });
        $scope.disabled=true;
    };



});