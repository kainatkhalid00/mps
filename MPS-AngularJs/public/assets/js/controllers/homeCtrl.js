app.controller('homeCtrl',function($scope,$rootScope){
    console.log('hello from homeCtrl');

    var products = [
        {
            id: 1,
            description: 'Fuselage',
            inStockQuantity: 34,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 2,
            description: 'Wing',
            inStockQuantity: 14,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 3,
            description: 'Tail',
            inStockQuantity: 64,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 4,
            description: 'Elevator',
            inStockQuantity: 44,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 5,
            description: 'Rudder',
            inStockQuantity: 24,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 6,
            description: 'Flaps',
            inStockQuantity: 3,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 7,
            description: 'Engine',
            inStockQuantity: 34,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 8,
            description: 'Propeller',
            inStockQuantity: 341,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 9,
            description: 'Spinner',
            inStockQuantity: 34,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        },
        {
            id: 10,
            description: 'Cockpit',
            inStockQuantity: 344,
            newOrderQuantity: 1,
            actualQuantity:0,
            avaiableRaw:5,
            requestedRaw:0,
            daysToRecieveRaw:0
        }
    ];

    $rootScope.products=products;

});