app.controller('mpsCtrl',function($http,$scope,$location,$rootScope){
    console.log('hello from mpsCtrl');

    var jssData=$rootScope.jssData;

    $scope.jssData=jssData;

    $scope.barChartNewOrder={
        chart:{
            "theme": "fusion",
            "caption": "Schedule And Duration Of New Order",
            "subCaption": "New Order Schedule",
            "yAxisName": "Duration",
            "xAxisName": "Products",
            "numberPrefix": "",
            "alignCaptionWithCanvas": "0",
            "plottooltext":"Expected duration of <br> product $label is <b>$value</b> units",
        },
        data:[
            {
                "label": "A",
                "value": "33"
            },
            {
                "label": "B",
                "value": "15",
                // color:"#123456"
            },
            {
                "label": "F",
                "value": "35"
            },
            {
                "label": "C",
                "value": "80"
            },
            {
                "label": "D",
                "value": "55"
            },
            {
                "label": "E",
                "value": "65"
            },
        ]
    };

    

    // console.log($rootScope.jssData);


    //http request to server
    // $http.get('/getMakespanForMPS',$rootScope.jssData).then(function(res){
    //     console.log(res);
    // });

});