app.controller('jobsCtrl', function ($http, $scope, $rootScope, $location,$timeout      ) {

        console.log('hello from jobsCtrl');

        $scope.status=['Waiting','InProgress','Completed'];

        $scope.disabled=true;

        $scope.statusButton=['Start','Recieved','Done'];


        //workers list
        $rootScope.workers=[];
        //         'Ali',
        //         'Umer',
        //         'Asad',
        //         'Ayan',
        //         'Ayat',
        //         'Kainat',
        //         'Zakriya',
        //         'Shoaib'
        // ];

        //busy workers count
        // var busyWorkers=0;
        // $scope.busyWorkers=busyWorkers;
        $scope.busyWorkers=0;

        //new job obj
        // $scope.newJob={
        //         allotedWorker:$scope.workers[$scope.busyWorkers],
        //         jobId:Math.round(Math.random()*10000)+$scope.busyWorkers,
        //         jobType:'',
        //         jobDuration:0,
        //         startTime:'------',
        //         endTime:'------',
        //         jobStatus:0
        // };

        //jobs holder array
        $rootScope.jobs=[];
                // {
                //         allotedWorker:'Hamid',
                //         jobId:7897,
                //         jobType:'BW',
                //         jobDuration:30,
                //         startTime:'------',
                //         endTime:'------',
                //         jobStatus:0,
                // }
        // ];
        
        $http.get('/jobsRoute').then(function(resp){
                console.log(resp.data);
                $rootScope.jobs=resp.data;
        });

        $http.get('/workerRoute').then(function(resp){
                console.log(resp.data);
                var temp=resp.data;
                temp.forEach(function(element) {
                        $rootScope.workers.push(element.name);
                }, this);
                // $scope.workers=;
        });

        $scope.edit=function(){
                $scope.disabled=false;
        };

        $scope.updateJobStatus=function(id){
                var i=0;
                for(i=0;i<$rootScope.jobs.length;i++){
                        if($rootScope.jobs[i].jobId==id){
                                break;
                        }
                }
                if($rootScope.jobs[i].jobStatus==0){
                        $rootScope.jobs[i].startTime=new Date().toLocaleTimeString();
                        $rootScope.jobs[i].jobStatus=1;
                }else if($rootScope.jobs[i].jobStatus==1){
                        $rootScope.jobs[i].endTime=new Date().toLocaleTimeString();
                        $rootScope.jobs[i].jobStatus=2;  
                }
                
                // $http.post('/jobsRoute',{jobs:$rootScope.jobs[i]}).then(function(resp){
                //         console.log(resp.data);

                //         location.reload();
                // });

        };

        $scope.addNew=function(){
                // $rootScope.wait=$rootScope.wait+1;
                // $scope.busyWorkers=($scope.busyWorkers+1)%$scope.workers.length;
                // console.log($scope.busyWorkers);
                // return;
                $scope.disabled=false;
                var jobObj={
                        allotedWorker:$rootScope.workers[(Math.round(Math.random()*$rootScope.workers.length))%$rootScope.workers.length],
                        jobId:-1,
                        jobType:'',
                        jobDuration:0,
                        startTime:'------',
                        endTime:'------',
                        jobStatus:0
                };
                // var newJob=Object.assign({},$scope.newJob);
                $rootScope.jobs.push(jobObj);

        };

        var jobStatus={};//jobStatusRoute
        $http.get('/jobStatusRoute')
        .then(function(resp){
                jobStatus=resp.data;
        });

        //update a job
        $scope.update=function(job,id){
                console.log(job);

                
                
                $http.post('/jobsRoute',{jobs:job}).then(function(resp){
                        console.log(resp.data);
                        $scope.disabled=true;
                        if(job.jobStatus==0){
                                jobStatus.wait=jobStatus.wait+1;
                        }
                        else if(job.jobStatus==1){
                                jobStatus.wait=jobStatus.wait-1;
                                jobStatus.wip=jobStatus.wip+1;
                        }
                        else if(job.jobStatus==2){
                                jobStatus.wip=jobStatus.wip-1;
                                jobStatus.done=jobStatus.done+1;
                        }
                        console.log(jobStatus);
                }).then(function(){
                        $http.post('/jobStatusRoute',{jobStatus:jobStatus})
                        .then(function(resp){
                                console.log(resp.data);
                                location.reload();
                                
                        });
                });
                
                

        }

        //remove a job from jobsList
        $scope.remove=function(obj){



                // var oldJobs=Array..assign({},$rootScope.jobs);
                $http.post('/jobsRoute/delete',{jobs:obj})
                        .then(function(resp){
                        console.log(resp);
                        console.log('before',$rootScope.jobs);
                        $rootScope.jobs.splice(obj,1);
                        console.log('after',$rootScope.jobs);
                });
                
                // $rootScope.jobs=oldJobs;
        }

        // $rootScope.wip=0;
        // $rootScope.done=0;
        // $rootScope.wait=0;



        // $timeout(function(){

        //         console.log($rootScope.jobs);

        //         $rootScope.jobs.forEach(element => {
                        

        //                 if(element.jobStatus==0){
        //                         $rootScope.wait=$rootScope.wait+1;
        //                 }
        //                 else if(element.jobStatus==1){
        //                         $rootScope.wait=$rootScope.wait-1;
        //                         $rootScope.wip=$rootScope.wip+1;
        //                 }
        //                 else if(element.jobStatus==2){
        //                         $rootScope.wip=$rootScope.wip-1;
        //                         $rootScope.done=$rootScope.done+1;
        //                 }
        //         });
        // })
});