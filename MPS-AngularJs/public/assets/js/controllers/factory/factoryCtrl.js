app.controller('factoryCtrl', function ($http,$scope,$rootScope, $location){

    console.log('hello from factoryCtrl');
    
    $scope.wip=0;
    $scope.done=0;
    $scope.wait=0;

    $scope.viewMachines=function(){
        $location.path('/machines');
    }

    $scope.viewJobs=function(){
        $location.path('/jobs');
    };

    var jobStatus={};//jobStatusRoute
    $http.get('/jobStatusRoute')
    .then(function(resp){
        jobStatus=resp.data;

        console.log(jobStatus);
        
        $scope.wip=jobStatus.wip;
        $scope.done=jobStatus.done;
        $scope.wait=jobStatus.wait;
    });

    // $rootScope.machines=0;

});