
app.controller('machinesCtrl', function ($http,$scope,$rootScope, $location){
    
    console.log('hello from machinesCtrl');

    $scope.disabled=true;
    $scope.status=["Working","Stopped"];

    $scope.machines=[];
    //     {
    //         machineId:1,
    //         machineType:"RM",
    //         machineCapacity:100,
    //         machineInstallationDate:"11-12-1990",
    //         machineStatus:0
    //     }
    // ];

    $http.get('/machineRoute').then(function(resp){
        console.log(resp.data);
        $scope.machines=resp.data;
    });

    $scope.edit=function(){
        $scope.disabled=false;
    }

    $scope.update=function(i,mac){
        // $scope.machines[i].machineId=$scope.machineId;
        // $scope.machines[i].machineType=$scope.machineType;
        // $scope.machines[i].machineInstallationDate=$scope.machineInstallationDate;
        // $scope.machines[i].machineCapacity=$scope.machineCapacity;
        // $scope.machines[i].machineStatus=$scope.status[0];

        console.log(mac);
        // return;

        $http.post('/machineRoute',{machines:mac}).then(function(resp){
            console.log(resp.data);
            $scope.machines=resp.data;
            location.reload();
        });

    }

    $scope.updateMachineStatus=function(m,i){
       $scope.machines[i].machineStatus=($scope.machines[i].machineStatus+1)%2;
    }

    $scope.addNew=function(){
        // $scope.machines[i].machineStatus=$scope.status[0];
        var arr=$scope.machines;
        $scope.machines.push(
            {
                machineId:arr.length+1,
                machineType:"",
                machineCapacity:0,
                // machineInstallationDate:new Date().getUTCDate,
                machineStatus:1
            }
            
        );
    };

    $scope.remove=function(i,mac){
        console.log('i = ',i,'mac= ',mac);
        $http.post('/machineRoute/delete',{machines:mac}).then(function(resp){
            console.log(resp.data);
            // $scope.machines=resp.data;
            console.log('before',$scope.machines);
            $scope.machines.splice(i,1);
            console.log('after',$scope.machines);
            location.reload();
        });
    }

});