app.controller('workerCtrl', function ($http, $scope, $rootScope, $location, $timeout) {
  console.log('hello from workersCtrl')

  $scope.status = ['Waiting', 'InProgress', 'Completed']

  $rootScope.isWorker = true;
  $scope.disabled = false;
  $scope.thisWorker = true;

  $scope.statusButton = ['Start', 'Recieved', 'Done']

  $rootScope.workers = []
  $scope.busyWorkers = 0

  // jobs holder array
  $rootScope.jobs = []

  $http.get('/jobsRoute').then(function (resp) {
    console.log(resp.data)
    $rootScope.jobs = resp.data
  })

  $http.get('/workerRoute').then(function (resp) {
    console.log(resp.data)
    var temp = resp.data
    temp.forEach(function (element) {
      $rootScope.workers.push(element.name)
    }, this)
  })

  $scope.edit = function () {
    $scope.disabled = false
  }

  $scope.updateJobStatus = function (id) {
    var i = 0
    for (i = 0;i < $rootScope.jobs.length;i++) {
      if ($rootScope.jobs[i].jobId == id) {
        break;
      }
    }
    if ($rootScope.jobs[i].jobStatus == 0) {
      $rootScope.jobs[i].startTime = new Date().toLocaleTimeString()
      $rootScope.jobs[i].jobStatus = 1
    }else if ($rootScope.jobs[i].jobStatus == 1) {
      $rootScope.jobs[i].endTime = new Date().toLocaleTimeString()
      $rootScope.jobs[i].jobStatus = 2
    }
  }

  var jobStatus = {} // jobStatusRoute
  $http.get('/jobStatusRoute')
    .then(function (resp) {
      jobStatus = resp.data
    })

  // update a job
  $scope.update = function (job, id) {
    console.log(job)

    $http.post('/jobsRoute', {jobs: job}).then(function (resp) {
      console.log(resp.data)
      $scope.disabled = true
      if (job.jobStatus == 0) {
        jobStatus.wait = jobStatus.wait + 1
      }
      else if (job.jobStatus == 1) {
        jobStatus.wait = jobStatus.wait - 1
        jobStatus.wip = jobStatus.wip + 1
      }
      else if (job.jobStatus == 2) {
        jobStatus.wip = jobStatus.wip - 1
        jobStatus.done = jobStatus.done + 1
      }
      console.log(jobStatus)
    }).then(function () {
      $http.post('/jobStatusRoute', {jobStatus: jobStatus})
        .then(function (resp) {
          console.log(resp.data)
          location.reload()
        })
    })
  };

  $scope.viewMyJobs=function(wid){
	  $http.post('/workerRoute/name',{id:wid}).then(function(resp){
		$rootScope.jobs=resp.data;
		console.log(resp.data);
		
		$scope.thisWorker = false;
	  });
  };
});
