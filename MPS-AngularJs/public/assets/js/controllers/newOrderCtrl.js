app.controller('newOrderCtrl', function ($http,$scope,$rootScope, $location){//, jssOrdersService) {
    console.log('hello from newOrderCtrl');

    // $scope.parts=[{"Fuselage"},{"Wing"},{"Tail"},{"Elevator"},{"Rudder"},{"Flaps"},{"Engine"},{"Propeller"},{"Spinner"},{"Cockpit"}]

    $rootScope.jssData={fabricationOrders:[],machines:$scope.machines,orderName:"newOrder"};

    // $rootScope.products = [
    //     {
    //         id: 1,
    //         description: 'Fuselage',
    //         inStockQuantity: 34,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 2,
    //         description: 'Wing',
    //         inStockQuantity: 14,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 3,
    //         description: 'Tail',
    //         inStockQuantity: 64,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 4,
    //         description: 'Elevator',
    //         inStockQuantity: 44,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 5,
    //         description: 'Rudder',
    //         inStockQuantity: 24,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 6,
    //         description: 'Flaps',
    //         inStockQuantity: 3,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 7,
    //         description: 'Engine',
    //         inStockQuantity: 34,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 8,
    //         description: 'Propeller',
    //         inStockQuantity: 341,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 9,
    //         description: 'Spinner',
    //         inStockQuantity: 34,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     },
    //     {
    //         id: 10,
    //         description: 'Cockpit',
    //         inStockQuantity: 344,
    //         newOrderQuantity: 1,
    //         actualQuantity:0,
    //         avaiableRaw:5,
    //         requestedRaw:0,
    //         daysToRecieveRaw:0
    //     }
    // ];


    $scope.products=[];
    $http.get('/inventoryRoute').then(function(resp){
        console.log(resp.data);
        $scope.products=resp.data;
    });

    $scope.update=function(qty,id){
        var min=5;
        var max=20;
        $scope.products[id].newOrderQuantity=qty;
        if($scope.products[id].newOrderQuantity>$scope.products[id].inStockQuantity){
            $scope.products[id].actualQuantity=Math.abs($scope.products[id].newOrderQuantity-$scope.products[id].inStockQuantity);
        }else{
            $scope.products[id].actualQuantity=0;
        }
        if($scope.products[id].actualQuantity>$scope.products[id].avaiableRaw){
            $scope.products[id].requestedRaw=Math.abs($scope.products[id].actualQuantity-$scope.products[id].avaiableRaw);
        }else{
            $scope.products[id].requestedRaw=0;
        }
        $scope.products[id].daysToRecieveRaw=Math.round(min+Math.random()*(max-min));
    }


    $scope.selectedProducts = []

    //product added to list by clicking add
    $scope.addToSelectedProducts = function (obj) {
        if ($scope.selectedProducts.indexOf(obj) == -1) {
            $scope.selectedProducts.push(obj);
        }
    };

    var foId=0;

    $scope.updateQuantity = function (q, i) {
        // $scope.products[i].newOrderQuantity = q;
        for(var t=0;t<q;t++){
            var job=Object.assign({},$scope.fos[i]);
            job.FOID=foId;
            // console.log($scope.fos[i]);
            $rootScope.jssData.fabricationOrders.push(job);
            foId++;
        }
    };

    // $scope.removeQuantity=function(i){

    // };

    $scope.msg="";
    $scope.ifMsg=true;
    $scope.buildShow=false;
    $scope.confirmShow=true;

    $scope.confirmOrder = function () {
        if($scope.newOrderName===undefined || $scope.newOrderName==""){
            alert('please enter a valid order name first');
            return;
        }

        if($scope.selectedProducts.length==0){
            alert('please select products first');
            return;
        };

        if($rootScope.jssData.fabricationOrders.length==0){
            alert('add products first');
            return;
        }

        //http request to server
        $rootScope.jssData.orderName=$scope.newOrderName;
        var jssData=$rootScope.jssData;
        $http.post('/getMakespanForMPS',jssData)
        .then(function(res){
            console.log(res.data);

            $scope.ifMsg=false;
            $scope.msg="order has been placed";

            $scope.buildShow=true;
            $scope.confirmShow=false;

            $scope.newOrderName="";
            $rootScope.jssData.fabricationOrders=[];
            $rootScope.jssData.orderName="newOrder";
            
        });
        
        $scope.executeShow=false;

        $scope.buildJss=function(){

            $scope.msg="building JSS ...";
            $http.post('/buildJss',{data:"test"}).then(function(resp){
                console.log(resp.data)
                $scope.msg=resp.data;
                $scope.executeShow=true;
                $scope.buildShow=false;
            });
        };

        $scope.executeJss=function(){

            $scope.buildJss=false;
            $scope.msg="Executing JSS..."

            $http.post('/runJss',{data:"test"}).then(function(resp){
                console.log(resp.data)
                $scope.msg="Schedule Generated";
                $scope.viewScheduleShow=true;
                $scope.executeShow=false;
            });
        }

        $scope.viewScheduleShow=false;
        $scope.viewSchedule=function(){

            $scope.msg="Opening Gantt,In Please Wait ..."

            $http.post('/generateGantt',{data:"test"}).then(function(resp){
                // console.log(resp.data)
                $scope.msg="Gantt Generated";
            });
        }
        // .then(function(){
            // $location.path('/');
            // $rootScope.jssData[0].fos=[];
            // $rootScope.jssData[2].orderName="newOrder";
            // $scope.newOrderName="";
        // });

        // console.log($scope.selectedProducts);
        // $rootScope.jssData=$scope.selectedProducts;

        // $scope.$emit('jssEvent',$scope.selectedProducts);

        // $location.path('/mps');
        // $scope.$apply()//used this if we to changed the URL and add it to the history stack

    };

    //fabrication orders list
    $scope.fos = [
        {
            'FOID': 0,
            'description': 'Fuselage',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 0,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 0,
                'operationDuration': 660,
                'mappable': true
            },
            {
                'operationID': 1,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 1,
                'operationDuration': 75,
                'mappable': true
            },
            {
                'operationID': 2,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 2,
                'operationDuration': 870,
                'mappable': true
            },
            {
                'operationID': 3,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 3,
                'operationDuration': 1455,
                'mappable': true
            },
            {
                'operationID': 4,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 4,
                'operationDuration': 135,
                'mappable': true
            },
            {
                'operationID': 5,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 5,
                'operationDuration': 1260,
                'mappable': true
            },
            {
                'operationID': 6,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 6,
                'operationDuration': 1155,
                'mappable': true
            },
            {
                'operationID': 7,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 7,
                'operationDuration': 1440,
                'mappable': true
            },
            {
                'operationID': 8,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 8,
                'operationDuration': 870,
                'mappable': true
            },
            {
                'operationID': 9,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 9,
                'operationDuration': 1335,
                'mappable': true
            }]
        },
        {
            'FOID': 1,
            'description': 'Wing',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 10,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 0,
                'operationDuration': 225,
                'mappable': true
            },
            {
                'operationID': 11,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 1,
                'operationDuration': 465,
                'mappable': true
            },
            {
                'operationID': 12,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 2,
                'operationDuration': 1305,
                'mappable': true
            },
            {
                'operationID': 13,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 3,
                'operationDuration': 855,
                'mappable': true
            },
            {
                'operationID': 14,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 4,
                'operationDuration': 1155,
                'mappable': true
            },
            {
                'operationID': 15,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 5,
                'operationDuration': 1275,
                'mappable': true
            },
            {
                'operationID': 16,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 6,
                'operationDuration': 1215,
                'mappable': true
            },
            {
                'operationID': 17,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 7,
                'operationDuration': 585,
                'mappable': true
            },
            {
                'operationID': 18,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 8,
                'operationDuration': 1095,
                'mappable': true
            },
            {
                'operationID': 19,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 9,
                'operationDuration': 315,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 2,
            'description': 'Cockpit',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 20,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 0,
                'operationDuration': 1230,
                'mappable': true
            },
            {
                'operationID': 21,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 1,
                'operationDuration': 330,
                'mappable': true
            },
            {
                'operationID': 22,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 2,
                'operationDuration': 150,
                'mappable': true
            },
            {
                'operationID': 23,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 3,
                'operationDuration': 1050,
                'mappable': true
            },
            {
                'operationID': 24,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 4,
                'operationDuration': 735,
                'mappable': true
            },
            {
                'operationID': 25,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 5,
                'operationDuration': 600,
                'mappable': true
            },
            {
                'operationID': 26,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 6,
                'operationDuration': 510,
                'mappable': true
            },
            {
                'operationID': 27,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 7,
                'operationDuration': 720,
                'mappable': true
            },
            {
                'operationID': 28,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 8,
                'operationDuration': 1200,
                'mappable': true
            },
            {
                'operationID': 29,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 9,
                'operationDuration': 1065,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 3,
            'description': 'Propeller',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 30,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 0,
                'operationDuration': 1365,
                'mappable': true
            },
            {
                'operationID': 31,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 1,
                'operationDuration': 255,
                'mappable': true
            },
            {
                'operationID': 32,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 2,
                'operationDuration': 930,
                'mappable': true
            },
            {
                'operationID': 33,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 3,
                'operationDuration': 1125,
                'mappable': true
            },
            {
                'operationID': 34,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 4,
                'operationDuration': 705,
                'mappable': true
            },
            {
                'operationID': 35,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 5,
                'operationDuration': 165,
                'mappable': true
            },
            {
                'operationID': 36,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 6,
                'operationDuration': 105,
                'mappable': true
            },
            {
                'operationID': 37,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 7,
                'operationDuration': 1080,
                'mappable': true
            },
            {
                'operationID': 38,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 8,
                'operationDuration': 525,
                'mappable': true
            },
            {
                'operationID': 39,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 9,
                'operationDuration': 825,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 4,
            'description': 'Engine',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 40,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 0,
                'operationDuration': 1065,
                'mappable': true
            },
            {
                'operationID': 41,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 1,
                'operationDuration': 1350,
                'mappable': true
            },
            {
                'operationID': 42,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 2,
                'operationDuration': 1125,
                'mappable': true
            },
            {
                'operationID': 43,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 3,
                'operationDuration': 960,
                'mappable': true
            },
            {
                'operationID': 44,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 4,
                'operationDuration': 1410,
                'mappable': true
            },
            {
                'operationID': 45,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 5,
                'operationDuration': 225,
                'mappable': true
            },
            {
                'operationID': 46,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 6,
                'operationDuration': 180,
                'mappable': true
            },
            {
                'operationID': 47,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 7,
                'operationDuration': 1005,
                'mappable': true
            },
            {
                'operationID': 48,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 8,
                'operationDuration': 300,
                'mappable': true
            },
            {
                'operationID': 49,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 9,
                'operationDuration': 750,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 5,
            'description': 'Spinner',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 50,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 0,
                'operationDuration': 1050,
                'mappable': true
            },
            {
                'operationID': 51,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 1,
                'operationDuration': 1395,
                'mappable': true
            },
            {
                'operationID': 52,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 2,
                'operationDuration': 1155,
                'mappable': true
            },
            {
                'operationID': 53,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 3,
                'operationDuration': 435,
                'mappable': true
            },
            {
                'operationID': 54,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 4,
                'operationDuration': 870,
                'mappable': true
            },
            {
                'operationID': 55,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 5,
                'operationDuration': 1395,
                'mappable': true
            },
            {
                'operationID': 56,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 6,
                'operationDuration': 1020,
                'mappable': true
            },
            {
                'operationID': 57,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 7,
                'operationDuration': 855,
                'mappable': true
            },
            {
                'operationID': 58,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 8,
                'operationDuration': 105,
                'mappable': true
            },
            {
                'operationID': 59,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 9,
                'operationDuration': 780,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 6,
            'description': 'Flaps',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 60,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 0,
                'operationDuration': 1305,
                'mappable': true
            },
            {
                'operationID': 61,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 1,
                'operationDuration': 945,
                'mappable': true
            },
            {
                'operationID': 62,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 2,
                'operationDuration': 390,
                'mappable': true
            },
            {
                'operationID': 63,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 3,
                'operationDuration': 90,
                'mappable': true
            },
            {
                'operationID': 64,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 4,
                'operationDuration': 1230,
                'mappable': true
            },
            {
                'operationID': 65,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 5,
                'operationDuration': 405,
                'mappable': true
            },
            {
                'operationID': 66,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 6,
                'operationDuration': 840,
                'mappable': true
            },
            {
                'operationID': 67,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 7,
                'operationDuration': 720,
                'mappable': true
            },
            {
                'operationID': 68,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 8,
                'operationDuration': 540,
                'mappable': true
            },
            {
                'operationID': 69,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 9,
                'operationDuration': 1425,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 7,
            'description': 'Rudder',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 70,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 0,
                'operationDuration': 540,
                'mappable': true
            },
            {
                'operationID': 71,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 1,
                'operationDuration': 225,
                'mappable': true
            },
            {
                'operationID': 72,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 2,
                'operationDuration': 615,
                'mappable': true
            },
            {
                'operationID': 73,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 3,
                'operationDuration': 1170,
                'mappable': true
            },
            {
                'operationID': 74,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 4,
                'operationDuration': 1140,
                'mappable': true
            },
            {
                'operationID': 75,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 5,
                'operationDuration': 1260,
                'mappable': true
            },
            {
                'operationID': 76,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 6,
                'operationDuration': 450,
                'mappable': true
            },
            {
                'operationID': 77,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 7,
                'operationDuration': 1140,
                'mappable': true
            },
            {
                'operationID': 78,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 8,
                'operationDuration': 540,
                'mappable': true
            },
            {
                'operationID': 79,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 9,
                'operationDuration': 120,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 8,
            'description': 'Tail',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 80,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 0,
                'operationDuration': 1320,
                'mappable': true
            },
            {
                'operationID': 81,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 1,
                'operationDuration': 1215,
                'mappable': true
            },
            {
                'operationID': 82,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 2,
                'operationDuration': 195,
                'mappable': true
            },
            {
                'operationID': 83,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 3,
                'operationDuration': 1230,
                'mappable': true
            },
            {
                'operationID': 84,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 4,
                'operationDuration': 810,
                'mappable': true
            },
            {
                'operationID': 85,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 5,
                'operationDuration': 195,
                'mappable': true
            },
            {
                'operationID': 86,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 6,
                'operationDuration': 435,
                'mappable': true
            },
            {
                'operationID': 87,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 7,
                'operationDuration': 600,
                'mappable': true
            },
            {
                'operationID': 88,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 8,
                'operationDuration': 1170,
                'mappable': true
            },
            {
                'operationID': 89,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 9,
                'operationDuration': 1125,
                'mappable': true
            }
            ]
        },
        {
            'FOID': 9,
            'description': 'Elevator',
            'sectionID': 1,
            'QPA': 1,
            quantity:1,
            'FOPriority': 1,
            'sectionPriority': 1,
            'operations': [{
                'operationID': 90,
                'operationCode': '9',
                'operationType': '9',
                'operationSequence': 0,
                'operationDuration': 1320,
                'mappable': true
            },
            {
                'operationID': 91,
                'operationCode': '4',
                'operationType': '4',
                'operationSequence': 1,
                'operationDuration': 810,
                'mappable': true
            },
            {
                'operationID': 92,
                'operationCode': '6',
                'operationType': '6',
                'operationSequence': 2,
                'operationDuration': 960,
                'mappable': true
            },
            {
                'operationID': 93,
                'operationCode': '7',
                'operationType': '7',
                'operationSequence': 3,
                'operationDuration': 480,
                'mappable': true
            },
            {
                'operationID': 94,
                'operationCode': '0',
                'operationType': '0',
                'operationSequence': 4,
                'operationDuration': 780,
                'mappable': true
            },
            {
                'operationID': 95,
                'operationCode': '2',
                'operationType': '2',
                'operationSequence': 5,
                'operationDuration': 90,
                'mappable': true
            },
            {
                'operationID': 96,
                'operationCode': '8',
                'operationType': '8',
                'operationSequence': 6,
                'operationDuration': 810,
                'mappable': true
            },
            {
                'operationID': 97,
                'operationCode': '5',
                'operationType': '5',
                'operationSequence': 7,
                'operationDuration': 1230,
                'mappable': true
            },
            {
                'operationID': 98,
                'operationCode': '3',
                'operationType': '3',
                'operationSequence': 8,
                'operationDuration': 90,
                'mappable': true
            },
            {
                'operationID': 99,
                'operationCode': '1',
                'operationType': '1',
                'operationSequence': 9,
                'operationDuration': 390,
                'mappable': true
            }
            ]
        }
    ];


    //machines list
    $scope.machines=[
        
        {
            "machineID": 0,
            "machineName": "0",
            "machineOperationCode": "0",
            "machineOperationType": "0",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-5T14:00:00.000Z",
                "end": "2019-1-5T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 1,
            "machineName": "1",
            "machineOperationCode": "1",
            "machineOperationType": "1",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-4T14:00:00.000Z",
                "end": "2019-1-4T18:00:00.000Z"
            },
            {
                "start": "2019-1-5T16:00:00.000Z",
                "end": "2019-1-5T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 2,
            "machineName": "2",
            "machineOperationCode": "2",
            "machineOperationType": "2",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-3T14:00:00.000Z",
                "end": "2019-1-3T18:00:00.000Z"
            },
            {
                "start": "2019-1-8T16:00:00.000Z",
                "end": "2019-1-8T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 3,
            "machineName": "3",
            "machineOperationCode": "3",
            "machineOperationType": "3",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-4T16:00:00.000Z",
                "end": "2019-1-4T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 4,
            "machineName": "4",
            "machineOperationCode": "4",
            "machineOperationType": "4",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-5T14:00:00.000Z",
                "end": "2019-1-5T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 5,
            "machineName": "5",
            "machineOperationCode": "5",
            "machineOperationType": "5",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-5T14:00:00.000Z",
                "end": "2019-1-5T18:00:00.000Z"
            }]
        },
        {
            "machineID": 6,
            "machineName": "6",
            "machineOperationCode": "6",
            "machineOperationType": "6",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-2T14:00:00.000Z",
                "end": "2019-1-2T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 7,
            "machineName": "7",
            "machineOperationCode": "7",
            "machineOperationType": "7",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-5T14:00:00.000Z",
                "end": "2019-1-5T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 8,
            "machineName": "8",
            "machineOperationCode": "8",
            "machineOperationType": "8",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-5T14:00:00.000Z",
                "end": "2019-1-5T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            },
            {
                "start": "2019-1-10T14:00:00.000Z",
                "end": "2019-1-10T18:00:00.000Z"
            }
            ]
        },
        {
            "machineID": 9,
            "machineName": "9",
            "machineOperationCode": "9",
            "machineOperationType": "9",
            "machineFreeTime": 0,
            "mappable": true,
            "maintenanceRecurrencePeriod": 6000,
            "maintenanceDuration": 300,
            "machineRunTime": 1200,
            "scheduledDowntime": [{
                "start": "2019-1-5T14:00:00.000Z",
                "end": "2019-1-5T18:00:00.000Z"
            },
            {
                "start": "2019-1-7T16:00:00.000Z",
                "end": "2019-1-7T20:00:00.000Z"
            },
            {
                "start": "2019-1-12T14:00:00.000Z",
                "end": "2019-1-12T18:00:00.000Z"
            }
            ]
        }
    ];

});
