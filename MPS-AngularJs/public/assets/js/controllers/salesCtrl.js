app.controller('salesCtrl', function ($scope) {
  console.log('hello from salesCtrl')
  $scope.msg = 'hello from sales'

  var monthlySales = 0;
  var todaySales = 0;

  // sales chart per month 
  $scope.chartSales = {
    chart: {
      caption: 'Amount Of Sales by Month',
      subcaption: 'Sales per Month',
      xaxisname: 'Month',
      yaxisname: 'Sales Per Month(PKR)',
      numbersuffix: '',
      theme: 'fusion',
      'plottooltext': 'No. of sales <br> in month $label are <b>$value</b>'
    },
    data: [
      {
        label: 'Jan',
        value: '500'
      },
      {
        label: 'Feb',
        value: '600'
      },
      {
        label: 'Mar',
        value: '990'
      },
      {
        label: 'Apr',
        value: '1200'
      },
      {
        label: 'May',
        value: '1170'
      },
      {
        label: 'Jun',
        value: '1500'
      },
      {
        label: 'Jul',
        value: '1500'
      },
      {
        label: 'Aug',
        value: '1500'
      },
      {
        label: 'Sep',
        value: '1500'
      },
      {
        label: 'Oct',
        value: '1100'
      },
      {
        label: 'Nov',
        value: '1500'
      },
      {
        label: 'Dec',
        value: '1270'
      }
    ]
  };


  // topSales chart per month 
  $scope.topSales = {
    chart: {
      caption: 'Top 5 Sales Of Month',
      subcaption: 'Top 5 Sales',
      xaxisname: 'Products',
      yaxisname: 'Sales Per Month(PKR)',
      numbersuffix: '',
      theme: 'fusion',
      'plottooltext': 'No. of sales <br> of $label are <b>$value</b>'
    },
    data: [
      {
        label: 'Jan',
        value: '500'
      },
      {
        label: 'Feb',
        value: '600'
      },
      {
        label: 'Mar',
        value: '990'
      },
      {
        label: 'Apr',
        value: '1200'
      },
      {
        label: 'May',
        value: '1170'
      }
    ]
  };

  // radar top 5 sales
  $scope.radarTopSales = {
    chart: {
      caption: 'Sales of Top 5 Products',
      subcaption: 'Sales of products',
      theme: 'fusion',
      showlegend: '0',
      showdivlinevalues: '0',
      showlimits: '0',
      showvalues: '1',
      plotfillalpha: '40',
      plottooltext: "Sales of product <b>$label</b> <br>are <b>$value</b>"
    },
    categories: [
      {
        category: [
          {
            label: 'Communication'
          },
          {
            label: 'Punctuality'
          },
          {
            label: 'Meeting Deadlines'
          },
          {
            label: 'Team Player'
          },
          {
            label: 'Technical Knowledge'
          }
        ]
      }
    ],
    dataset: [
      {
        seriesname: 'Sales',
        data: [
          {
            value: '3'
          },
          {
            value: '4'
          },
          {
            value: '3'
          },
          {
            value: '2'
          },
          {
            value: '4'
          }
        ]
      }
    ]
  };

  //donut of topSales  
$scope.topSalesDonut = {
    chart: {
        doughnutRadius:"70",
        pieRadius:"100",
        caption: "Sales of Products",
        subcaption: "Sales",
        showpercentvalues: "1",
        defaultcenterlabel: "Products",
        aligncaptionwithcanvas: "0",
        captionpadding: "0",
        decimals: "1",
        plottooltext:
        "<b>$percentValue</b> of our sales <br>are <b>$label</b>",
        centerlabel: "Total $label : $value",
        theme: "fusion"
    },
    data: [
        {
        label: "A",
        value: "100",
        color:"#123456"
        },
        {
        label: "B",
        value: "80"
        },
        {
        label: "C",
        value: "40"
        },
        {
        label: "D",
        value: "40"
        },
        {
        label: "EE",
        value: "40"
        },
        {
        label: "FD",
        value: "40"
        },
        {
        label: "G",
        value: "40"
        },
        {
        label: "H",
        value: "40"
        },
        {
        label: "I",
        value: "40"
        },
        {
        label: "J",
        value: "40"
        },
        {
        label: "K",
        value: "40"
        },
        {
        label: "L",
        value: "40"
        },
        {
        label: "M",
        value: "40"
        },
        {
        label: "N",
        value: "40"
        }
    ]
};


});
