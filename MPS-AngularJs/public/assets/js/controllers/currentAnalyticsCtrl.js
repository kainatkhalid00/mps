app.controller('currentAnalyticsCtrl',function($scope,$interval,socket,$http){
    console.log('hello from currentAnalyticsCtrl');
    //variables


    // socket.connect();
    // var socket=io.client();

    var samples=0;
    var currentActualValue=0;
    var currentPredictedValue=0;
    var temperatureActualValue=0;
    var temperaturePredictedValue=0;
    var humidityActualValue=0;
    var humidityPredictedValue=0;
    //for current analytics graph
    var chart={
        caption: "Current Dissipation",
        yaxisname: "Amperes(A)",
        xaxisname: "time(ms)",
        numbersuffix: "A",
        subcaption: "(Actual Vs Predicted)",
        yaxismaxvalue: "2",
        plottooltext:"$seriesName is <b>$dataValue</b> $label",
        theme: "fusion"
    };
    var categories=[
        {
        category: [
            {
            label: ""+getSampleNumber()
            }
            ,
            {
            label: ""+getSampleNumber()
            }
        ]
        }
    ];
    var dataset=[
        {
        seriesname: "Actual",
        data: [
            {
            value: ""+currentActualValue
            }

          ]
        }
        ,
        {
        seriesname: "Predicted",
        data: [
            {
            value: ""+currentPredictedValue
            }
            // ,
            // {
            // value: "0.74"
            // }
        ]
        }
    ];
    $scope.myDataSource = {
        chart: chart,
        categories: categories,
        dataset: dataset
    };

    //for temperature graph
    var chartTemprature={
        caption: "Temperature Analytics",
        yaxisname: "Celcius(`C)",
        xaxisname: "time(ms)",
        numbersuffix: "`C",
        // subcaption: "(Actual Vs Predicted)",
        yaxismaxvalue: "2",
        plottooltext:"$seriesName is <b>$dataValue</b> $label",
        theme: "fusion"
    };
    var categoriesTemprature=[
        {
        category: [
            {
            label: ""+getSampleNumber()
            }
            // ,
            // {
            // label: ""+getSampleNumber()
            // }
        ]
        }
    ];
    var datasetTemprature=[
        {
        seriesname: "Actual",
        data: [
            {
            value: "30"
            }
            // ,
            // {
            // value: "30"
            // }

        ]
        }
        // ,
        // {
        // seriesname: "Predicted",
        // data: [
        //     {
        //     value: "30"
        //     },
        //     {
        //     value: "31"
        //     }
        // ]
        // }
    ];
    $scope.myDataSourceTemprature = {
        chart: chartTemprature,
        categories: categoriesTemprature,
        dataset: datasetTemprature
    };
    
    //for Humidity graph
    var chartHumidity={
        caption: "Humidity Analytics",
        yaxisname: "Humidity(%)",
        xaxisname: "time(ms)",
        numbersuffix: "%",
        // subcaption: "(Actual Vs Predicted)",
        yaxismaxvalue: "2",
        plottooltext:"$seriesName is <b>$dataValue</b> $label",
        theme: "fusion"
    };
    var categoriesHumidity=[
        {
        category: [
            {
            label: ""+getSampleNumber()
            },
            {
            label: ""+getSampleNumber()
            }
        ]
        }
    ];
    var datasetHumidity=[
        {
        seriesname: "Actual",
        data: [
            {
            value: "68"
            },
            {
            value: "69"
            }

        ]
        }
        // ,
        // {
        // seriesname: "Predicted",
        // data: [
        //     {
        //     value: "70"
        //     },
        //     {
        //     value: "69"
        //     }
        // ]
        // }
    ];
    $scope.myDataSourceHumidity = {
        chart: chartHumidity,
        categories: categoriesHumidity,
        dataset: datasetHumidity
    };

    //guage for current
    $scope.guageCurrent = {
        chart: {
          caption: "Current",
          captionontop: "1",
          origw: "380",
          origh: "320",
          gaugestartangle: "135",
          gaugeendangle: "45",
          gaugeoriginx: "190",
          gaugeoriginy: "250",
          gaugeouterradius: "190",
          theme: "fusion",
          showvalue: "1",
          numbersuffix: " A",
          valuefontsize: "25"
        },
        colorrange: {
          color: [
            {
              minvalue: "0",
              maxvalue: "10",
              code: "#62B58F"
            },
            {
              minvalue: "10",
              maxvalue: "20",
              code: "#FFC533"
            },
            {
              minvalue: "20",
              maxvalue: "30",
              code: "#F2726F"
            }
          ]
        },
        dials: {
          dial: [
            {
              value: ""+currentActualValue,
              tooltext: "Actual Current",
              color:"#000000"
            }
            ,
            {
              value: ""+currentPredictedValue,
              tooltext: "Predicted Current",
              color:"#F2726F"
            }
          ]
        }
    };
    
    //guage for temperature
    $scope.guageTemprature = {
        chart: {
          caption: "Temperature",
          captionontop: "1",
          origw: "380",
          origh: "320",
          gaugestartangle: "135",
          gaugeendangle: "45",
          gaugeoriginx: "190",
          gaugeoriginy: "250",
          gaugeouterradius: "190",
          theme: "fusion",
          showvalue: "1",
          numbersuffix: " `C",
          valuefontsize: "25"
        },
        colorrange: {
          color: [
            {
              minvalue: "0",
              maxvalue: "25",
              code: "#62B58F"
            },
            {
              minvalue: "25",
              maxvalue: "50",
              code: "#FFC533"
            },
            {
              minvalue: "50",
              maxvalue: "75",
              code: "#F2726F"
            },
            {
              minvalue: "75",
              maxvalue: "100",
              code: "#F2C26F"
            }
          ]
        },
        dials: {
          dial: [
            {
              value: ""+0,
              tooltext: "Actual Temperature"
            }
            // ,
            // {
            //   value: ""+0,
            //   tooltext: "Predicted Temperature"
            // }
          ]
        }
    };
    
    //guage for Humidity
    $scope.guageHumidity = {
        chart: {
          caption: "Humidity",
          captionontop: "1",
          origw: "380",
          origh: "320",
          gaugestartangle: "135",
          gaugeendangle: "45",
          gaugeoriginx: "190",
          gaugeoriginy: "250",
          gaugeouterradius: "190",
          theme: "fusion",
          showvalue: "1",
          numbersuffix: " %",
          valuefontsize: "25"
        },
        colorrange: {
          color: [
            {
              minvalue: "0",
              maxvalue: "20",
              code: "#62B58F"
            },
            {
              minvalue: "20",
              maxvalue: "40",
              code: "#FFC533"
            },
            {
              minvalue: "40",
              maxvalue: "60",
              code: "#F2A26F"
            },
            {
              minvalue: "60",
              maxvalue: "80",
              code: "#F27A6F"
            },
            {
              minvalue: "80",
              maxvalue: "100",
              code: "#F27FFF"
            }
          ]
        },
        dials: {
          dial: [
            {
              value: ""+0,
              tooltext: "Actual Humidity"
            }
            // ,
            // {
            //   value: ""+0,
            //   tooltext: "Predicted Humidity"
            // }
          ]
        }
    };

    function getSampleNumber(){
        samples+=200;
        return samples;
    }

    $scope.cData=0;

    // socket.on("connection",function(data){
    //   console.log('socket connected...');
    //   socket.on("currentDataFromServer",function(data){
    //     console.log(data);
    //     $scope.cData=data;
    //   });
    // });



    $interval(function(){
        var i=getSampleNumber();
        //current
        // currentActualValue=Math.random()+3;
        $http.post('/currentData',{index:i/200}).then(function(resp){
          // console.log(resp.data);
          var temp=resp.data;
          currentActualValue=temp.currentData[temp.currentInstanceCounter];//here $scope.cData
          
          currentPredictedValue=temp.predictedVal;
        });
        // currentPredictedValue=Math.random()+3;

        $scope.guageCurrent.dials.dial[0].value=currentActualValue;
        $scope.guageCurrent.dials.dial[1].value=currentPredictedValue;

        categories[0].category.push({'label':""+i});
        dataset[0].data.push({'value':currentActualValue});
        dataset[1].data.push({'value':currentPredictedValue});
        //temperature
        temperatureActualValue=Math.random()+30;
        // temperaturePredictedValue=Math.random()+30;

        $scope.guageTemprature.dials.dial[0].value=temperatureActualValue;
        // $scope.guageTemprature.dials.dial[1].value=temperaturePredictedValue;

        categoriesTemprature[0].category.push({'label':""+i});
        datasetTemprature[0].data.push({'value':temperatureActualValue});
        // datasetTemprature[1].data.push({'value':temperaturePredictedValue});
        //humidity
        humidityActualValue=Math.random()+60;
        humidityPredictedValue=Math.random()+60;

        $scope.guageHumidity.dials.dial[0].value=humidityActualValue;
        // $scope.guageHumidity.dials.dial[1].value=humidityPredictedValue;

        categoriesHumidity[0].category.push({'label':""+i});
        datasetHumidity[0].data.push({'value':humidityPredictedValue});
        // datasetHumidity[1].data.push({'value':humidityPredictedValue});

        if(categories[0].category.length>10){
            //current
            categories[0].category.shift();
            dataset[0].data.shift();
            // dataset[1].data.shift();
            //temperature
            categoriesTemprature[0].category.shift();
            datasetTemprature[0].data.shift();
            // datasetTemprature[1].data.shift();
            //humidity
            categoriesHumidity[0].category.shift();
            datasetHumidity[0].data.shift();
            // datasetHumidity[1].data.shift();

        }


    },200);

});