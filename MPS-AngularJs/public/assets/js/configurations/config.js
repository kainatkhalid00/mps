app.config(['$routeProvider', '$locationProvider',
function($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
    .when('/', {
        templateUrl: '../../views/home.html',
        controller: 'homeCtrl'
    })
    .when('/home', {
        templateUrl: '../../views/home.html',
        controller: 'homeCtrl'
    })
    .when('/currentAnalytics', {
        templateUrl: '../../views/currentAnalytics.html',
        controller: 'currentAnalyticsCtrl'
    }).when('/bom', {
        templateUrl: '../../views/bom.html',
        controller: 'bomCtrl'
    }).when('/hr', {
        templateUrl: '../../views/hr.html',
        controller: 'hrCtrl'
    }).when('/inventory', {
        templateUrl: '../../views/inventory.html',
        controller: 'inventoryCtrl'
    }).when('/newOrders', {
        templateUrl: '../../views/newOrders.html',
        controller: 'newOrderCtrl'
    }).when('/reports', {
        templateUrl: '../../views/reports.html',
        controller: 'reportsCtrl'
    }).when('/sales', {
        templateUrl:'../../views/sales.html',
        controller: 'salesCtrl'
    }).when('/profiling', {
        templateUrl:'../../views/profiling.html',
        controller: 'profilingCtrl'
    }).when('/mps', {
        templateUrl:'../../views/MPS/mps.html',
        controller: 'mpsCtrl'
    }).when('/factory', {
        templateUrl:'../../views/factory/factoryMain.html',
        controller: 'factoryCtrl'
    }).when('/machines', {
        templateUrl:'../../views/factory/machines.html',
        controller: 'machinesCtrl'
    }).when('/jobs', {
        templateUrl:'../../views/factory/jobs.html',
        controller: 'jobsCtrl'
    }).when('/worker', {
        templateUrl:'../../views/factory/worker.html',
        controller: 'workerCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });
}
]);