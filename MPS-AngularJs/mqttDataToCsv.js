var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost:1884');

process.setMaxListeners(0);

var fs=require('fs');
var csv = require('csv-write-stream');
var writer =csv();
writer = csv({sendHeaders: false});

var samples=0;
var appliance='b1b2h';
var data=[];


client.on('connect', function () {
  client.subscribe('nodeData', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt')
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
  //   console.log(message.toString())
  var i = message.toString().indexOf(',');
  if (i === null || i === undefined || i === -1) {
    return;
  }
  var j = message.toString().indexOf(',', i + 1);
  if (j === null || j === undefined || j === -1) {
    return;
  }
  //   console.log(i,j)
  var current = Number.parseFloat(message.toString().slice(i + 1, j));
  console.log('samples = ',samples++,'current = ', current);

  data.push(current);

  if(samples==20){
      client.end();
    //   console.log('gonna write to json');
    data.forEach(function(element) {
        writer.pipe(fs.createWriteStream('electricityData.csv', {flags: 'a'}));
        writer.write({Irms: element, label: appliance});    
    }, this);
    writer.end();
  }

});

