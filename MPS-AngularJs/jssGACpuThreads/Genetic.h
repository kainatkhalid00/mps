#pragma once
#include <vector>
#include <chrono>
#include <algorithm>
#include <random>
#include <iostream>
#include "classes.h"
#include <map>
#include <unordered_map>
#include <cmath>
#include <thread>
#include <mutex>


//#include <map>
#include "ParseJSON.h"
#include "HelperFunctions.h"

using namespace std;
//#include "Genetic.h"

const string FILENAME = "input.json";

vector<Job> jobs;
vector<Machine> machines;
unordered_map<string, vector<int>> machinesMap;
ofstream outfile;
vector<int> timeTaken;

mutex m;

//int **tabulist;

void generate(vector<Job>&jobs, vector<vector<int>>&parents,int id) {
	vector<int> chromosome;

	for (auto job : jobs) {

		vector<int> vec(job.operations.size(), job.jobCode);
		chromosome.insert(chromosome.end(), vec.begin(), vec.end());
	}

	parents.at(id) = chromosome;
}

void generateChromosome(vector<Job> jobs, vector<vector<int>>&parents,int id) {
	//using milli = std::chrono::milliseconds;
	//auto start = std::chrono::high_resolution_clock::now();

	vector<int> chromosome={
		0, 1, 3, 6, 7, 8, 9, 1, 0, 5, 4, 7, 9, 1, 3, 0, 7, 8, 3, 7, 9, 0, 5, 1, 4, 8, 2, 7, 8, 9, 3, 5, 0, 0, 9, 6, 4, 2, 1, 9, 7, 3, 5, 6, 2, 6, 6, 8, 2, 9, 4,
		3, 8, 7, 9, 8, 2, 3, 3, 6, 4, 5, 0, 8, 1, 6, 2, 8, 3, 5, 7, 4, 0, 3, 1, 9, 2, 4, 9, 6, 4, 8, 5, 1, 2, 0, 6, 4, 6, 2, 7, 1, 4, 0, 5, 7, 2, 5, 5, 1
	};

	// for (auto job : jobs) {

	// 	vector<int> vec(job.operations.size(), job.jobCode);
	// 	chromosome.insert(chromosome.end(), vec.begin(), vec.end());
	// }

	/*vector<int> chromosome;

	for (auto job : jobs) {

		vector<int> vec(job.operations.size(), job.jobCode);
		chromosome.insert(chromosome.end(), vec.begin(), vec.end());
	}*/

	//shuffle(chromosome.begin(), chromosome.end(), default_random_engine(random_device()()));
	// for (auto i : chromosome) {
	// 	cout << i << " ";
	// }
	// cout << endl;

	// auto finish = std::chrono::high_resolution_clock::now();
	// cout << "generateChromosome() took "
	// 	<< std::chrono::duration_cast<milli>(finish - start).count()
	// 	<< " milliseconds\n";

	parents.at(id) = chromosome;
	//return chromosome;

}

void mutate(vector<int>& chromosome) {

	vector<int> beforeSwap = chromosome;
	// using milli = std::chrono::milliseconds;
	// auto start = std::chrono::high_resolution_clock::now();
	std::random_device rd;
	std::mt19937 eng(rd());
	std::uniform_int_distribution<> distr(0, chromosome.size() - 1);
	// cout << distr(eng) << ',' << distr(eng) << endl;
	swap(chromosome[distr(eng)], chromosome[distr(eng)]);
	//vector<int> afterSwap = chromosome;

	// if(beforeSwap==afterSwap) {
	// 	cout << "Same" << endl;
	//
	// }
	// else {
	// 	cout << "Diff" << endl;
	// }
	// cout << beforeSwap == afterSwap << endl;
	// auto finish = std::chrono::high_resolution_clock::now();
	//
	// std::cout << __func__ << " took "
	// 	<< std::chrono::duration_cast<milli>(finish - start).count()
	// 	<< " milliseconds\n";

}

//vector<int> 
void onePointCrossover(vector<int> &parent1, vector<int> parent2,vector<int>&child) {

	//auto start = std::chrono::high_resolution_clock::now();
	child.clear();

	//vector<int> offspring;
	/*std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, parent1.size() - 1);*/

	//int splittingPoint = dis(gen);
	int splittingPoint = int(parent1.size()/2);
	//cout << "SplittingPoint:" << splittingPoint << endl;
	//vector<int> substring1 = vector<int>(parent1.begin(), parent1.begin() + splittingPoint);
	child.insert(child.begin(), parent1.begin(), parent1.begin() + splittingPoint);

	//offspring = substring1;
	//child = substring1;

	//for (auto gene : offspring) {
	for (auto gene : child) {
		parent2.erase(find(parent2.begin(), parent2.end(), gene));
	}
	for (auto gene : parent2) {
		//offspring.push_back(gene);
		child.push_back(gene);
	}
	// printVector(offspring);
	//auto finish = std::chrono::high_resolution_clock::now();

	// std::cout << __func__ << " took "
	// 	<< std::chrono::duration_cast<chrono::microseconds>(finish - start).count()
	// 	<< " microseconds\n";

	// timeTaken.push_back(std::chrono::duration_cast<chrono::microseconds>(finish - start).count());
	//return offspring;
	//child = offspring;
}


void getMakespan(vector<int>& chromosome, vector<Job> jobs, vector<Machine> machines,vector<int>&makespanVector,int id) {


	// write_container(chromosome, outfile);

	//using milli = std::chrono::milliseconds;
	//auto start = std::chrono::high_resolution_clock::now();

	/*vector<OrderedOperation> orderedOperations;
	orderedOperations.reserve(chromosome.size());
	int jobSize = jobs.size();
	vector<int> operationOfJobCount(jobSize, -1);
	for (auto gene : chromosome) {
		operationOfJobCount[gene] += 1;
		orderedOperations.push_back(OrderedOperation(gene, operationOfJobCount[gene], 
		jobs[gene].operations[operationOfJobCount[gene]].operationCode,
			jobs[gene].operations[operationOfJobCount[gene]].operationDuration));

	}*/

	//for (auto orderedOperation : orderedOperations) {

	int jobSize = jobs.size();
	vector<int> operationOfJobCount(jobSize,-1);
	for (int gene = 0; gene < int(chromosome.size());gene++) {
		/*int jobIdx = orderedOperation.job;
		int opIdx = orderedOperation.operation;
		string opCode = orderedOperation.opCode;
		int duration = orderedOperation.duration;*/

		int jobIdx =chromosome[gene];
		operationOfJobCount[jobIdx]++;
		int opIdx = operationOfJobCount[jobIdx];
		//operationOfJobCount[jobIdx]++;
		string opCode = jobs[jobIdx].operations[opIdx].operationCode;
		int duration = jobs[jobIdx].operations[opIdx].operationDuration;


		Operation& currentOperation = jobs[jobIdx].operations[opIdx];


		int prevOpIdx=0;
		int prevOpEndTime=0;
		//vector<int> eligibleMachineIndexes = getMachinesForOperation(opCode);

		if (opIdx != 0) {

			prevOpIdx = opIdx - 1;
			prevOpEndTime = jobs[jobIdx].operations[prevOpIdx].operationEndTime;
		}
		else {
			prevOpEndTime = 0;
		}

		vector<int> machinesForCurrentOrderedOp = machinesMap.at(opCode);


		if (currentOperation.mappable) {

			//compute best timeslot
			vector<TimeSlotIndex> timeSlots;

			for (auto machineIdx : machinesForCurrentOrderedOp) {
				TimeSlotIndex timeSlotIndex = machines[machineIdx].getSuitableTimeSlot(opIdx, prevOpEndTime, duration);
				timeSlots.push_back(timeSlotIndex);
			}


			TimeSlotIndex bestTimeSlot = *min_element(timeSlots.begin(), timeSlots.end(),
				[](const TimeSlotIndex& x, const TimeSlotIndex& y) {
				return x.operationStartTime < y.operationStartTime;
			});


			//map operation to machine
			Machine& eligibleMachine = machines[bestTimeSlot.machineIdx];
			eligibleMachine.scheduleOperation(jobs, bestTimeSlot, currentOperation, jobIdx);

		}

		else {
			Machine& eligibleMachine = machines[machinesForCurrentOrderedOp[0]];

			if (opIdx == 0) {
				eligibleMachine.scheduleOperation(jobs, 0, duration, currentOperation, jobIdx);
			}
			else {
				eligibleMachine.scheduleOperation(jobs, prevOpEndTime, duration, currentOperation, jobIdx);
			}
		}
	}


	vector<int> makespan;
	for (auto job : jobs) {
		//for (auto operation : job.operations)
		makespan.push_back(job.operations.back().operationEndTime);
	}

	//auto finish = std::chrono::high_resolution_clock::now();

	//std::cout << __func__ << " took "
	//	<< std::chrono::duration_cast<chrono::microseconds>(finish - start).count()
	//	<< " microseconds\n";

	//timeTaken.push_back(std::chrono::duration_cast<chrono::microseconds>(finish - start).count());

	// timeTaken.push_back(std::chrono::duration_cast<chrono::microseconds>(finish - start).count());
	// std::cout << __func__ << " took "
	// 	<< std::chrono::duration_cast<chrono::milliseconds>(finish - start).count()
	// 	<< " milliseconds\n";

	// writeCSVsortedByOperation(jobs, machines);
	// writeCSVsortedByMachine(jobs, machines);

	makespanVector.at(id) = *max_element(makespan.begin(), makespan.end());
	//cout << makespanVector.at(id) << endl;
	//cout <<"crossing tid = "  << id << endl;
	//cout << *max_element(makespan.begin(), makespan.end()) << endl;
	//return *max_element(makespan.begin(), makespan.end());

	//return max(makespan.begin(), makespan.end());

}


vector<int> getMachinesForOperation(string operationCode) {

	return machinesMap.at(operationCode);


}

// void loadData() {
//
// 	loadJobsData(jobs,)
// }

void loadData() {
	loadJSON(FILENAME);
	loadJobsData(jobs);
	loadMachinesData(machines);
	machinesMap = mapMachinesToOperations(machines);

}

int generateRandomNumber(int start, int end) {

	random_device rd;
	mt19937 eng(rd());
	uniform_int_distribution<> distr(start, end);

	return distr(eng);

}


vector<int> shuffler(vector<int> &seed) {
	std::random_shuffle(seed.begin(), seed.end());
	return seed;

}


vector<int> swapper(vector<int> &seed, int tabuTenure, int **tabuList/*,int maxIterations*/) {

	//	int tabulist[int(jobs.size())][int(jobs.size())];
	//	tabuList={0};

	//	for(int i=0;i<maxIterations;i++){

	int a = generateRandomNumber(0, int(seed.size()) - 1);
	int b = generateRandomNumber(0, int(seed.size()) - 1);

	while (a == b) {
		b = generateRandomNumber(0, int(seed.size()) - 1);
	}

	if (tabuList[seed.at(a)][seed.at(b)] == 0 || tabuList[seed.at(b)][seed.at(a)] == 0) {
		tabuList[seed.at(a)][seed.at(b)] = tabuTenure;
		tabuList[seed.at(b)][seed.at(a)] = tabuTenure;

		int tempA = seed.at(a);
		int tempB = seed.at(b);

		seed.at(a) = tempB;
		seed.at(b) = tempA;

	}
	else {

		for (int i = 0; i<int(jobs.size()); i++) {
			for (int j = 0; j<int(jobs.size()); j++) {
				if ((i == a && j == b) || (j == a && i == b)) {
					//for avoiding current element added to Tabu List
					continue;
				}
				else if (tabuList[i][j] != 0) {
					tabuList[seed.at(i)][seed.at(j)]--;
					tabuList[seed.at(j)][seed.at(i)]--;
				}
			}
		}

	}
	//	}
	return seed;
}


/*void runGeneticAlgo(int popSize,int maxGenerations) {

	const int nJ = int(jobs.size());

	//	int currentA=0,currentB=0;

	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	clock_t c_start, c_end;
	c_start = clock();

	// outfile.open("asad.txt");


//	vector<vector<int>> parentPop;
//	parentPop.reserve(popSize);

//
//	for (auto i = 0; i < popSize; i++) {
//		parentPop.push_back(generateChromosome(jobs));
//	}

	vector<int> ch = generateChromosome(jobs);
	//	vector<int> ch = {

	//			17, 37, 31, 33, 29, 15, 40, 19, 12, 10, 31, 7, 1, 34, 7, 10, 26, 40, 22, 18, 2, 37, 25, 16, 31, 22, 29, 5, 14, 7, 37, 14, 22, 21, 30, 1, 6, 0, 36, 25,
	//			7, 37, 20, 23, 16, 3, 1, 10, 18, 14, 16, 29, 2, 39, 15, 25, 9, 25, 0, 41, 13, 8, 18, 40, 21, 0, 7, 2, 0, 18, 25, 35, 41, 31, 3, 4, 31, 33, 26, 8, 12,
	//			38, 13, 13, 25, 23, 23, 1, 0, 8, 35, 24, 9, 20, 5, 5, 41, 36, 31, 28, 5, 6, 32, 2, 33, 23, 19, 27, 14, 36, 41, 13, 21, 17, 6, 26, 36, 12, 17, 2, 28, 19,
	//			7, 32, 7, 24, 25, 32, 39, 26, 2, 34, 10, 36, 32, 39, 35, 27, 35, 2, 12, 5, 11, 11, 41, 20, 24, 40, 35, 18, 16, 9, 11, 18, 2, 34, 22, 31, 32, 16, 22, 6,
	//			10, 36, 8, 27, 19, 41, 34, 5, 27, 9, 14, 19, 37, 34, 36, 38, 31, 27, 24, 14, 21, 40, 0, 0, 4, 39, 14, 28, 13, 1, 7, 11, 6, 38, 18, 30, 41, 3, 34, 8, 23,
	//			39, 23, 2, 41, 3, 19, 31, 20, 39, 11, 15, 39, 27, 31, 17, 35, 9, 37, 32, 13, 40, 35, 32, 9, 30, 17, 35, 38, 26, 4, 39, 24, 14, 31, 20, 24, 36, 25, 40,
	//			8, 38, 3, 3, 41, 20, 20, 17, 27, 4, 33, 41, 40, 17, 40, 2, 7, 16, 3, 10, 38, 6, 12, 19, 40, 38, 4, 29, 41, 3, 26, 14, 16, 36, 37, 7, 37, 2, 6, 9, 16,
	//			28, 34, 40, 1, 0, 15, 39, 26, 9, 7, 15, 21, 1, 0, 25, 20, 11, 17, 6, 10, 20, 12, 30, 22, 12, 16, 5, 39, 23, 1, 35, 21, 33, 35, 4, 24, 1, 23, 39, 21, 6,
	//			12, 10, 31, 10, 6, 4, 19, 10, 3, 12, 0, 15, 33, 3, 17, 10, 14, 27, 23, 4, 37, 8, 13, 38, 18, 12, 22, 10, 19, 8, 26, 40, 12, 21, 16, 36, 23, 6, 39, 26,
	//			14, 40, 20, 36, 8, 0, 3, 32, 23, 20, 11, 16, 15, 39, 20, 25, 15, 29, 22, 29, 15, 19, 9, 32, 35, 7, 2, 32, 26, 27, 12, 41, 22, 5, 1, 18, 9, 13, 29, 11,
	//			11, 28, 37, 9, 18, 13, 18, 10, 38, 22, 36, 41, 23, 6, 22, 41, 27, 24, 24, 1, 30, 28, 6, 3, 32, 19, 6, 8, 22, 26, 0, 30, 41, 37, 9, 7, 13, 25, 28, 25,
	//			13, 10, 26, 37, 4, 5, 8, 8, 41, 34, 20, 17, 33, 39, 3, 4, 13, 40, 4, 33, 27, 35, 23, 17, 27, 32, 15, 40, 17, 14, 20, 29, 31, 30, 9, 30, 38, 0, 30, 9,
	//			35, 1, 16, 32, 30, 29, 19, 40, 27, 14, 26, 37, 31, 32, 32, 0, 36, 32, 11, 37, 10, 31, 17, 38, 41, 14, 20, 5, 28, 9, 7, 26, 24, 26, 34, 6, 32, 27, 29,
	//			25, 24, 30, 38, 39, 29, 26, 36, 30, 11, 6, 38, 22, 35, 7, 28, 37, 18, 0, 34, 21, 14, 25, 19, 6, 4, 36, 22, 32, 17, 34, 3, 33, 33, 17, 20, 41, 21, 34,
	//			21, 24, 41, 17, 13, 25, 11, 0, 12, 23, 11, 30, 19, 28, 2, 35, 21, 21, 6, 1, 25, 22, 1, 8, 16, 38, 23, 33, 18, 28, 4, 3, 15, 18, 16, 36, 25, 14, 41, 30,
	//			9, 16, 37, 20, 14, 5, 16, 34, 13, 12, 12, 21, 7, 16, 12, 20, 15, 5, 26, 16, 25, 19, 29, 40, 12, 35, 27, 24, 11, 23, 30, 16, 2, 40, 41, 35, 39, 20, 39,
	//			30, 41, 27, 33, 10, 21, 31, 35, 5, 5, 27, 34, 8, 23, 8, 24, 40, 27, 3, 13, 25, 38, 24, 21, 37, 7, 22, 37, 24, 36, 34, 30, 0, 33, 1, 19, 20, 5, 8, 10, 4,
	//			36, 5, 31, 12, 32, 9, 36, 15, 27, 13, 2, 7, 28, 41, 29, 27, 24, 10, 1, 21, 0, 33, 21, 28, 38, 25, 4, 36, 19, 37, 18, 40, 16, 33, 17, 12, 28, 17, 18, 17,
	//			19, 32, 17, 31, 5, 4, 5, 5, 35, 8, 21, 15, 2, 4, 12, 24, 33, 28, 0, 5, 28, 6, 18, 34, 5, 3, 30, 33, 13, 37, 3, 22, 4, 27, 2, 39, 37, 14, 36, 8, 20, 0,
	//			39, 40, 39, 36, 32, 38, 19, 12, 29, 26, 35, 36, 30, 19, 25, 5, 13, 25, 8, 24, 9, 28, 13, 15, 7, 28, 41, 17, 32, 14, 9, 25, 15, 33, 23, 5, 2, 19, 26, 31,
	//			21, 29, 37, 32, 35, 34, 6, 36, 13, 18, 40, 35, 41, 19, 34, 35, 11, 26, 38, 14, 33, 24, 27, 22, 36, 36, 41, 9, 33, 16, 41, 18, 20, 30, 11, 13, 26, 16,
	//			39, 7, 27, 23, 38, 3, 9, 34, 11, 5, 36, 27, 41, 23, 34, 31, 30, 1, 10, 35, 35, 37, 22, 29, 4, 7, 5, 29, 35, 13, 29, 37, 15, 25, 36, 32, 38, 35, 41, 17,
	//			28, 14, 29, 20, 15, 39, 12, 22, 11, 29, 0, 28, 12, 26, 10, 10, 38, 15, 15, 11, 17, 23, 21, 34, 30, 6, 4, 18, 31, 33, 31, 3, 6, 11, 8, 29, 3, 28, 8, 1,
	//			22, 0, 20, 39, 4, 20, 35, 8, 11, 28, 1, 2, 5, 10, 5, 7, 3, 27, 1, 40, 9, 11, 38, 28, 26, 15, 18, 2, 30, 29
	//};
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	vector<int> bestCh={
	//			12,41,5,13,24,5,39,33,14,18,37,33,23,19,1,26,19,17,32,22,22,39,39,17,2,40,34,27,7,32,39,16,22,30,35,32,
	//			13,3,7,25,9,18,32,0,38,30,34,0,3,17,19,0,37,41,33,0,22,13,28,40,30,36,27,23,26,35,14,27,23,8,22,25,41,29,11,4,33,
	//			14,15,35,7,29,0,31,21,5,27,0,28,40,26,5,13,28,12,20,4,31,30,35,20,16,29,0,26,38,37,6,16,21,31,38,36,31,41,27,32,25,
	//			17,36,16,21,9,1,29,34,27,13,15,2,30,18,23,29,26,9,21,18,20,21,29,26,35,10,39,17,23,32,36,1,24,7,19,3,35,29,35,25,
	//			32,39,41,27,40,5,14,8,20,25,23,35,34,27,17,35,5,31,37,21,27,40,21,2,10,30,9,35,36,1,26,3,38,25,5,11,7,36,11,22,22,
	//			37,18,12,5,1,5,12,35,6,37,6,37,29,38,36,6,41,38,2,16,33,16,36,18,11,20,9,22,10,33,27,3,29,22,4,16,9,40,28,20,14,
	//			32,34,8,17,20,14,12,17,7,20,36,16,18,18,10,18,14,9,16,16,16,0,10,30,39,12,17,16,28,12,13,41,11,4,40,12,31,1,15,
	//			34,28,0,26,37,40,31,28,0,3,15,22,7,41,35,35,39,21,9,33,38,15,35,3,30,15,24,33,3,11,34,37,38,11,23,37,17,17,37,24,
	//			38,38,38,41,39,19,34,41,31,22,31,22,8,19,36,37,32,6,9,5,28,29,4,39,11,15,22,32,28,29,35,28,15,30,28,41,31,31,25,
	//			17,31,36,1,6,36,11,16,9,24,41,24,18,8,31,28,17,8,21,30,5,16,40,41,6,38,13,16,5,33,18,28,29,26,11,8,35,32,16,6,8,
	//			40,31,5,1,32,5,4,2,4,16,16,12,4,37,9,34,17,15,24,5,32,7,4,34,10,24,27,24,7,8,26,32,7,7,35,20,20,14,10,25,25,13,39,
	//			19,21,22,24,19,23,21,22,1,0,21,12,1,1,8,40,38,10,13,21,2,1,8,24,40,8,39,25,36,30,40,33,13,6,6,30,24,11,6,11,20,5,
	//			6,20,12,14,14,24,10,25,5,37,4,4,40,11,40,12,40,2,26,19,26,39,19,20,20,4,12,1,0,2,27,13,2,23,28,2,19,36,36,36,25,
	//			2,39,7,34,34,27,23,17,36,21,22,18,2,3,2,12,19,7,8,41,25,26,26,13,28,7,27,34,28,17,13,8,17,30,7,13,13,23,3,3,41,13,
	//			41,3,40,8,14,14,2,23,3,1,14,35,22,36,8,33,26,23,15,15,15,32,6,29,33,0,39,33,38,19,14,0,26,12,33,40,21,18,10,20,35,
	//			34,41,27,1,35,13,13,39,18,25,20,9,38,23,13,26,4,29,21,31,35,38,29,4,22,34,9,18,9,37,7,41,3,25,27,36,18,41,25,29,9,
	//			26,33,3,10,35,4,4,35,26,0,33,18,11,30,36,33,11,26,10,38,5,5,18,4,38,30,30,1,38,39,27,16,21,12,39,25,10,33,38,41,21,
	//			33,2,27,12,37,18,18,28,5,34,20,9,6,39,6,28,22,1,0,8,27,36,7,3,6,29,30,5,8,29,0,23,19,32,29,24,28,5,9,32,23,32,9,9,
	//			15,20,30,19,2,20,8,31,17,17,32,17,22,23,26,19,32,15,37,41,27,0,27,27,26,10,16,34,11,11,6,16,10,7,36,6,29,24,9,36,
	//			11,41,15,30,19,37,37,15,37,37,38,9,19,32,20,20,11,38,6,31,27,37,31,2,30,2,25,25,15,10,41,20,0,41,24,20,11,12,8,39,
	//			12,12,41,15,25,39,0,10,32,7,24,39,32,25,31,31,25,14,28,26,11,14,29,21,4,5,9,34,5,14,34,3,18,15,4,2,5,34,13,17,40,
	//			40,37,1,24,23,21,27,8,8,10,32,25,14,13,5,14,10,11,25,3,3,6,3,1,15,35,24,7,35,6,40,28,34,35,35,0,0,28,3,41,23,20,14,
	//			39,19,7,3,36,22,10,4,41,17,19,27,30,13,19,5,36,24,30,36,10,23,36,7,4,41,41,28,12,35,12,10,29,40,33,40,6,37,31,23,
	//			15,1,16,2,14,12,33,17,30,36,21,20,31,19
	//	};
	int initalSeedMakespan = getMakespan(ch, jobs, machines);
	//	printf("total jobs = %d ,total machines = %d \n",jobs.size(),machines.size());
	int tt = sqrt(jobs.size()*machines.size());

	//	m.lock();
	//	printf("Initial Seed makespan for thread[%d] = %d\n",tid,initalSeedMakespan);
	////	cout<<"tabuTenure = "<<tt<<endl;
	//	m.unlock();

	int lastIterMakespan = initalSeedMakespan;
	int swappedSeedMakespan = 999999;

	//	vector<int> newCh=ch;
	//	for(int i=0;;i++){
	//		vector<int> newCh=ch;
	//		for(int j=0;j<ch.size();j++){
	//			int temp=0;
	//
	//			temp=newCh.at(i);
	//			newCh.at(i)=newCh.at(j);
	//			newCh.at(j)=temp;
	//			swappedSeedMakespan=getMakespan(ch, jobs, machines);
	//			cout << "makespan for iterations("<<i<<","<<j<<") = "<<swappedSeedMakespan<< endl;
	//			if(swappedSeedMakespan<lastIterMakespan){
	////				cout << "swapped Seed makespan for iterations("<<i<<") = "<<swappedSeedMakespan<< endl;
	//				ch=newCh;
	//				lastIterMakespan=swappedSeedMakespan;
	//			}
	//		}
	//	}
	//
	for (int i=0;i< maxGenerations;i++){

	cout << "iterations(x)"<< endl;

	//	for(int i=0;;i++){
	//	std::chrono::duration<double, std::milli> elapsed ;

		//		cout << "iterations("<<i<<")"<< endl;

		vector<int> newCh=shuffler(ch);
		//vector<int> newCh = swapper(ch, tt, tabuList);

		swappedSeedMakespan = getMakespan(ch, jobs, machines);

		//		cout << "iterations("<<i<<")"<< endl;
		if (swappedSeedMakespan < lastIterMakespan) {
			//			m.lock();
			//			cout << "swapped Seed makespan for thread ["<<tid<<"] after iterations("<<i<<") = "<<swappedSeedMakespan<< endl;
			//			m.unlock();
			ch = newCh;
			lastIterMakespan = swappedSeedMakespan;
		}
		if (swappedSeedMakespan <= 1850) {
			break;

		}

		//		auto finishA = std::chrono::high_resolution_clock::now();
		//		std::chrono::duration<double, std::milli> elapsed =(finishA-start);
		//		if(elapsed.count()>=10000){
		//			cout<<"breaking\n";
		//			break;
		//		}
		//		c_end = clock();
		//	    printf("c_end = %.2lf",c_end/ (double)CLOCKS_PER_SEC);
		i++;
		//		cout<<"continuing\n";

	}
	auto finish = std::chrono::high_resolution_clock::now();
	m.lock();
	//	std::cout << __func__ << " took "<< std::chrono::duration_cast<milli>(finish - start).count()<< " milliseconds\n";


	printf("final Seed makespan for thread[] after iterations(%d) = %d\n", lastIterMakespan);
	m.unlock();
}*/
void runGA(int maxIterations) {

	const int N = 2;
	thread threads[N];


	for (int tid = 0; tid < N; tid++) {

		//threads[tid] = thread(runGeneticAlgo, maxIterations, tid);
		//		threads[tid].id=tid;
	}

	for (int tid = 0; tid < N; tid++) {

		threads[tid].join();
	}

}