#pragma once
#include "classes.h"
#include "json.hpp"
using json = nlohmann::json;

using namespace std;

extern json jsonData;


void loadJSON(string filename);

void loadJobsData(vector<Job>& jobs);

void loadMachinesData(vector<Machine>& machines);

unordered_map<string, vector<int>> mapMachinesToOperations(vector<Machine>& machines);