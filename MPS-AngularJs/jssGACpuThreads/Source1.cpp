#include "classes.h"
// #include <sys/time.h>
#include "HelperFunctions.h"
#include "Genetic.h"
#include "ParseJSON.h"
#include <thread>


const int popSize = 16;
const int genSize = 100;

int bestParentId = -1;
int bestChildId = -1;

int bestFitness = -1;

//jobs and machines for each chromosome
// vector<vector<Job>>newJobs(popSize);
// vector<vector<Machine>>newMachines(popSize);
// vector<vector<Job>>newJobsC(popSize);
// vector<vector<Machine>>newMachinesC(popSize);

vector<vector<int>> parentPop(popSize);
// parentPop.reserve(popSize);
vector<int> makespanP(popSize);
// makespanP.reserve(popSize);

vector<vector<int>> childPop(popSize);
// childPop.reserve(popSize);
vector<int>makespanC(popSize);

using namespace std;
using namespace std::chrono;

char* getCmdOption(char** begin, char** end, const std::string& option) {

	char** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end) {
		return *itr;
	}
	return 0;
}


bool cmdOptionExists(char** begin, char** end, const std::string& option) {
	return std::find(begin, end, option) != end;
}

void jobsAndMachinesAllocator(vector<vector<Job>>&newJobs, vector<vector<Machine>>&newMachines,vector<vector<Job>>&newJobsC, 
vector<vector<Machine>>&newMachinesC,int id) {
	newJobs[id] = jobs;
	newMachines[id] = machines;
	newJobsC[id] = jobs;
	newMachinesC[id] = machines;
}

void executeGA(int popSize,int maxGenerations) {

	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	thread *threads=new std::thread[popSize];

	// vector<vector<int>> parentPop(popSize);
	// // parentPop.reserve(popSize);
	// vector<int> makespanP(popSize);
	// // makespanP.reserve(popSize);

	// vector<vector<int>> childPop(popSize);
	// // childPop.reserve(popSize);
	// vector<int>makespanC(popSize);
	// // makespanC.reserve(popSize);


	for (int gen = 0; gen < maxGenerations; gen++) {

		// cout << "gen = " << gen<<" , "<<endl;
		//for (int i = 0; i < popSize; i++) {
		//	newJobs[i] = jobs;
		//	newMachines[i] = machines;
		//}
		// for (int i = 0; i < popSize; i++) {
		// 	threads[i] = thread(jobsAndMachinesAllocator, ref(newJobs), ref(newMachines),ref(newJobsC), ref(newMachinesC),i);
		// }
		// for (int i = 0; i < popSize; i++) {
		// 	threads[i].join();
		// }
		// for (int i = 0; i < popSize; i++) {
		// 	threads[i] = thread(jobsAndMachinesAllocator, ref(newJobsC), ref(newMachinesC), i);
		// }
		// for (int i = 0; i < popSize; i++) {
		// 	threads[i].join();
		// }

		for (int i = 0; i < popSize; i++) {
			//parentPop.push_back(generateChromosome(jobs,parentPop,i));
			//generateChromosome(jobs, parentPop, i);
			// threads[i] = thread(generateChromosome, ref(newJobs[i]), ref(parentPop), i);
			threads[i] = thread(generateChromosome, jobs, ref(parentPop), i);
		}
		for (int i = 0; i < popSize; i++) {
			threads[i].join();
		}

		// auto finish = std::chrono::high_resolution_clock::now();

		// std::cout << "generation took "
		// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
		// 	<< " milliseconds\n" << endl;

		for (int i = 0; i < popSize; i++) {
			//makespanP.push_back(getMakespan(ch, jobs, machines));
			//getMakespan(parentPop[i], jobs, machines);
			// threads[i] = thread(getMakespan, ref(parentPop[i]), ref(newJobs[i]), ref(newMachines[i]), ref(makespanP), i);
			threads[i] = thread(getMakespan, ref(parentPop[i]), jobs, machines, ref(makespanP), i);
		}
		for (int i = 0; i < popSize; i++) {
			threads[i].join();
		}


		int bestParentFitness = *min_element(makespanP.begin(), makespanP.end());
		int bestParentIndex = getIndex(makespanP, bestParentFitness);
		//cout << "best parent = " << bestParentFitness <<" , ";

		// std::cout << "getMakespan took "
		// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
		// 	<< " milliseconds\n" << endl;


		for (int i = 0; i < popSize; i += 2) {
			//childPop.push_back(onePointCrossover(parentPop[i], parentPop[i + 1]));
			threads[i] = thread(onePointCrossover, ref(parentPop[i]), parentPop[i + 1], ref(childPop[i]));
			//childPop.push_back(onePointCrossover(parentPop[i+1], parentPop[i]));
			threads[i + 1] = thread(onePointCrossover, ref(parentPop[i + 1]), parentPop[i], ref(childPop[i + 1]));
		}
		for (int i = 0; i < popSize; i++) {
			threads[i].join();
		}
		//
		// std::cout << "crossover took "
		// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
		// 	<< " milliseconds\n" << endl;


		for (int i = 0; i < popSize; i++) {
			//mutate(ch);
			threads[i] = thread(mutate, ref(childPop[i]));
		}
		for (int i = 0; i < popSize; i++) {
			threads[i].join();
		}

		/*for (auto m : machines) {
			m.newTimeSlots.clear();
			m.newTimeSlots.insert(TimeSlot(0, 5000));
		}*/

		// std::cout << "mutate  took "
		// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
		// 	<< " milliseconds\n" << endl;

		/*for (int i = 0; i < popSize; i++) {
			newJobs[i] = jobs;
			newMachines[i] = machines;
		}*/

		for (int i = 0; i < popSize; i++) {
			//makespanC.push_back(getMakespan(ch, jobs, machines));
			//getMakespan(ch, jobs, machines);
			// threads[i] = thread(getMakespan, ref(childPop[i]), ref(newJobsC[i]), ref(newMachinesC[i]), ref(makespanC), i);
			threads[i] = thread(getMakespan, ref(childPop[i]), jobs, machines, ref(makespanC), i);
			// std::cout << "getMakespan took "
			// 	<< std::chrono::duration_cast<chrono::milliseconds>(high_resolution_clock::now() - start).count()
			// 	<< " milliseconds\n" << endl;
		}
		for (int i = 0; i < popSize; i++) {
			threads[i].join();
		}


		int bestChildFitness = *min_element(makespanC.begin(), makespanC.end());
		int bestChildIndex= getIndex(makespanC, bestChildFitness);
		int worstChildFitness = *max_element(makespanC.begin(), makespanC.end());
		int worstChildIndex = getIndex(makespanC, worstChildFitness);

		//cout << "best child = " << bestChildFitness << " , ";
		//cout << "worst child = " << worstChildFitness << endl;

		childPop[worstChildIndex] = parentPop[bestParentIndex];

		//parentPop.clear();
		for (int i = 0; i < popSize;i++) {
			parentPop[i]=childPop[i];
			//newJobs
			//newMachines
		}

		if (bestParentFitness >= bestChildFitness) {
			//cout << "best is parent having = " << bestParentFitness << endl;
			bestParentId = bestParentIndex;
			bestChildId = -1;
			bestFitness = bestParentFitness;
		}
		else {
			//cout << "best is child having = " << bestChildFitness << endl;bestFitness
			bestChildId = bestChildIndex;
			bestParentId = -1;
			bestFitness = bestChildFitness;
		}

	}

	//delete threads;

	auto finish = std::chrono::high_resolution_clock::now();
	std::cout << __func__ << " took "
		<< std::chrono::duration_cast<chrono::milliseconds>(finish - start).count()
		<< " milliseconds\n";

}

vector<int> getTokensofLine(string s) {
	vector<int> tokens;
	std::string delimiter = ",";

	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
		token = s.substr(0, pos);
		if (token.compare("")) {
			tokens.push_back(stoi(token));
		}
		s.erase(0, pos + delimiter.length());
	}

	return tokens;
}


int main(int argc, char* argv[]) {
	// char* filename = getCmdOption(argv, argv + argc, "-f");
	char* streamsChar = getCmdOption(argv, argv + argc, "-s");

	loadData();
	//vector<int>makespan(popSize);

	//for (int i = 0; i < 5000; i++)
		//getMakespan(generateChromosome(jobs), jobs, machines);
	//cout << "starting..." << endl;

	// newJobs.reserve(popSize);
	// newMachines.reserve(popSize);
	// newJobsC.reserve(popSize);
	// newMachinesC.reserve(popSize);

	cout<<"popSize = "<<popSize<<", genSize = "<<genSize<<endl;

	executeGA(popSize,genSize);

	cout << "best fitness = " << bestFitness << endl;

	if (bestChildId < bestParentId) {
		// writeCSVsortedByOperation(newJobs[bestParentId]);
		writeCSVsortedByOperation(jobs);
	}
	else {
		// writeCSVsortedByOperation(newJobsC[bestChildId]);
		writeCSVsortedByOperation(jobs);
	}

	//writeCSVsortedByMachine(newMachines[0]);
	//runGeneticAlgo(2,100);
	//runGA(7000);

	/*vector<int> ch = { 26,35,17,11,1,25,16,27,2,36,23,27,37,21,41,8,30,22,5,33,7,20,35,29,17,37,29,28,36,7,25,41,21,18,40,36,9,23,25,4,8,18,27,13,34,13,4,4,7,19,15,24,31,41,29,33,25,41,21,0,29,35,41,16,22,12,27,37,14,2,1,26,10,31,36,1,6,14,39,34,11,41,19,1,34,9,26,3,30,20,41,10,12,5,15,28,12,39,8,31,36,30,23,20,22,20,20,30,28,27,25,10,38,33,26,27,25,20,33,26,6,10,31,33,0,21,22,38,11,17,34,26,36,26,16,0,29,6,18,22,41,19,30,0,18,24,23,5,33,11,21,11,22,3,15,12,12,31,38,28,13,4,6,8,1,24,16,16,9,16,31,34,15,14,27,35,23,24,32,32,28,32,2,4,2,9,32,10,39,40,40,38,39,28,14,39,33,29,18,25,39,14,29,19,27,41,5,19,17,41,10,6,20,20,1,20,37,3,6,27,33,22,1,41,35,13,21,10,17,16,10,0,4,10,14,22,28,3,3,20,10,9,17,1,6,0,0,20,6,36,0,32,41,10,5,36,23,28,28,18,23,16,18,34,37,11,30,1,29,9,3,3,37,20,17,28,13,35,29,37,13,12,23,38,20,38,7,34,20,20,29,8,40,7,27,1,1,0,15,36,1,37,37,22,18,35,3,38,17,4,21,15,36,25,12,26,27,2,7,30,32,4,21,34,8,26,15,34,27,30,9,13,18,8,15,21,13,13,13,17,12,13,24,24,11,5,13,9,5,24,15,32,24,9,31,14,9,32,14,35,8,36,39,32,19,36,6,23,26,5,5,40,11,31,37,5,5,35,5,35,25,11,4,6,23,30,31,41,23,39,8,39,37,39,19,11,23,3,39,14,35,35,6,21,40,11,40,16,14,14,39,14,14,18,23,14,40,19,16,8,6,6,3,14,31,28,3,3,25,22,12,25,7,18,3,0,27,26,26,26,17,37,3,28,20,28,19,38,10,5,30,37,15,36,40,41,39,10,12,18,25,25,12,40,14,7,28,16,4,24,35,0,4,22,24,18,6,3,2,38,38,25,17,40,9,17,38,31,21,0,18,31,38,4,23,17,12,5,27,23,16,40,23,20,0,28,24,7,2,41,23,40,38,40,11,17,4,28,24,32,37,2,28,41,26,26,23,7,35,29,36,11,33,35,22,9,19,26,40,5,25,4,23,36,1,40,31,14,12,4,9,41,8,8,35,4,8,19,19,10,12,2,5,15,1,15,34,32,22,21,38,34,15,37,9,27,7,31,24,19,15,22,35,35,10,2,22,6,39,15,10,10,10,29,33,37,37,4,33,33,30,37,15,25,40,8,28,11,9,11,32,32,35,2,20,13,7,2,20,13,20,7,16,13,12,33,11,16,19,2,33,11,11,16,6,6,36,7,36,21,36,38,37,27,12,30,37,6,40,22,5,34,34,15,31,2,30,40,13,16,20,28,32,7,13,12,29,41,6,10,27,27,13,20,37,31,36,29,2,31,32,27,27,19,1,35,39,25,19,14,33,34,19,1,39,32,13,12,2,19,19,4,36,8,12,39,19,9,21,16,5,32,14,0,9,33,0,21,25,30,18,24,24,32,24,24,30,34,34,22,22,9,36,17,7,7,29,15,26,17,41,41,21,7,32,17,16,0,30,30,4,26,26,26,30,40,29,30,26,40,38,3,30,40,41,5,17,17,32,18,5,6,17,29,8,36,32,41,3,29,12,11,8,18,14,18,9,9,33,18,9,40,33,6,1,14,39,32,16,38,22,41,5,35,20,3,16,33,39,21,38,38,38,21,41,41,24,17,22,18,13,25,30,19,9,25,12,27,3,1,12,35,35,11,35,35,26,2,7,31,17,15,0,35,31,31,3,41,35,0,11,7,0,8,4,25,29,10,27,27,10,37,37,31,16,34,41,0,34,17,29,28,34,23,1,30,5,8,32,18,8,28,5,33,22,36,33,5,13,30,5,39,36,38,36,28,15,39,8,39,38,0,36,27,21,15,1,5,3,23,2,2,41,29,21,13,24,24,32,34,3,27,36,21,2,29,6,25,39,20,26,0,5,25,19,10,20,16,7,25,7,31,34,8,28,37,41,11,35,36,12,4 };
	cout << getMakespan(ch, jobs, machines);*/


	/*int asad;
	cin >> asad;*/
	/*float average = accumulate(timeTaken.begin(), timeTaken.end(), 0.0) / timeTaken.size();
	 cout << "\n\n\nAverage (us):" << average;*/

	 // std::ifstream file("asad.txt");
	 // std::string str;
	 // while (getline(file, str)) {
	 // 	//cout << str << endl;
	 // 	// Process str
	 // 	vector<int> ch = getTokensofLine(str);
	 // 	cout << getMakespan(ch, jobs, machines) << ",";
	 // }

	 // vector<int> ch = {
	 // 	17, 37, 31, 33, 29, 15, 40, 19, 12, 10, 31, 7, 1, 34, 7, 10, 26, 40, 22, 18, 2, 37, 25, 16, 31, 22, 29, 5, 14, 7, 37, 14, 22, 21, 30, 1, 6, 0, 36, 25,
	 // 	7, 37, 20, 23, 16, 3, 1, 10, 18, 14, 16, 29, 2, 39, 15, 25, 9, 25, 0, 41, 13, 8, 18, 40, 21, 0, 7, 2, 0, 18, 25, 35, 41, 31, 3, 4, 31, 33, 26, 8, 12,
	 // 	38, 13, 13, 25, 23, 23, 1, 0, 8, 35, 24, 9, 20, 5, 5, 41, 36, 31, 28, 5, 6, 32, 2, 33, 23, 19, 27, 14, 36, 41, 13, 21, 17, 6, 26, 36, 12, 17, 2, 28, 19,
	 // 	7, 32, 7, 24, 25, 32, 39, 26, 2, 34, 10, 36, 32, 39, 35, 27, 35, 2, 12, 5, 11, 11, 41, 20, 24, 40, 35, 18, 16, 9, 11, 18, 2, 34, 22, 31, 32, 16, 22, 6,
	 // 	10, 36, 8, 27, 19, 41, 34, 5, 27, 9, 14, 19, 37, 34, 36, 38, 31, 27, 24, 14, 21, 40, 0, 0, 4, 39, 14, 28, 13, 1, 7, 11, 6, 38, 18, 30, 41, 3, 34, 8, 23,
	 // 	39, 23, 2, 41, 3, 19, 31, 20, 39, 11, 15, 39, 27, 31, 17, 35, 9, 37, 32, 13, 40, 35, 32, 9, 30, 17, 35, 38, 26, 4, 39, 24, 14, 31, 20, 24, 36, 25, 40,
	 // 	8, 38, 3, 3, 41, 20, 20, 17, 27, 4, 33, 41, 40, 17, 40, 2, 7, 16, 3, 10, 38, 6, 12, 19, 40, 38, 4, 29, 41, 3, 26, 14, 16, 36, 37, 7, 37, 2, 6, 9, 16,
	 // 	28, 34, 40, 1, 0, 15, 39, 26, 9, 7, 15, 21, 1, 0, 25, 20, 11, 17, 6, 10, 20, 12, 30, 22, 12, 16, 5, 39, 23, 1, 35, 21, 33, 35, 4, 24, 1, 23, 39, 21, 6,
	 // 	12, 10, 31, 10, 6, 4, 19, 10, 3, 12, 0, 15, 33, 3, 17, 10, 14, 27, 23, 4, 37, 8, 13, 38, 18, 12, 22, 10, 19, 8, 26, 40, 12, 21, 16, 36, 23, 6, 39, 26,
	 // 	14, 40, 20, 36, 8, 0, 3, 32, 23, 20, 11, 16, 15, 39, 20, 25, 15, 29, 22, 29, 15, 19, 9, 32, 35, 7, 2, 32, 26, 27, 12, 41, 22, 5, 1, 18, 9, 13, 29, 11,
	 // 	11, 28, 37, 9, 18, 13, 18, 10, 38, 22, 36, 41, 23, 6, 22, 41, 27, 24, 24, 1, 30, 28, 6, 3, 32, 19, 6, 8, 22, 26, 0, 30, 41, 37, 9, 7, 13, 25, 28, 25,
	 // 	13, 10, 26, 37, 4, 5, 8, 8, 41, 34, 20, 17, 33, 39, 3, 4, 13, 40, 4, 33, 27, 35, 23, 17, 27, 32, 15, 40, 17, 14, 20, 29, 31, 30, 9, 30, 38, 0, 30, 9,
	 // 	35, 1, 16, 32, 30, 29, 19, 40, 27, 14, 26, 37, 31, 32, 32, 0, 36, 32, 11, 37, 10, 31, 17, 38, 41, 14, 20, 5, 28, 9, 7, 26, 24, 26, 34, 6, 32, 27, 29,
	 // 	25, 24, 30, 38, 39, 29, 26, 36, 30, 11, 6, 38, 22, 35, 7, 28, 37, 18, 0, 34, 21, 14, 25, 19, 6, 4, 36, 22, 32, 17, 34, 3, 33, 33, 17, 20, 41, 21, 34,
	 // 	21, 24, 41, 17, 13, 25, 11, 0, 12, 23, 11, 30, 19, 28, 2, 35, 21, 21, 6, 1, 25, 22, 1, 8, 16, 38, 23, 33, 18, 28, 4, 3, 15, 18, 16, 36, 25, 14, 41, 30,
	 // 	9, 16, 37, 20, 14, 5, 16, 34, 13, 12, 12, 21, 7, 16, 12, 20, 15, 5, 26, 16, 25, 19, 29, 40, 12, 35, 27, 24, 11, 23, 30, 16, 2, 40, 41, 35, 39, 20, 39,
	 // 	30, 41, 27, 33, 10, 21, 31, 35, 5, 5, 27, 34, 8, 23, 8, 24, 40, 27, 3, 13, 25, 38, 24, 21, 37, 7, 22, 37, 24, 36, 34, 30, 0, 33, 1, 19, 20, 5, 8, 10, 4,
	 // 	36, 5, 31, 12, 32, 9, 36, 15, 27, 13, 2, 7, 28, 41, 29, 27, 24, 10, 1, 21, 0, 33, 21, 28, 38, 25, 4, 36, 19, 37, 18, 40, 16, 33, 17, 12, 28, 17, 18, 17,
	 // 	19, 32, 17, 31, 5, 4, 5, 5, 35, 8, 21, 15, 2, 4, 12, 24, 33, 28, 0, 5, 28, 6, 18, 34, 5, 3, 30, 33, 13, 37, 3, 22, 4, 27, 2, 39, 37, 14, 36, 8, 20, 0,
	 // 	39, 40, 39, 36, 32, 38, 19, 12, 29, 26, 35, 36, 30, 19, 25, 5, 13, 25, 8, 24, 9, 28, 13, 15, 7, 28, 41, 17, 32, 14, 9, 25, 15, 33, 23, 5, 2, 19, 26, 31,
	 // 	21, 29, 37, 32, 35, 34, 6, 36, 13, 18, 40, 35, 41, 19, 34, 35, 11, 26, 38, 14, 33, 24, 27, 22, 36, 36, 41, 9, 33, 16, 41, 18, 20, 30, 11, 13, 26, 16,
	 // 	39, 7, 27, 23, 38, 3, 9, 34, 11, 5, 36, 27, 41, 23, 34, 31, 30, 1, 10, 35, 35, 37, 22, 29, 4, 7, 5, 29, 35, 13, 29, 37, 15, 25, 36, 32, 38, 35, 41, 17,
	 // 	28, 14, 29, 20, 15, 39, 12, 22, 11, 29, 0, 28, 12, 26, 10, 10, 38, 15, 15, 11, 17, 23, 21, 34, 30, 6, 4, 18, 31, 33, 31, 3, 6, 11, 8, 29, 3, 28, 8, 1,
	 // 	22, 0, 20, 39, 4, 20, 35, 8, 11, 28, 1, 2, 5, 10, 5, 7, 3, 27, 1, 40, 9, 11, 38, 28, 26, 15, 18, 2, 30, 29
	 // };
	 //
	 // cout << getMakespan(ch, jobs, machines);


	 // vector<vector<int>> pop;
	 // for (int i = 0; i < 16; i++) {
	 // 	pop.push_back(generateChromosome(jobs));
	 // }
	 //
	 // vector<int> makespan;
	 // for (auto ch : pop) {
	 // 	// printVector(ch);
	 // 	makespan.push_back(getMakespan(ch, jobs, machines));
	 // }
	 // printVector(makespan);
	 // cout << "______________________________________" << endl;
	 //
	 // makespan.clear();
	 // for (auto i = 0; i < 16; i += 2) {
	 // 	vector<int> ch1 = onePointCrossover(pop[i], pop[i + 1]);
	 // 	vector<int> ch2 = onePointCrossover(pop[i + 1], pop[i]);
	 // 	mutate(ch1);
	 // 	mutate(ch2);
	 // 	// printVector(ch1);
	 // 	// printVector(ch2);
	 // 	makespan.push_back(getMakespan(ch1, jobs, machines));
	 // 	makespan.push_back(getMakespan(ch2, jobs, machines));
	 // 	// pop.push_back(ch1);
	 // 	// pop.push_back(ch2);
	 // }
	 //
	 // printVector(makespan);
	 // cout << "_______________________________________" << endl;
	 //
	 // for(auto ch:pop) {
	 // 	cout << getMakespan(ch, jobs, machines) << "\t";
	 // }

	 // cout << "......." << endl;


	 // if (streamsChar == 0) {
	 // 	NUMBER_OF_STREAMS = 1;
	 // 	cout << "No streams provided, defaulting to 1 stream" << endl;
	 // }

	 // 	else {
	 // //		int streamsNum = atoi(streamsChar);
	 // 		NUMBER_OF_STREAMS = atoi(streamsChar);
	 // 		cout << "Streams:" << NUMBER_OF_STREAMS << endl;
	 // 	}

	 // try {
	 // 	if (!filename) {
	 // 		throw std::invalid_argument("no input file provided");
	 // 	}
	 //
	 // }
	 // catch (const std::invalid_argument& ia) {
	 // 	std::cerr << "Invalid argument: " << ia.what() << '\n';
	 // 	exit(-1);
	 // }

	 //	using milli = std::chrono::milliseconds;
	 //	auto start = std::chrono::high_resolution_clock::now();


	 // loadJobsData(jobs, jsonData);
	 // loadMachinesData(machines, jsonData);
	 // machinesMap = mapMachinesToOperations(machines);

	 // for (auto& job : jobs) {
	 // 	for (auto& operation : job.operations) {
	 // 		vector<Machine>& eligibleMachines = machinesMap.at(operation.operationCode);
	 // 		//
	 //
	 // 		for (auto& eligibleMachine : eligibleMachines) {
	 // 			int index = getIndex(machines, eligibleMachine);
	 // 			operation.eligibleMachinesVector.push_back(index);
	 // 		}
	 //
	 // 		operation.eligibleMachinesSize = operation.eligibleMachinesVector.size();
	 // 	}
	 // }

	 // int jobSize = jobs.size();
	 // int machineSize = machines.size();


	 // testFunc();

	 // loadJSON(FILENAME);
	 // loadJobsData(jobs);
	 // loadMachinesData(machines);
	 // machinesMap = mapMachinesToOperations(machines);

	 // for (int i = 0; i < 10; i++) {
	 // 	vector<int> ch = generateChromosome(jobs);
	 // 	mutate(ch);
	 // 	// printVector(ch);
	 // 	cout << endl;
	 // }
	 // getMakespan(ch, jobs, machines);
	 // int a;
	 // cin >> a;

	 // vector<vector<int>> parentPopulation;
	 // for (auto i = 0; i < 4; i++) {
	 // 	parentPopulation.push_back(generateChromosome(jobs));
	 // }
	 //
	 // vector<int> parentFitness;
	 // vector<int> childFitness;
	 //
	 // vector<vector<int>> childPop;
	 //
	 // for (auto ch : parentPopulation) {
	 // 	parentFitness.push_back(getMakespan(ch, jobs, machines));
	 // }
	 // cout << "Parent Fitness:" << endl;
	 // printVector(parentFitness);
	 // cout << "_____________________" << endl;
	 //
	 // for (int i = 0; i < 100; i++) {
	 //
	 // 	childPop.push_back(onePointCrossover(parentPopulation[0], parentPopulation[2]));
	 // 	childPop.push_back(onePointCrossover(parentPopulation[1], parentPopulation[3]));
	 // 	childPop.push_back(onePointCrossover(parentPopulation[2], parentPopulation[0]));
	 // 	childPop.push_back(onePointCrossover(parentPopulation[3], parentPopulation[1]));
	 //
	 // 	for (auto ch : parentPopulation) {
	 // 		cout << ch.size() << " ";
	 // 	}
	 // 	cout << endl;
	 //
	 // 	for (auto ch : childPop) {
	 // 		cout << ch.size() << " ";
	 // 	}
	 // 	cout << endl;
	 //
	 //
	 // 	for (auto ch : childPop) {
	 // 		mutate(ch);
	 // 	}
	 //
	 // 	for (auto ch : childPop) {
	 // 		childFitness.push_back(getMakespan(ch, jobs, machines));
	 // 	}
	 //
	 // 	int bestParent = *min_element(parentFitness.begin(), parentFitness.end());
	 // 	int bestParentFitness = getIndex(parentFitness, bestParent);
	 // 	int bestChild = *min_element(childFitness.begin(), childFitness.end());
	 //
	 // 	int worstChild = *max_element(childFitness.begin(), childFitness.end());
	 // 	int worstChildIndex = getIndex(childFitness, worstChild);
	 //
	 // 	if (bestParent < bestChild) {
	 // 		childPop[worstChildIndex] = parentPopulation[bestParentFitness];
	 // 		childFitness[worstChildIndex] = parentFitness[bestParentFitness];
	 // 	}
	 //
	 // 	parentPopulation.clear();
	 // 	parentFitness.clear();
	 //
	 // 	for (auto ch : childPop) {
	 // 		parentPopulation.push_back(ch);
	 //
	 // 	}
	 //
	 //
	 // 	for (auto ch : childFitness) {
	 // 		parentFitness.push_back(ch);
	 //
	 // 	}
	 //
	 // 	printVector(parentFitness);
	 //
	 // 	childPop.clear();
	 // 	childFitness.clear();
	 //
	 //
	 // }


	 // vector<int> ch = generateChromosome();

	 // vector<int> ch = {2, 1, 1, 0, 0, 1, 1, 2, 1, 0, 2, 2, 2, 2};
	 //printChromosome(ch);

	 // vector<int> ch = {
	 // 	35, 32, 34, 34, 41, 30, 13, 25, 4, 10, 4, 23, 40, 20, 23, 30, 23, 1, 22, 3, 26, 18, 41, 28, 18, 17, 20, 36, 5, 6, 9, 41, 5, 29, 11,
	 // 	16, 31, 30, 26, 24, 26, 30, 14, 3, 23, 0, 22, 8, 8, 36, 38, 41, 21, 20, 40, 33, 10, 33, 22, 38, 40, 25, 33, 12, 33, 5, 28, 13, 40,
	 // 	4, 14, 5, 20, 3, 35, 37, 31, 2, 33, 19, 10, 15, 39, 21, 6, 17, 3, 37, 39, 27, 25, 11, 34, 17, 1, 16, 32, 7, 40, 10, 7, 35, 34, 25,
	 // 	33, 27, 27, 9, 37, 35, 40, 2, 11, 6, 28, 20, 25, 31, 4, 13, 22, 35, 32, 40, 37, 10, 41, 1, 37, 30, 26, 28, 34, 16, 7, 35, 8, 21, 38,
	 // 	25, 0, 13, 28, 7, 36, 23, 31, 22, 1, 22, 25, 28, 4, 25, 12, 33, 38, 6, 38, 36, 5, 25, 6, 1, 0, 3, 26, 26, 19, 14, 5, 4, 34, 23, 36,
	 // 	36, 32, 14, 21, 36, 36, 32, 21, 31, 41, 39, 14, 26, 31, 13, 16, 15, 3, 15, 33, 20, 29, 10, 0, 17, 5, 19, 31, 15, 37, 24, 18, 25, 2,
	 // 	20, 20, 12, 23, 0, 19, 29, 20, 19, 37, 10, 15, 28, 23, 2, 27, 24, 0, 22, 37, 28, 24, 3, 30, 17, 38, 27, 36, 27, 20, 16, 32, 16, 35,
	 // 	18, 0, 0, 25, 8, 12, 18, 39, 19, 7, 10, 9, 28, 32, 26, 26, 14, 13, 35, 37, 40, 27, 12, 26, 37, 30, 22, 3, 28, 20, 20, 8, 9, 20, 20,
	 // 	30, 41, 13, 11, 19, 5, 16, 10, 27, 35, 41, 9, 29, 29, 31, 19, 8, 0, 14, 4, 39, 34, 29, 16, 22, 1, 21, 5, 19, 36, 2, 10, 38, 38, 32,
	 // 	31, 6, 5, 33, 17, 6, 3, 35, 16, 35, 38, 32, 39, 11, 9, 40, 16, 36, 30, 36, 1, 39, 26, 18, 29, 0, 5, 40, 24, 17, 22, 9, 0, 12, 24,
	 // 	23, 8, 17, 17, 12, 33, 33, 28, 32, 6, 21, 23, 17, 6, 26, 25, 8, 14, 4, 20, 4, 27, 18, 38, 33, 9, 35, 25, 14, 2, 27, 35, 36, 18, 41,
	 // 	5, 4, 1, 26, 11, 32, 8, 39, 7, 35, 31, 5, 37, 34, 37, 8, 34, 29, 6, 19, 0, 3, 19, 24, 4, 17, 27, 32, 12, 16, 29, 31, 28, 16, 10, 29,
	 // 	3, 34, 31, 20, 15, 4, 9, 8, 36, 41, 36, 16, 29, 24, 25, 32, 14, 27, 19, 8, 7, 3, 12, 7, 23, 9, 6, 9, 10, 14, 14, 36, 7, 33, 40, 36,
	 // 	17, 3, 21, 7, 22, 40, 2, 33, 40, 18, 15, 5, 27, 32, 24, 11, 2, 0, 41, 39, 35, 18, 38, 4, 16, 2, 13, 22, 17, 9, 34, 28, 34, 1, 15,
	 // 	20, 4, 16, 28, 25, 40, 17, 1, 12, 34, 36, 28, 12, 10, 7, 13, 27, 36, 9, 39, 15, 11, 27, 9, 20, 33, 20, 23, 38, 28, 37, 1, 27, 31,
	 // 	30, 36, 3, 8, 37, 19, 3, 26, 29, 21, 13, 20, 26, 15, 34, 14, 17, 17, 3, 14, 32, 4, 35, 31, 8, 15, 2, 36, 12, 9, 30, 6, 41, 4, 5, 41,
	 // 	21, 39, 4, 24, 41, 10, 40, 21, 5, 37, 22, 15, 7, 27, 32, 18, 31, 29, 27, 2, 3, 39, 6, 30, 6, 11, 26, 9, 10, 14, 7, 30, 10, 9, 41, 2,
	 // 	25, 5, 24, 12, 14, 15, 5, 20, 22, 28, 35, 22, 23, 7, 11, 22, 31, 41, 6, 27, 24, 37, 41, 39, 34, 4, 33, 1, 23, 19, 18, 7, 41, 37, 35,
	 // 	40, 15, 38, 35, 19, 21, 39, 16, 19, 21, 9, 19, 30, 28, 39, 13, 40, 15, 19, 30, 5, 1, 3, 38, 8, 4, 0, 35, 12, 21, 39, 36, 10, 8, 39,
	 // 	8, 32, 25, 39, 5, 37, 10, 14, 4, 39, 25, 11, 20, 17, 16, 32, 24, 5, 13, 39, 19, 6, 18, 33, 4, 9, 11, 30, 1, 21, 13, 30, 18, 30, 1,
	 // 	35, 26, 23, 36, 5, 0, 29, 29, 28, 3, 41, 27, 13, 11, 35, 12, 12, 5, 17, 24, 14, 5, 0, 22, 36, 7, 6, 31, 34, 1, 11, 5, 15, 6, 13, 16,
	 // 	3, 15, 16, 0, 12, 37, 27, 24, 2, 39, 30, 21, 17, 6, 34, 21, 22, 32, 41, 28, 29, 36, 37, 25, 2, 18, 19, 11, 26, 29, 17, 23, 26, 10,
	 // 	23, 30, 12, 17, 26, 3, 41, 41, 7, 0, 17, 11, 2, 29, 14, 41, 13, 10, 18, 8, 40, 40, 33, 11, 12, 27, 19, 24, 1, 31, 41, 40, 1, 29, 18,
	 // 	40, 13, 9, 30, 2, 11, 22, 16, 39, 0, 30, 29, 37, 24, 13, 12, 18, 38, 41, 6, 6, 25, 35, 25, 21, 38, 18, 0, 32, 24, 8, 24, 28, 9, 13,
	 // 	9, 20, 30, 18, 2, 12, 40, 41, 5, 2, 37, 29, 22, 31, 1, 21, 15, 10, 27, 32, 38, 0, 8, 38, 5, 26, 36, 7, 11, 12, 23, 7, 28, 15, 35, 7,
	 // 	11, 18, 13, 31, 28, 27, 31, 23, 1, 35, 0, 20, 19, 38, 13, 22, 15, 23, 25, 29, 12, 34, 21, 33, 2, 34, 7, 7, 41, 25, 3, 35, 11, 14,
	 // 	13, 26, 41, 33, 32, 10, 39, 31, 34, 40, 8, 36, 14, 27, 34, 36, 2, 38, 17, 33, 8, 21, 25, 15, 16, 37, 6, 35, 41, 20, 26, 32, 23, 16,
	 // 	32, 11, 38, 35, 38, 37, 24
	 // };

	 // printVector(ch);
	 // cout << getMakespan(ch, jobs, machines) << endl;
	 //
	 // int size = 0;
	 // for (auto job : jobs) {
	 // 	/*for(auto operation:job.operations) {
	 // 		if(!operation.mappable) {
	 // 			size += 1;
	 // 		}
	 // 	}*/
	 // 	size += job.operations.size();
	 // }
	 // cout << "JobCount:" << jobs.size() << endl;
	 // cout << "OperationCount:" << accumulate(begin(jobs), end(jobs), 0,
	 // [](int i, const Job& o) { return o.operations.size() + i; }) << endl;
	 // generateChromosome();
	 //
	 // vector<int> parent1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	 // vector<int> parent2 = {11, 1, 13, 14, 2, 16, 3, 4, 5, 20};
	 // onePointCrossover(parent1, parent2);
	 //
	 // printVector(parent1);
	 // mutate(parent1);
	 // printVector(parent1);
	 //
	 // vector<vector<int>> population;
	 // for (int i = 0; i < 8; i++) {
	 // 	population.push_back(generateChromosome());
	 // }
	return 0;
}
