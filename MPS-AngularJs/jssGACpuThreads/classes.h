#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <random>
#include <chrono>
#include <unordered_map>
#include <set>

#include<thread>

using namespace std;

class Operation;
class Job;
class Machine;
class Tuple;
class ScheduledOperation;
class Managed;
class TimeSlot;


const int timeSlotsSize = 5000;
//const int scheduledOperationsSize = 900000;

struct OrderedOperation {
	int job;
	int operation;
	string opCode;
	int duration;

	OrderedOperation(int job, int operation, string opCode, int duration) {
		this->job = job;
		this->operation = operation;
		this->opCode = opCode;
		this->duration = duration;
	}
};


class TimeSlot {

public:

	int startTime;
	int duration;

	bool isEmpty() {
		return startTime == 9999999 && duration == 9999999;
	}

	TimeSlot() {
		startTime = 9999999;
		duration = 9999999;
	}

	TimeSlot(int startTime, int duration) {
		this->startTime = startTime;
		this->duration = duration;

	}

	void setStartTime(int startTime) {
		this->startTime = startTime;
	}

	void setDuration(int duration) {
		this->duration = duration;
	}

	void clearTimeSlot() {
		startTime = 9999999;
		duration = 9999999;
	}
};

class TimeSlotIndex {
public:
	// int index;
	int operationStartTime;
	TimeSlot timeSlot;
	int machineIdx;

	TimeSlotIndex(const TimeSlot& timeSlot, int operationStartTime, int machineIdx)
		:operationStartTime(operationStartTime), timeSlot(timeSlot), machineIdx(machineIdx) {
		// this->timeSlot = timeSlot;
		// this->index = index;
		// this->operationStartTime = operationStartTime;

	}
};

struct TsComp {
	bool operator()(const TimeSlot& o1, const TimeSlot& o2) {
		return o1.startTime < o2.startTime;
	}
};

class Operation {

public:
	string operationCode;
	int operationDuration;
	int operationSequence;
	int operationStartTime;
	int operationEndTime;
	int operationIndex;
	int operationId;
	//Job *jobPtr;
	// int* eligibleMachinesIndices;
	// vector<int> eligibleMachinesVector;
	// int eligibleMachinesSize;
	//	const char* operationCodeChar;
	int allotedMachine;
	bool mappable;
	int FOQtyIndex;

	void clear();

	Operation(string operationCode, int operationDuration, int operationId, int operationIndex, bool mappable,int FOQtyIndex);

	Operation(string operationCode, int operationDuration, int operationIndex, int operationSequence, bool mappable) {
		this->operationCode = operationCode;
		this->operationDuration = operationDuration;
		this->operationSequence = operationSequence;
		this->mappable = mappable;
		this->operationIndex = operationIndex,
		this->operationStartTime = 0;
		this->operationEndTime = 0;

	}

	Operation() {
	}

	// void initializeEligibleMachinesIndicesArray(int size);

	friend bool operator==(const Operation& o1, const Operation& o2);

	Operation(const Operation& operation) {
		operationDuration = operation.operationDuration;
		operationCode = operation.operationCode;
		operationId = operation.operationId;
		// operationSequence = operation.operationSequence;
		operationStartTime = operation.operationStartTime;
		operationEndTime = operation.operationEndTime;
		// eligibleMachinesIndices = operation.eligibleMachinesIndices;
		// eligibleMachinesSize = operation.eligibleMachinesSize;
		// eligibleMachinesVector = operation.eligibleMachinesVector;
		//		operationCodeChar = operationCode.c_str();
		allotedMachine = operation.allotedMachine;
		operationIndex = operation.operationIndex;
		mappable = operation.mappable;
		FOQtyIndex = operation.FOQtyIndex;
	}

};


class Tuple {
public:
	int start;
	int end;

	Tuple(int start, int end);

	Tuple() {
		start = 0;
		end = 0;
	}

};

class Machine {
public:
	int machineID;
	string operationCode;
	string machineName;
	int machineFreeTime;
	//vector<ScheduledOperation> scheduledOperations;

	int timeSlots[timeSlotsSize] = { 0 };
	//	bool* timeSlotsPtr;
	//	Tuple usableTimeSlot;
	// ScheduledOperation scheduledOperations[scheduledOperationsSize] = {ScheduledOperation()};

	vector<ScheduledOperation> scheduledOperations;

	// ScheduledOperation scheduledOperations[scheduledOperationsSize] = {ScheduledOperation()};

	int scheduledOperationIndex = 0;

	// TimeSlot newTimeSlots[timeSlotsSize];

	struct TimeSlotCompare {
		bool operator() (const TimeSlot& lhs, const TimeSlot& rhs) const {
			// stringstream s1, s2;
			// s1 << lhs;
			// s2 << rhs;
			return lhs.startTime < rhs.startTime;
		}
	};

	set<TimeSlot, TimeSlotCompare> newTimeSlots;


	int lastTimeSlotIndex = 0;

	TimeSlotIndex getSuitableTimeSlot(int operationIdx, int prevOpEndTime, int operationDuration) {

		int startTime=0;

		for (auto timeSlot : newTimeSlots) {

			int slotStartTime = timeSlot.startTime;
			int slotDuration = timeSlot.duration;
			int slotEndTime = slotStartTime + slotDuration;

			if (prevOpEndTime < slotEndTime) {

				if (operationIdx == 0) {

					if (slotDuration >= operationDuration) {
						// startTime = slotStartTime;

						return TimeSlotIndex(timeSlot, slotStartTime, machineID);

					}
				}

				else {
					if (prevOpEndTime <= slotStartTime) {

						if (slotDuration >= operationDuration) {
							// startTime = slotStartTime;
							return TimeSlotIndex(timeSlot, slotStartTime, machineID);

						}
					}

					else if (prevOpEndTime > slotStartTime && prevOpEndTime <= slotEndTime) {

						if (slotEndTime - prevOpEndTime >= operationDuration) {
							// startTime = prevOpEndTime;
							return TimeSlotIndex(timeSlot, prevOpEndTime, machineID);

						}
					}
				}

				//		if (newTimeSlots[i].startTime >= prevOpEndTime && operationDuration <= newTimeSlots[i].duration){

			}

			//				return new TimeSlotIndex(&newTimeSlots[i], i);
			//				return newTimeSlots[i];

		}
	}

	void scheduleOperation(vector<Job>& jobs, TimeSlotIndex& bestTimeSlot, Operation& currentOperation, const int jobIdx);
	void scheduleOperation(vector<Job>& jobs, int operationStartTime, int operationDuration, Operation& currentOperation, const int jobIdx);

	// void sortTimeSlots() {
	//
	// 	// thrust::sort(thrust::seq, newTimeSlots, newTimeSlots + (timeSlotsSize - 1), TsComp());
	// 	std::sort(newTimeSlots, newTimeSlots + (timeSlotsSize - 1),
	// 	          [](TimeSlot const& a, TimeSlot const& b) -> bool { return a.startTime < b.startTime; });
	// }

	void addNewTimeSlot(int startTime, int duration) {
		// newTimeSlots[lastTimeSlotIndex].startTime = startTime;
		// newTimeSlots[lastTimeSlotIndex].duration = duration;
		// lastTimeSlotIndex += 1;
		// sortTimeSlots();
		TimeSlot timeSlot(startTime, duration);
		pair<set<TimeSlot>::iterator, bool> ret;

		ret = this->newTimeSlots.insert(timeSlot);
		// cout << "addNewTimeSlot() called" << endl;
	}

	void clearTimeSlot(TimeSlot& timeSlot) {
		// newTimeSlots[timeSlotIndex].clearTimeSlot();
		// // sortTimeSlots();
		// lastTimeSlotIndex -= 1;
		//newTimeSlots.erase(find(newTimeSlots.begin(), newTimeSlots.end(), timeSlot));
		newTimeSlots.erase(timeSlot);
	}


	void clear();

	Machine(int machineID, string operationCode, string machineName);

	Machine() {

	}

	vector<Tuple> getFreeTimeSlots();

	//	__device__ __host__ Tuple* getFreeTimeSlotsPtr();

	friend bool operator==(const Machine& m1, const Machine& c2);

	// Machine(const Machine& machine) {
	// 	machineName = machine.machineName;
	// 	machineID = machine.machineID;
	// 	operationCode = machine.operationCode;
	// 	machineFreeTime = machine.machineFreeTime;
	// 	//scheduledOperations = machine.scheduledOperations;
	// 	lastTimeSlotIndex = machine.lastTimeSlotIndex;
	//
	// 	for (int i = 0; i < timeSlotsSize; i++) {
	// 		timeSlots[i] = machine.timeSlots[i];
	// 		// newTimeSlots[i] = machine.newTimeSlots[i];
	//
	// 	}
	//
	// 	// for (int i = 0; i < scheduledOperationsSize; i++) {
	// 	// 	scheduledOperations[i].jobPtr = machine.scheduledOperations[i].jobPtr;
	// 	// 	scheduledOperations[i].operationPtr = machine.scheduledOperations[i].operationPtr;
	// 	// }
	//
	// }

};

class Job {
public:
	int jobIndex;
	int jobCode;
	vector<Operation> operations;
	int unscheduledOperationIndex;
	Operation* operationsPtr;
	int operationsSize;
	int quantity;
	int priority;
	bool mapped = false;

	Job() {
	};

	Job(int jobCode, int FOQtyIndex, int pr);

	Job(int jobCode) {
		this->jobCode = jobCode;
		this->unscheduledOperationIndex = 0;
	}

	void clear();

	void addOperation(Operation operation);
	friend bool operator==(const Job& j1, const Job& j2);

	Job(const Job& job) {
		jobCode = job.jobCode;

		/*for (int i = 0; i < operations.size();i++) {
		 operations[i] = *(new Operation(job.operations[i]));

		 }*/
		operations = job.operations;
		unscheduledOperationIndex = job.unscheduledOperationIndex;
		operationsPtr = job.operationsPtr;
		jobIndex = job.jobIndex;
		quantity = job.quantity;
		priority = job.priority;
		mapped = job.mapped;
	}

};

class ScheduledOperation {
public:
	// Job * jobPtr;
	// Operation* operationPtr;

	Job jobRef;
	Operation operationRef;

	ScheduledOperation(const Job& job, const Operation& operation) : jobRef(job), operationRef(operation) {
	}

	ScheduledOperation() {
		// jobRef = NULL, jobRef = NULL;
	}

	friend bool operator==(const ScheduledOperation& o1, const ScheduledOperation& o2);

};