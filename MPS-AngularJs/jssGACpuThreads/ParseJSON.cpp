
#include "ParseJSON.h"
#include "HelperFunctions.h"

json jsonData;

const int AIRCRAFTS_PER_BATCH = 1;

vector<int> prioritiesContainer;

int PartsQty = 0;

const int BASE_TIME_UNIT_MINUTES = 15;


void loadJSON(string filename) {

	// json jsonData;
	ifstream is;

	is.open(filename, ifstream::in);
	is >> jsonData;

	// return jsonData;
}

void loadJobsData(vector<Job>& jobs) {
	jobs.clear();
	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	/*for (json section : jsonData["fos"]) {
		for (json job : section["jobs"]) {
			int jobcode = job["jobcode"];
			int qty = job["qty"];

			Job newJob(jobcode, qty, 1);

			for (json operation : job["operations"]) {

				for (int i = 0; i < qty; i++) {
					Operation newOperation(operation["type"],
						ceil(operation["duration"].get<int>() / (double)BASE_TIME_UNIT_MINUTES),
						operation["operationId"].get<int>(), operation["sequence"].get<int>(), operation["mappable"], i);

					//				newOperation.operationIndex = newOperation.operationSequence;

					newJob.addOperation(newOperation);
					//cout << operation["type"] << ceil(operation["duration"].get<int>() / (double) 60) << operation["sequence"] << operation["mappable"] << endl;
				}

			}
			jobs.push_back(newJob);
			jobs.back().jobIndex = getIndex(jobs, newJob);
		}
	}*/
	for (json job : jsonData["fabricationOrders"]) {
		int jobcode = job["FOID"];

		Job newJob(jobcode);

		for (json operation : job["operations"]) {
			Operation newOperation(operation["operationType"],
				int(ceil(operation["operationDuration"].get<int>() / (double)BASE_TIME_UNIT_MINUTES)),
				operation["operationID"], operation["operationSequence"], operation["mappable"]);
			//			newOperation.operationIndex = operation["operationID"];
			newJob.addOperation(newOperation);
			//			cout << operation["operationType"] << ceil(operation["operationDuration"].get<int>() / (double) 15) <<
			//			operation["operationSequence"] << operation["mappable"] << endl;
		}
		jobs.push_back(newJob);
		jobs.back().jobIndex = getIndex(jobs, newJob);
	}

	auto finish = std::chrono::high_resolution_clock::now();
	std::cout << "loadJobsData() took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
		<< " milliseconds\n";
}


void loadMachinesData(vector<Machine>& machines) {
	machines.clear();
	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	/*for (json machineObj : jsonData["availableMachines"]) {
		for (json machine : machineObj["machines"]) {
			machines.push_back(Machine(machine["machineId"], machine["code"], machine["name"]));
		}
	}*/
	for (json machine : jsonData["machines"]) {
		Machine newMachine(machine["machineID"], machine["machineOperationCode"], machine["machineName"]);
		//cout << machine["machineID"]<< machine["machineOperationCode"]<<machine["machineName"]<< endl;

		machines.push_back(newMachine);
	}

	auto finish = std::chrono::high_resolution_clock::now();
	std::cout << "loadMachinesData() took "
		<< std::chrono::duration_cast<milli>(finish - start).count()
		<< " milliseconds\n";

	//	for (Machine machine : machines) {
	//		cout << machine.machineID << "\t" << machine.operationCode << "\t" << machine.machineName << endl;
	//	}
}

unordered_map<string, vector<int>> mapMachinesToOperations(vector<Machine>& machines) {
	vector<string> uniqueOperations;
	unordered_map<string, vector<int>> machinesMap;

	for (auto machine : machines) {
		if (find(uniqueOperations.begin(), uniqueOperations.end(), machine.operationCode) == uniqueOperations.end()) {
			uniqueOperations.push_back(machine.operationCode);
		}
	}

	for (auto uniqueOperation : uniqueOperations) {
		vector<int> machineIdxes;
		machinesMap[uniqueOperation] = machineIdxes;
	}

	for (auto machine : machines) {
		string operation = machine.operationCode;
		machinesMap[operation].push_back(getIndex(machines, machine));
	}

	return machinesMap;
}