var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var  machineSchema = new Schema({
    
    
    machineId:Number,
    machineType:String,
    machineCapacity:Number,
    machineInstallationDate:String,
    machineStatus:Number
    
  },
  {
      collection:'machines'
  }
);

// the schema is useless so far
// we need to create a model using it
var Machine = mongoose.model('Machine', machineSchema);

// make this available to our Machine in our Node applications
module.exports = Machine;