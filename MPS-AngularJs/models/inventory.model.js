var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var inventorySchema = new Schema({
    
    id: Number,
    description: String,
    inStockQuantity: Number,
    newOrderQuantity: Number,
    actualQuantity:Number,
    avaiableRaw:Number,
    requestedRaw:Number,
    daysToRecieveRaw:Number
    
  },
  {
      collection:'inventory'
  }
);

// the schema is useless so far
// we need to create a model using it
var Inventory = mongoose.model('Inventory', inventorySchema);

// make this available to our Inventory in our Node applications
module.exports = Inventory;