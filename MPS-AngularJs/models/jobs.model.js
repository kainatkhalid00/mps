var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var  jobSchema = new Schema({
    
    allotedWorker:String,
    jobId:Number,
    jobType:String,
    jobDuration:Number,
    startTime:String,
    endTime:String,
    jobStatus:Number
    
  },
  {
      collection:'jobs'
  }
);

// the schema is useless so far
// we need to create a model using it
var Job = mongoose.model('Job', jobSchema);

// make this available to our Job in our Node applications
module.exports = Job;