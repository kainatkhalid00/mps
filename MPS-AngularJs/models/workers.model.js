var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var  workerSchema = new Schema({
    
    name:String,
    id:Number
    
  },
  {
      collection:'workers'
  }
);

// the schema is useless so far
// we need to create a model using it
var Worker = mongoose.model('Worker', workerSchema);

// make this available to our Job in our Node applications
module.exports = Worker;