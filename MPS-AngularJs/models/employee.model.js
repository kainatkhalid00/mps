var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var employeeSchema = new Schema({
    employeeId: Number,
    name: String,
    designation:String,
    department:String,
    gender:String,
    salary:Number
  },
  {
      collection:'employees'
  }
);

// the schema is useless so far
// we need to create a model using it
var Employee = mongoose.model('Employee', employeeSchema);

// make this available to our Employee in our Node applications
module.exports = Employee;