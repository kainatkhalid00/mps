var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var  jobStatusSchema = new Schema({
    
    wait:Number,
    wip:Number,
    done:Number
    
  },
  {
      collection:'jobStatus'
  }
);

// the schema is useless so far
// we need to create a model using it
var JobStatus = mongoose.model('JobStatus', jobStatusSchema);

// make this available to our Job in our Node applications
module.exports = JobStatus;