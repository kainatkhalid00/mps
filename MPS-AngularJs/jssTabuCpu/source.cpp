
#include "classes.h"
//#include "tabuSearch.h"
#include<map>

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

//#include <Python.h>

#include<iostream>
#include <chrono>
#include "json.hpp"
using namespace std;
//using namespace std::chrono;
using json = nlohmann::json;

vector<Job> jobs;
vector<Machine> machines;
//vector<Operation> operations;
vector<AnOperation> operations;
map<string,vector<int>> machinesMap;

//ScheduledOperationTuple **sOT;

int totalJobs=0;
int totalMachinesSet=0;
//int totalOperations

const int timeSlotsSize=20000;


int **seed;

int ***tabuList;
const int tabuTenure=100;


template<typename T>
int getIndex(const vector<T>& vec, const T& obj) {
	return (distance(vec.begin(), find(vec.begin(), vec.end(), obj)));
}

char* getCmdOption(char** begin, char** end, const std::string& option) {

	char** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end) {
		return *itr;
	}
	return 0;
}

void loadJobsData(json& jsonData){

	jobs.clear();

	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	for (json section : jsonData["fos"]) {
		for (json job : section["jobs"]) {
			int jobCode = job["jobcode"];

			Job newJob(jobCode);

			for (json operation : job["operations"]) {
//				Operation newOperation(operation["foId"],operation["mappable"],operation["type"],operation["sectionId"],operation["operationId"],
//						operation["sequence"],ceil(operation["duration"].get<int>() / (double) 60));

//				newOperation.operationIndex = newOperation.operationSequence;

				AnOperation newOperation(jobCode,operation["operationId"],operation["type"],operation["mappable"],ceil(operation["duration"].get<int>() / (double) 60));
//				newOperation.duration=operation["duration"];
//				cout<<ceil(operation["duration"].get<int>() / (double) 60)<<endl;

				operations.push_back(newOperation);

				newJob.addAnOperation(newOperation);

//				newJob.opSize=int(operation["sequence"])+1;

//				cout << operation["type"] << ceil(operation["duration"].get<int>() / (double) 60) << operation["sequence"] << operation["mappable"] << endl;

			}
			newJob.opSize=newJob.operations.size();
			jobs.push_back(newJob);
//			jobs.back().jId = getIndex(jobs, newJob);
		}
	}

	auto finish = std::chrono::high_resolution_clock::now();
	 std::cout << "loadJobsData() took "
	 	<< std::chrono::duration_cast<milli>(finish - start).count()
	 	<< " milliseconds\n";

}

void loadMachinesData(json& jsonData){

	machines.clear();
	using milli = std::chrono::milliseconds;
	auto start = std::chrono::high_resolution_clock::now();

	for (json machineObj : jsonData["availableMachines"]) {
		for (json machine : machineObj["machines"]) {
			Machine newMachine(machine["machineId"], machine["code"],machine["name"]);

//			cout<<machine["machineId"]<< machine["code"]<<machine["name"]<<endl;

			machines.push_back(newMachine);
		}
	}

	auto finish = std::chrono::high_resolution_clock::now();
	std::cout << "loadMachinesData() took "<< std::chrono::duration_cast<milli>(finish - start).count()	<< " milliseconds\n";

//	for (Machine machine : machines) {
//		cout << machine.machineID << "\t" << machine.operationCode << "\t" << machine.machineName << endl;
//	}

}

//void loadOperationsData(vector<Operation>& operations,json& jsonData){
//
//}

void mapMachinesToOperations(){

	machinesMap.clear();

	string mCode;//,opType;
	int nOp=operations.size();
	int nJ=jobs.size();
	int nM=machines.size();
	printf("total operations = %d ,total jobs = %d ,total machines = %d\n",nOp,nJ,nM);

	totalJobs=nJ;


	vector<string> opType;
	opType.clear();

	for(auto op:operations){

		opType.push_back(op.opName);

	}
	sort(opType.begin(),opType.end());
	opType.erase(unique(opType.begin(),opType.end()),opType.end());

	printf("total unique operationTypes = %d",int(opType.size()));

	for(auto op:opType){
//		cout<<op<<endl;

		vector<int> machinesIdVector;

		for (auto m :machines){
			if(op==m.mCode){
				machinesIdVector.push_back(m.mId);
			}
		}

		machinesMap[op]=machinesIdVector;
	}


	totalMachinesSet=machinesMap.size();

//	for(auto m:machines){
//		string mCode=m.c
//	}

}


int generateRandomNumber(int start=0,int end=10){

	random_device rd;
	mt19937 eng(rd());
	uniform_int_distribution<> distr(start,end);

	return distr(eng);

}


void  generateInitialSeed(int seedIdx){
//
//	using milli = std::chrono::milliseconds;
//	auto start = std::chrono::high_resolution_clock::now();

	const int nJ=jobs.size();
	int jobsCountArray[nJ];//=new int[nJ];

	int i=0;
	for(auto j :jobs){
		jobsCountArray[i]=j.opSize;
		i++;
	}

//	for(int j=0;j<nJ;j++){
//		cout<<"job["<<j<<"= has operations = "<<jobsCountArray[j]<<endl;
//	}

	const int nOp=operations.size();

	int isAllSet=0;
	i=0;
	while(isAllSet==0){
//		cout<<i<<endl;
//		cout<<"random number "<<i<<" = "<<generateRandomNumber(1,9551)<<endl;

		int jRandIdx=generateRandomNumber(0,nJ-1);
//		cout<<"jRandIdx = "<<jRandIdx<<endl;

		if(jRandIdx>=0 && jRandIdx<=nJ){

			if (jobsCountArray[jRandIdx]>0){
				jobsCountArray[jRandIdx]--;

				if (i==int(operations.size())){
					break;
				}

				seed[seedIdx][i]=jRandIdx;


				i++;

				int isLastJobCountZero=0;
				for (int j=0;j<nJ;j++){

					if (jobsCountArray[j]==0){
						isLastJobCountZero++;
					}
					if(isLastJobCountZero==nJ){
						isAllSet=1;
					}
				}
			}
		}
	}

	cout<<"seed = ";
	for( i=0;i<nOp;i++){
		cout<<seed[seedIdx][i]<<" ";
	}


//	auto finish = std::chrono::high_resolution_clock::now();
//	std::cout << "\ngenerateInitialSeed() took "<< std::chrono::duration_cast<milli>(finish - start).count()	<< " milliseconds";


//	cout<<endl;

}

void get_free_time_slot_optimized(int idx,vector<Machine> &localMachines, int* eligibleMachineIndices, int operationIndex,
		int currentOpDuration, int prevOpEndTime, int*timeSlots) {

	int machineIndex = eligibleMachineIndices[idx];

	int i = prevOpEndTime;

//	int suitableStartTime=-1;

//	int selectNextIndexAsSuitable=0;
	if (prevOpEndTime>=localMachines.at(machineIndex).lastFilledIndex){
		timeSlots[idx]=localMachines.at(machineIndex).lastFilledIndex;
		return;
	}
	else if (prevOpEndTime<=localMachines.at(machineIndex).timeSlotsArray.at(0).start && currentOpDuration<=localMachines.at(machineIndex).timeSlotsArray.at(0).duration){
		timeSlots[idx]=localMachines.at(machineIndex).timeSlotsArray.at(0).start;
		return;
	}

	for (auto tsa:localMachines.at(machineIndex).timeSlotsArray){
//		cout<<"z@kii test123456\n";

//		printf("mid = %d , tsa St = %d, tsa D = %d\n",machineIndex,tsa.start,tsa.duration);

		if(		currentOpDuration<=tsa.duration &&
				prevOpEndTime>=tsa.start &&
				prevOpEndTime+currentOpDuration<=tsa.start+tsa.duration ){


			timeSlots[idx]=prevOpEndTime;

			return;
		}
		else if(prevOpEndTime<=tsa.start && currentOpDuration<=tsa.duration){
			timeSlots[idx]=tsa.start;
			return;
		}
	}


//	cout<<"suitableStartTime = "<<suitableStartTime<<endl;

//	timeSlots[idx] = suitableStartTime;

}


//void get_free_time_slot_optimized_new(int idx,vector<Machine> &localMachines, int* eligibleMachineIndices, int operationIndex,
//		int currentOpDuration, int prevOpEndTime, int*timeSlots) {
////	int idx = threadIdx.x;
//
//	int machineIndex = eligibleMachineIndices[idx];
//
//	int i = prevOpEndTime;
//
//	int start = 0;
//	int end = 0;
//
//	int suitableStartTime = -1;
//
//
//	if (prevOpEndTime >= localMachines.at(machineIndex).lastFilledIndex) {
//		timeSlots[idx] = prevOpEndTime;
//		return;
//	}
//
//	start=i;
////	while(i<=localMachines.at(machineIndex).lastFilledIndex && localMachines.at(machineIndex).timeSlots[i]==1 && i<timeSlotsSize){
////		i++;
////	}
//
//	while(i<=localMachines.at(machineIndex).lastFilledIndex && i<timeSlotsSize){
//		if(localMachines.at(machineIndex).timeSlots[i]==0){
//
////			cout<<i<<endl;
//			i++;
//			continue;
//		}
//		end=i;
//		if(currentOpDuration<=(end-start)){
//			timeSlots[idx]=start;
//			break;
//		}
////		else{
////			start=i;
////		}
//		i++;
//	}
//
//	timeSlots[idx]=prevOpEndTime;
//
//
//}

void get_free_time_slot_optimized_old(int idx,vector<Machine> &localMachines, int* eligibleMachineIndices, int operationIndex,
		int currentOpDuration, int prevOpEndTime, int*timeSlots) {
//	int idx = threadIdx.x;

	int machineIndex = eligibleMachineIndices[idx];

	int i = prevOpEndTime;

	int start = 0;
	int end = 0;

	int suitableStartTime = -1;

	if (prevOpEndTime >= localMachines.at(machineIndex).lastFilledIndex) {
//		printf("%d prevOpEndTime>=..........................\n",atomicAdd(&countVar, 1));
//		suitableStartTime = prevOpEndTime;
//		timeSlots[idx] = suitableStartTime;
		timeSlots[idx] = prevOpEndTime;
		return;
//		printf("%d prevOpEndTime>=###########################\n",atomicAdd(&countVar, 1));
//		return;
	}
////

//	while(i<=localMachines.at(machineIndex).lastFilledIndex && localMachines.at(machineIndex).timeSlots[i]!=0){
//
//		i++;
//		start=i;
////		if(i==localMachines.at(machineIndex).lastFilledIndex)
//	}
//	end

	while (i != timeSlotsSize) {

		while (localMachines.at(machineIndex).timeSlots[i] != 0) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		start = i;

		if (i == localMachines.at(machineIndex).lastFilledIndex) {
			timeSlots[idx] = i;
			return;
		}

		while (localMachines.at(machineIndex).timeSlots[i] != 1) {
			i += 1;
			if (i == timeSlotsSize)
				break;
		}

		end = i;

		int timeSlotDuration = end - start;

		if (operationIndex == 0) {
			////printf("opIndex==0\n");
			if (timeSlotDuration >= currentOpDuration) {
				suitableStartTime = start;
//				printf("timeSlotDuration>=currentOpDuration\n");
				break;
			}
		} else {
			if (prevOpEndTime <= start) {
				////printf("prevOpEndTime<=start\n");
				if (timeSlotDuration >= currentOpDuration) {
					suitableStartTime = start;
					////printf("timeSlotDuration>=currentOpDuration\n");
					break;
				}
			}

			else if (prevOpEndTime >= start && prevOpEndTime <= end) {
				////printf("prevOpEndTime >= start && prevOpEndTime <= end\n");

				if (end - prevOpEndTime >= currentOpDuration) {
					suitableStartTime = prevOpEndTime;
					////printf("end - prevOpEndTime >= currentOpDuration\n");
					break;
				}
			}
		}

	}



	timeSlots[idx] = suitableStartTime;
}

void mapOperationToMachine(int machineIndex, int jobIndex, int operationIndex, int startTime,vector<Machine> &localMachines,
		vector<Job> &localJobs) {

//	cout<<"in mapOperationsToMachine\n";

	AnOperation&currentOperation = localJobs.at(jobIndex).operations.at(operationIndex);

	Machine& eligibleMachine = localMachines.at(machineIndex);

	//mapping operation to machine

	currentOperation.startTime= startTime;
	currentOperation.endTime= startTime + currentOperation.duration;
	currentOperation.selectedMachine= eligibleMachine.mId;


	//updating free slots of the machine
	int p=0;
	for(auto tsa:localMachines.at(machineIndex).timeSlotsArray){

		if(tsa.start==startTime && tsa.duration>currentOperation.duration){

			TimeSlotTuple &tst(tsa);
			tst.start=startTime+currentOperation.duration;
			tst.duration=tsa.duration;


//			tsa.start=startTime+currentOperation.duration;
//			machines.at(machineIndex).timeSlotsArray.at(p).start=tsa.start;

//			cout<<"nodeStart is equal"<<endl;

		}
		else if(startTime>tsa.start && startTime+currentOperation.duration<tsa.start+tsa.duration){


			TimeSlotTuple &tst(tsa);
//			tst.start=startTime+currentOperation.duration;
			tst.duration=startTime-tsa.start;

//			tsa.duration=startTime-tsa.start;//before node

			TimeSlotTuple newTst;
			newTst.start=startTime+currentOperation.duration;
			newTst.duration=(tsa.start+tsa.duration)-(startTime+currentOperation.duration);//after node
			localMachines.at(machineIndex).timeSlotsArray.push_back(newTst);

//			cout<<"node is in center"<<endl;


		}
		else if(currentOperation.duration<=tsa.duration && (startTime+currentOperation.duration==tsa.start+tsa.duration)){
			tsa.duration=startTime-tsa.start;
			machines.at(machineIndex).timeSlotsArray.at(p).duration=tsa.duration;

//			cout<<"nodeEnd is equal"<<endl;
		}
		p++;
	}

	for (int i = startTime; i < startTime + currentOperation.duration; i++) {
		eligibleMachine.timeSlots[i] = 1;
		eligibleMachine.ScheduledOperationArray[i].jId= jobIndex;
		eligibleMachine.ScheduledOperationArray[i].opId= operationIndex;
//		eligibleMachine.machineScheduledOperationArray[i]=operationIndex;
//		eligibleMachine.machineScheduledJobArray[i]=jobIndex;
//		eligibleMachine.machineScheduledOperationArray[i]=operationIndex;
//		eligibleMachine.machineScheduledJobArray[i]=jobIndex;
//		printf("mId = %d , jId = %d , opId = %d\n",machineIndex,eligibleMachine.ScheduledOperationArray[i].jId,eligibleMachine.ScheduledOperationArray[i].opId);
//		printf("mId = %d , jId = %d , opId = %d\n",machineIndex,jobIndex,operationIndex);
	}



//	eligibleMachine.ScheduledOperationArray[machineScheduledOperationIndex[machineIndex]].operationPtr =&jobs[jobIndex].operationsPtr[operationIndex];


	if(currentOperation.endTime>eligibleMachine.lastFilledIndex){
		eligibleMachine.lastFilledIndex = currentOperation.endTime;
	}


}

void writeScheduleToCSV(vector<Machine>& localMachines,vector<Job>&localJobs){

	cout<<"\nin writeScheduleToCSV\n";

	ofstream myFile;
	myFile.open("schedule.csv");
//	myFile<<"t,mId,mType,jId,opId,\n";
//	for(auto mac:localMachines){
//		int t=0;
//		for(auto obj:mac.ScheduledOperationArray){
//			if(obj.jId==-1 || obj.opId==-1){
//				t++;
//				continue;
//			}
//			myFile<<t<<","<<mac.mId<<","<<mac.mName<<","<<obj.jId<<","<<obj.opId<<",\n";
//			t++;
//		}
//	}
	myFile<<"jId,opSeq,opId,st,et,dur,mid\n";
	for(auto j:localJobs){
		int opId=0;
		for(auto op:j.operations){
			myFile<<op.jobId<<","<<opId<<","<<op.opId<<","<<op.startTime<<","<<op.endTime<<","<<op.duration<<","<<op.selectedMachine<<",\n";
			opId++;
		}
	}
	myFile.close();


	cout<<"leaving writeScheduleToCSV\n";
}

void executeJSS(int seedId,vector<Machine> &localMachines,vector<Job> &localJobs,vector<AnOperation> &localOperations
		,map<string,vector<int>> localMachinesMap){

//	cout<<"in execute JSS\n";

	const int nJ=jobs.size();
	const int nOp=operations.size();

	int jobOpCount[nJ]={0};

	for(int s=0;s<nOp;s++){
		int j=seed[seedId][s];
		int op=jobOpCount[j];

//		printf("jId = %d , opId = %d \n",j,op);
//		jobOpCount[j]++;
//		continue;
//
//		if(j>=3){
//			continue;
//		}

		AnOperation& currentOp=localJobs.at(j).operations.at(op);
//			AnOperation& prevOp=localJobs.at(i).operations.at(j-1);


		if(!currentOp.isMappable ){

			//handle non mappable operations here

			int eligibleMachineIndex=localMachinesMap[currentOp.opName].at(0);

//			printf("isMappable = false , jobId = %d ,opId = %d , eligible machine index = %d\n",j,op,eligibleMachineIndex);

			int start=0;
//			if(op!=0){
				start=localMachines.at(eligibleMachineIndex).lastFilledIndex;
//			}

			currentOp.startTime = start;
			currentOp.endTime = start + currentOp.duration;
			currentOp.selectedMachine = eligibleMachineIndex;

//			mapOperationToMachine(eligibleMachineIndex, j, op, start, localMachines, localJobs);
			for (int i = start; i < start + currentOp.duration; i++) {
				localMachines.at(eligibleMachineIndex).timeSlots[i] = 1;
				localMachines.at(eligibleMachineIndex).ScheduledOperationArray[i].jId= j;
				localMachines.at(eligibleMachineIndex).ScheduledOperationArray[i].opId= op;
			}

//			if(currentOp.endTime>localMachines.at(eligibleMachineIndex).lastFilledIndex){
				localMachines.at(eligibleMachineIndex).lastFilledIndex= currentOp.endTime;
//			}

			jobOpCount[j]++;
			continue;
		}

		const int eligibleMachinesSize=localMachinesMap[currentOp.opName].size();
		int* timeSlotsLocal = new int[eligibleMachinesSize];
		for (int i = 0; i < eligibleMachinesSize; i++) {
			timeSlotsLocal[i] = 0;
		}

		int *eligibleMachinesIndices=new int[eligibleMachinesSize];
		int id=0;
//		for(auto mac:localMachines){
//			if(mac.mCode==currentOp.opName){
//				eligibleMachinesIndices[id]=mac.mId;
//				id++;
//			}
//		}
		for(auto mac:localMachinesMap[currentOp.opName]){
//			if(mac.mCode==currentOp.opName){
				eligibleMachinesIndices[id]=mac;
				id++;
//			}
		}

		if (op == 0) {

			for (int k=0;k<eligibleMachinesSize;k++){
//				get_free_time_slot_optimized_old(k,localMachines,eligibleMachinesIndices, op, currentOp.duration, 0,timeSlotsLocal);
				get_free_time_slot_optimized(k,localMachines,eligibleMachinesIndices, op, currentOp.duration, 0,timeSlotsLocal);
			}
//				printf("%d Thread # %d is now calling tsFinder\n", atomicAdd(&countVar, 1), idx);

		}

		else {

			for (int k=0;k<eligibleMachinesSize;k++){
//				printf("%d Thread # %d is now calling tsFinder\n", atomicAdd(&countVar, 1), idx);
//				get_free_time_slot_optimized_old(k,localMachines,eligibleMachinesIndices, op, currentOp.duration,
//						localJobs.at(j).operations.at(op-1).endTime, timeSlotsLocal);
				get_free_time_slot_optimized(k,localMachines,eligibleMachinesIndices, op, currentOp.duration,
						localJobs.at(j).operations.at(op-1).endTime, timeSlotsLocal);
			}

		}

		int minIdx = 0;
		for (int i = 1; i < eligibleMachinesSize; i++) {

			if (timeSlotsLocal[i] < timeSlotsLocal[minIdx])
				minIdx = i;
		}

		int eligibleMachineIndex = eligibleMachinesIndices[minIdx];

		int start = timeSlotsLocal[minIdx];

		delete timeSlotsLocal;



//		if (op == 0) {

//				printf("%d Thread # %d M # %d is now calling map for startTime = %d\n", atomicAdd(&countVar, 1), idx, eligibleMachineIndex,start);
//			mapOperationToMachine(eligibleMachineIndex, j, op, start, localMachines, localJobs);
//		}

//		else {
//				printf("%d Thread # %d M # %d is now calling map for startTime = %d\n", atomicAdd(&countVar, 1), idx, eligibleMachineIndex,start);

			mapOperationToMachine(eligibleMachineIndex, j, op, start, localMachines, localJobs);


//		}


		jobOpCount[j]++;
	}


//	for(auto mac:localMachines){
//		for(int i=0;i<mac.lastFilledIndex;i++){
//			cout<<mac.mId<<","<<mac.machineScheduledJobArray[i]<<","<<mac.machineScheduledOperationArray[i]<<",\n";
//		}
//	}

//	writeScheduleToCSV(localMachines);


}


//
//int* swapper(int threadId){
////	int len=(sizeof(seed)/sizeof(*seed));
//	int a =generateRandomNumber(0,int(operations.size()));
//	int b=generateRandomNumber(0,int(operations.size()));
//
//	while (a==b){
//		b=generateRandomNumber(0,int(operations.size()));
//	}
//
//
//	int &tempA=seed[threadId][a];
//	int &tempB=seed[threadId][b];
//
//
//	if(this->tabuList[seed[a]][seed[b]]==0){
//		this->tabuList[seed[a]][seed[b]]=this->tabuTenure;
//		this->tabuList[seed[b]][seed[a]]=this->tabuTenure;
//
//		seed[a]=tempB;
//		seed[b]=tempA;
//
//	}else{
//		this->tabuList[seed[a]][seed[b]]--;
//		this->tabuList[seed[b]][seed[a]]--;
//	}
//
//	//real swaping in seed is done here
//
//	return seed;
//
//}



void computeMakespan(int seedId){


	vector<Machine> localMachines=machines;
	vector<Job> localJobs=jobs;
	vector<AnOperation> localOperations=operations;
	map<string,vector<int>> localMachinesMap=machinesMap;


	generateInitialSeed(seedId);

	for(int N=0;N<1;N++){



		cout<<"initial seed generated successfully\n";

		using milli = std::chrono::milliseconds;
		auto start = std::chrono::high_resolution_clock::now();

		executeJSS(seedId,localMachines,localJobs,localOperations,localMachinesMap);

		int makespan=0;

		for (auto m:localMachines){
	//		cout<<"lfi of m["<<m.mId<<"] = "<<m.lastFilledIndex<<endl;
			if(makespan<m.lastFilledIndex){
				makespan=m.lastFilledIndex;
			}
		}

	//	for(auto j:localJobs){
	//		for(auto o:j.operations){
	//			if(makespan<o.endTime){
	//				makespan=o.endTime;
	//			}
	//		}
	//	}

		cout<<"makespan obtained by thread # ("<<seedId<<") = "<<int(makespan)<<endl;

		auto finish = std::chrono::high_resolution_clock::now();
		std::cout << "\ncomputeMakespan() took "<< std::chrono::duration_cast<milli>(finish - start).count()	<< " milliseconds";






	}
	//
	writeScheduleToCSV(localMachines,localJobs);

//	return makespan;

}

int main(int argc, char* argv[]){
	cout<<"*********************starting main*********************"<<endl;

	char* filename = getCmdOption(argv, argv + argc, "-f");

	json jsonData;

	ifstream is;
	is.open(string(filename),ifstream::in);
	is>>jsonData;

	loadJobsData(jsonData);
//	return 0;
	loadMachinesData(jsonData);
	cout<<"Jobs & Machines Data loaded successfully ...."<<endl;

	mapMachinesToOperations();
	cout<<"\nmapped Machines To Operations successfully ...."<<endl;

	const int seedSize=int(operations.size());
//	int seedGen[seedSize];
//	int *seedGen;//=new int[seedSize];
	const int threadSize=8;
	seed=new int*[threadSize];
	for (int i=0;i<threadSize;i++){
		seed[i]=new int[seedSize];
		for(int j=0;j<seedSize;j++){
			seed[i][j]=-1;
		}
	}

	//mentaining 2D-tabuLists for each thread
	const int jobSize=int(jobs.size());
	tabuList=new int**[threadSize];
	for(int i=0;i<threadSize;i++){
		tabuList[i]=new int*[jobSize];
		for(int j=0;j<jobSize;j++){
			tabuList[i][j]=new int[jobSize];
			for(int k=0;k<jobSize;k++){
				tabuList[i][j][k]=0;
			}
		}
	}


//	generateInitialSeed(1);
//	cout<<"initial seed generated successfully ...."<<endl;

//	cout<<"random number = "<<generateRandomNumber(0,jobs.size())<<endl;



//	TabuSearch ts;
//	int makespan=ts.computeMakespan(seedGen/*generateInitialSeed()*/,machinesMap,machines,operations,jobs);
//	int makespan=computeMakespanX(1);
	//int makespan=
	computeMakespan(1);
//	cout<<"makespan = "<<makespan<<endl;

//    Py_Initialize();

	cout<<"\n*********************exiting main*********************"<<endl;

//	while(1){}


//	delete seedGen;

//
//	char command[ 1024 ];
//	long pid = ( long ) ::getpid(); // use long to ensure correct format specifier
//	sprintf( command, "pmap %ld", pid );
//	system( command );


//	for(int j=0;j<threadSize;j++){
//		delete[] seed[j];
//	}
//	delete seed;

	return 0;
}



