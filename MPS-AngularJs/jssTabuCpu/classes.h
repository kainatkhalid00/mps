#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <random>
#include <chrono>
#include <map>
//#include "kernel.cuh"
//#include "device_functions.h"

using namespace std;

const int SLOTSSIZE = 20000;

class BestStartAndMachineId{
public:
	int start;
	int mId;

};

class TimeSlotTuple{
public:
	int start;
	int duration;
	TimeSlotTuple(){
		this->start=0;
		this->duration=SLOTSSIZE;
	}
	TimeSlotTuple(int s,int d){
		this->start=s;
		this->duration=d;
	}
	TimeSlotTuple (const TimeSlotTuple &tst){
		this->duration=tst.duration;
		this->start=tst.start;
	}
};

class ScheduledOperationTuple{
public:
	int jId;
	int opId;

	ScheduledOperationTuple(){
		this->jId=-1;
		this->opId=-1;
	}
	ScheduledOperationTuple(int j,int o){
		this->jId=j;
		this->opId=o;
	}
	ScheduledOperationTuple(const ScheduledOperationTuple& sot){
		this->jId=sot.jId;
		this->opId=sot.opId;
	}

	ScheduledOperationTuple operator=(const ScheduledOperationTuple& sot){
		this->jId=sot.jId;
		this->opId=sot.opId;
	}

};


class Machine{
public:
	int mId;
	string mCode;
	string mName;

	int lastFilledIndex;

	ScheduledOperationTuple ScheduledOperationArray[SLOTSSIZE];
	int machineScheduledOperationArray[SLOTSSIZE]={-1};
	int machineScheduledJobArray[SLOTSSIZE]={-1};
	vector<TimeSlotTuple> timeSlotsArray;
	int timeSlots[SLOTSSIZE]={0};

	Machine(){

		this->timeSlotsArray.clear();
		TimeSlotTuple tst;
		timeSlotsArray.push_back(tst);
		this->lastFilledIndex=0;
//		delete tst;

//		this->ScheduledOperationArray=new ScheduledOperationTuple[SLOTSSIZE];

	}
	Machine(int mId,string mCode,string mName){
		this->mId=mId;
		this->mCode=mCode;
		this->mName=mName;

		this->timeSlotsArray.clear();
		TimeSlotTuple tst;
		tst.start=0;
		tst.duration=SLOTSSIZE;
		timeSlotsArray.push_back(tst);
		this->lastFilledIndex=0;
	}
	Machine(int lastFilledIndex){
		this->lastFilledIndex=lastFilledIndex;
		this->timeSlotsArray.clear();
		TimeSlotTuple tst;
		timeSlotsArray.push_back(tst);
	}

	Machine (const Machine &machine){

		for(int i=0;i<SLOTSSIZE;i++){
			this->ScheduledOperationArray[i].jId=machine.ScheduledOperationArray[i].jId;
			this->ScheduledOperationArray[i].opId=machine.ScheduledOperationArray[i].opId;
		}

		this->lastFilledIndex=machine.lastFilledIndex;
		this->mCode=machine.mCode;
		this->mId=machine.mId;
		this->mName=machine.mName;
		this->timeSlotsArray=machine.timeSlotsArray;
//		this->

	}
	Machine operator=(const Machine &machine){

		for(int i=0;i<SLOTSSIZE;i++){
			this->ScheduledOperationArray[i]=machine.ScheduledOperationArray[i];
		}
		this->lastFilledIndex=machine.lastFilledIndex;
		this->mCode=machine.mCode;
		this->mId=machine.mId;
		this->mName=machine.mName;
		this->timeSlotsArray=machine.timeSlotsArray;
//		this->

	}

//	friend bool operator==(const Machine& m1, const Machine& m2);
	//{

//		return (m1.mId == m2.mId && m1.== m2.operationCode && m1.machineName == m2.machineName);
//	}
};



class Operation{
public:
	int foId;
	bool isMappable;
	string opType;
	int sectionId;
	int opId;
	int sequence;
	int duration;

	int selectedMachine;
	int startTime;
	int endTime;

	Operation(){

	}
	Operation(int foId,bool isMappable,string opType,int sectionId,int opId,int sequence,int duration){

		this->duration=duration;
		this->foId=foId;
		this->isMappable=isMappable;
		this->opId=opId;
		this->opType=opType;
		this->sectionId=sectionId;
		this->sequence=sequence;

	}


//	friend bool operator==(const Operation& o1, const Operation& o2);
//			{
//
//		return (operation1.operationCode == operation2.operationCode && operation1.operationDuration == operation2.operationDuration
//				&& operation1.operationSequence == operation2.operationSequence && operation1.operationStartTime == operation2.operationStartTime
//				&& operation1.operationEndTime == operation2.operationEndTime);

//	}

};


class AnOperation{
public:
	int jobId;
	int opId;
	string opName;
	bool isMappable;

	int duration;

	int selectedMachine;
	int startTime;
	int endTime;


	AnOperation (){

	}
	AnOperation (int jId,int oId,string opN,bool isMappable,int duration){
		this->jobId=jId;//jobCode
		this->opId=oId;
		this->opName=opN;
		this->isMappable=isMappable;
		this->duration=duration;

	}
};

class Job : AnOperation,Operation,Machine{
public:
	int jCode;
//	int jId;
	int qty;
	int opSize;

//	vector<Operation> operations;
	vector<AnOperation> operations;

	Job(){

		operations.clear();

	}
	Job(int jCode,int qty,int opSize){
		this->jCode=jCode;
		this->opSize=opSize;
		this->qty=qty;
	}

	Job(int jCode){
		this->jCode=jCode;
	}

//	void addOperation(Operation op){
//
//		this->operations.push_back(op);
//
//	}

	void addAnOperation(AnOperation op){

		this->operations.push_back(op);

	}

//	friend bool operator==(const Job& j1, const Job& j2);
	//{
//		return (job1.jobCode == job2.jobCode && job1.operations == job2.operations && job1.unscheduledOperationIndex == job2.unscheduledOperationIndex
//				&& job1.operationsPtr == job2.operationsPtr
//
//		);
//	}

};

class MachineFreeSlotTuple{
public:
	int suitableStartTime;
	int mId;

	MachineFreeSlotTuple(){
		this->mId=-1;
		this->suitableStartTime=-1;
	}
};


class TabuSearch{
public:

	int tabuTenure;
	int tabuList[50][50];



	TabuSearch(){

		int nJ=50;

		this->tabuTenure=int((nJ*nJ)^(1/2));//nJ=50

		for(int i=0;i<nJ;i++){
			for(int j=0;j<nJ;j++){
				this->tabuList[i][j]=0;
//				cout<<"tabuList[i][j]="<<tabuList[i][j]<<endl;
			}
		}
		cout<<"tabuList initialized successfully"<<endl;

	}


	int generateRandomNumber(int start=0,int end=10){

		random_device rd;
		mt19937 eng(rd());
		uniform_int_distribution<> distr(start,end);

		return distr(eng);

	}

	//////////////////////////////////////////////
	/*void generateInitialSeed(){

		const int nJ=jobs.size();
		int *jobsCountArray=new int[nJ];

		int i=0;
		for(auto j :jobs){
			jobsCountArray[i]=j.opSize;
			i++;
		}

	//	for(int j=0;j<nJ;j++){
	//		cout<<"job["<<j<<"= has operations = "<<jobsCountArray[j]<<endl;
	//	}

		const int nOp=operations.size();
		int seed[nOp]={-3};

		int isAllSet=0;
		i=0;
		while(!isAllSet){
	//		cout<<i<<endl;
	//		cout<<"random number "<<i<<" = "<<generateRandomNumber(1,9551)<<endl;

			int jRandIdx=generateRandomNumber(0,nJ);
	//		cout<<"jRandIdx = "<<jRandIdx<<endl;

			if(jRandIdx>=0 && jRandIdx<=nJ){

				if (jobsCountArray[jRandIdx]>0){
					jobsCountArray[jRandIdx]--;

					if (i==int(operations.size())){
						break;
					}


	//				int seedIdx=generateRandomNumber(0,nOp);
	//				seed[seedIdx]=jRandIdx;
					seed[i]=jRandIdx;


					i++;

					int isLastJobCountZero=0;
					for (int j=0;j<nJ;j++){

						if (jobsCountArray[j]==0){
							isLastJobCountZero++;
						}
						if(isLastJobCountZero==nJ){
							isAllSet=1;
						}
					}



				}
			}
		}
		cout<<"seed = ";
		for( i=0;i<nOp;i++){
			cout<<seed[i]<<" ";
		}
		cout<<endl;
	}*/
	//////////////////////////////////////////////



	int* swapper(int *seed){
		int len=(sizeof(seed)/sizeof(*seed));
		int a =generateRandomNumber(0,len);
		int b=generateRandomNumber(0,len);

		while (a==b){
			b=generateRandomNumber(0,len);
		}


		int tempA=seed[a];
		int tempB=seed[b];


		if(this->tabuList[seed[a]][seed[b]]==0){
			this->tabuList[seed[a]][seed[b]]=this->tabuTenure;
			this->tabuList[seed[b]][seed[a]]=this->tabuTenure;

			seed[a]=tempB;
			seed[b]=tempA;
		}else{
			this->tabuList[seed[a]][seed[b]]--;
			this->tabuList[seed[b]][seed[a]]--;
		}

		//real swaping in seed is done here

		return seed;

	}

//	int bestSuitableMachine(Operation currentOp,int prevEndTime,vector<Machine> &machines){
//
//	}
	bool compareInterval(TimeSlotTuple i1, TimeSlotTuple i2)
	{
	    return (i1.start < i2.start);
	}


	int computeMakespan(int *seedIn,map<string,vector<int>>&machinesMap,vector<Machine> &machines,vector<AnOperation> &operations,vector<Job>& jobs){

		cout<<"in computeMakespan now"<<endl;

		const int nOp=int(operations.size());
		int seed[nOp];

		for(int i=0;i<nOp;i++){
//			cout<<"si = "<<seedIn[i]<<endl;
			seed[i]=seedIn[i];
		}

		const int nJ=jobs.size();
		int mappedOperationsOfJobs[nJ]={0};

		int len=(sizeof(seed)/sizeof(*seed));
		cout<<"length of seed in computeMakespan = "<<len<<endl;

		for(int i=0;i<len;i++){
			int jId=seed[i];
			int opId=mappedOperationsOfJobs[jId];
//			printf("jId = %d ,opId = %d\n",jId,opId);

			AnOperation currentOp;
			currentOp.jobId=jId;
			currentOp.opId=opId;
//			printf("jId = %d ,opId = %d\n",currentOp.jobId,currentOp.opId);

			cout<<"crossing line#380\n";
			int prevEndTime=0;

			if(opId>0){
				prevEndTime=operations.at(i-1).endTime;
			}

			cout<<"crossing line#410\n";
			currentOp.duration=jobs.at(jId).operations.at(opId).duration;
			cout<<"crossing line#412\n";
			currentOp.opName=jobs.at(jId).operations.at(opId).opName;
			cout<<"crossing line#414\n";
			cout<<"op.opName="<<currentOp.opName<<endl;

			cout<<"crossing line#417\n";

//			Machine m=new Machine();

			vector<Machine> suitableMachines;//=machinesMap[currentOp.opType];
			suitableMachines.clear();
			cout<<"crossing line#423\n";

//			string opType=currentOp.opName;
			cout<<"opType = "<<currentOp.opName<<endl;

			int n=0;
			for(auto mac:machinesMap[currentOp.opName]){
				cout<<"crossing line#429"<<" ("<<n<<")"<<endl;
				n++;
				suitableMachines.push_back(mac);
			}

			cout<<"crossing line#433\n";

			vector<MachineFreeSlotTuple> bestSuitableTimeSlots;
			bestSuitableTimeSlots.clear();

			cout<<"crossing line#438\n";

			//finding best suitable timeslots
			for(auto mac:suitableMachines){
				int selectNextIndexAsSuitable=0;//
				cout<<"z@kii testAbc\n";
				for (auto tsa:mac.timeSlotsArray){
					cout<<"z@kii test123456\n";

					printf("tsa St = %d, tsa D = %d\n",tsa.start,tsa.duration);

					if(		currentOp.duration<=tsa.duration &&
							prevEndTime>=tsa.start &&
							prevEndTime<=tsa.start+tsa.duration &&
							prevEndTime+currentOp.duration<=tsa.start+tsa.duration &&
							selectNextIndexAsSuitable==0){

						MachineFreeSlotTuple mfst;
						mfst.mId=mac.mId;
						mfst.suitableStartTime=prevEndTime;
						bestSuitableTimeSlots.push_back(mfst);

						printf("mId = %d ,minIdx = %d\n",mfst.mId,mfst.suitableStartTime);
						cout<<"z@kii test-------------\n";

						break;
					}else if(selectNextIndexAsSuitable==1 && currentOp.duration<=tsa.duration && prevEndTime<=tsa.start){

						MachineFreeSlotTuple mfst;
						mfst.mId=mac.mId;
						mfst.suitableStartTime=tsa.start;
						bestSuitableTimeSlots.push_back(mfst);

						printf("mId = %d ,minIdx = %d\n",mfst.mId,mfst.suitableStartTime);
						cout<<"z@kii test.............\n";

						break;


					}else{
						selectNextIndexAsSuitable=1;
						cout<<"z@kii test#############\n";
					}
				}
			}

			cout<<"crossing line#477\n";

//			printf("mId = %d ,minIdx = %d\n",mId,minIdx);

			//choosing min time instance from the suitable times of a machine set
			int minIdx=0;
			int mId=0;
			for (auto bsts:bestSuitableTimeSlots){
				if(bsts.suitableStartTime<minIdx){
					minIdx=bsts.suitableStartTime;
					mId=bsts.mId;
				}
			}
//
//			printf("mId = %d ,minIdx = %d\n",mId,minIdx);

			cout<<"crossing line#491\n";

			//allocating time to an operation
			operations.at(i).startTime=minIdx;
			operations.at(i).endTime=minIdx+currentOp.duration;
			operations.at(i).selectedMachine=mId;
			operations.at(i).endTime=minIdx+currentOp.duration;

			cout<<"crossing line#498\n";

			//booking time instance of machine scheduled array
			for (int t=minIdx;t<=currentOp.duration;t++){

				machines.at(mId).ScheduledOperationArray[t].jId=jId;
				machines.at(mId).ScheduledOperationArray[t].opId=opId;

			}

			cout<<"crossing line#509\n";

			//updating last filled index of a machine
			if(machines.at(mId).lastFilledIndex<operations.at(i).endTime){
				machines.at(mId).lastFilledIndex=operations.at(i).endTime;
			}

			cout<<"crossing line#516\n";

			//updating free slots of the machine
			int p=0;
			for(auto tsa:machines.at(mId).timeSlotsArray){

				if(tsa.start==minIdx && tsa.duration>currentOp.duration){
					tsa.start=minIdx+currentOp.duration;
					machines.at(mId).timeSlotsArray.at(p).start=tsa.start;

					cout<<"nodeStart is equal"<<endl;

				}
				else if(minIdx>tsa.start && minIdx+currentOp.duration<tsa.start+tsa.duration){

					tsa.duration=minIdx-tsa.start;//before node

					TimeSlotTuple newTst;
					newTst.start=minIdx+currentOp.duration;
					newTst.duration=(tsa.start+tsa.duration)-(minIdx+currentOp.duration);//after node
					machines.at(mId).timeSlotsArray.push_back(newTst);

					cout<<"node is in center"<<endl;


				}
				else if(currentOp.duration<=tsa.duration && (minIdx+currentOp.duration==tsa.start+tsa.duration)){
					tsa.duration=minIdx-tsa.start;
					machines.at(mId).timeSlotsArray.at(p).duration=tsa.duration;

					cout<<"nodeEnd is equal"<<endl;
				}
				p++;
			}

//			sort(machines.at(mId).timeSlotsArray.begin(),machines.at(mId).timeSlotsArray.end());

			cout<<"i = "<<i<<endl;
			mappedOperationsOfJobs[jId]++;
			cout<<"mappedOperationsOfJobs["<<jId<<"]  = "<<mappedOperationsOfJobs[jId]<<endl;

		}

		int makespan=0;

		for(auto m:machines){
			if(m.lastFilledIndex>makespan){
				makespan=m.lastFilledIndex;
			}
		}



		return makespan;
	}

};





//*/





