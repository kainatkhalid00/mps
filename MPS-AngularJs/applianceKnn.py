import csv
import pandas as pd

from sklearn.neighbors import KNeighborsClassifier

neigh = KNeighborsClassifier(n_neighbors=1)

# X = [[1.1], [1.2],[1.5],[1.7], [2.1], [1.8]]
# y = [0,0,0,1, 1, 1]
# neigh.fit(X, y) 


df=pd.read_csv("electricityData.csv",delimiter = ',',names=['Irms','label'])#,dtype={'Irms':float,'label':str}) ;

Irms=df['Irms'].format(array)
trainData=[];
for item in Irms:
	trainData.extend(item)
labels=df['label']

# print(labels)    

neigh.fit(trainData, labels) 

testData=[mqttIn]

print(neigh.predict(testData))