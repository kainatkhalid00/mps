var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://localhost:1884');


client.on('connect', function () {
    client.subscribe('nodeData', function (err) {
        if (!err) {
            client.publish('presence', 'Hello mqtt');
        }
    });
});
    
client.on('message', function (topic, message) {

    // message is Buffer
    //   console.log(message.toString())
    var i=message.toString().indexOf(',');
    if(i===null || i===undefined || i===-1){
        return;
    };
    var j=message.toString().indexOf(',',i+1);
    if(j===null || j===undefined || j===-1){
        return;
    };
    //   console.log(i,j);
    var current = Number.parseFloat(message.toString().slice(i+1,j));
    console.log('current = ',current);
    // this.data.push(current);
});
        
    